# We have moved!

The SESMotifAnalyser, a framework for analysing social-ecological systems is no
longer hosted on GWDG's GitLab server. It moved to the mother ship and is no longer
maintained here. 

Please visit **https://gitlab.com/t.seppelt/sesmotifanalyser**!