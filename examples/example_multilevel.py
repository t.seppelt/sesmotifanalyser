#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# Example used in the documentation

import sma

"""
We use a random graph here. In order to be able to reproduce the counts, a fixed
(random) graph will be loaded instead.

nEdges = numpy.array([[11, 12,  6],
                      [ 0,  2, 10],
                      [ 0,  0,  5]])
generator = sma.randomMultiSENs([10,3,7], 
                                nEdges, 
                                names=['Actor', 'Venue', 'Issue'])
G = next(generator)
"""
G = sma.loadSEN("data/dummy_multi_adj.csv",
                "data/dummy_multi_attr.csv",
                delimiter = ',')

cm = {0: '#ffc000', 1: '#00b0f0', 2: '#00b050'} # adapt the colors
sma.drawSEN(G, color_map = cm)

# (a)-(c): set of all motifs consisting of two actors and one venue:
motifs1 = sma.MultiMotifs(G, 2, 1, 0)
list1 = list(motifs1) # [(('Actor 0', 'Actor 1'), ('Venue 0',), ()), … ]

# (d)-(e): set of all motifs consisting of one actor, one venue and one issue
motifs2 = sma.MultiMotifs(G, 1, 1, 1)
list2 = list(motifs2) # [(('Actor 0',), ('Venue 0',), ('Issue 0',)), …]

# (f)-(g): one venue, two issues
motifs3 = sma.MultiMotifs(G, 0, 1, 2)
list3 = list(motifs3) # [((), ('Venue 0',), ('Issue 0', 'Issue 1')), …]

# (h)-(i): one actor, two venues, one issue
motifs4 = sma.MultiMotifs(G, 1, 2, 1)
list4 = list(motifs4) # [(('Actor 0',), ('Venue 0', 'Venue 1'), ('Issue 0',)), …]

classificator = sma.MultiMotifClassificator(G, 2, 1, 0)
classificator((('Actor 0', 'Actor 9'), ('Venue 0',), ())) # I.C = (b)
classes = list(map(classificator, list1)) # ['I.B', 'I.B', 'I.B', 'I.B', 'I.B' …]

counts210 = sma.countMotifs(G, 
                            sma.MultiMotifClassificator(G, 2, 1, 0), 
                            sma.MultiMotifs(G, 2, 1, 0))
# {'I.A': 40, 'I.B': 48, 'I.C': 14, 'II.A': 9, 'II.B': 16, 'II.C': 8}

# or shortened:
counts210 = sma.countMultiMotifs(G, 2, 1, 0) # same output as before

counts012 = sma.countMultiMotifs(G, 0, 1, 2)
# {'I.A': 12, 'I.B': 24, 'I.C': 12, 'II.A': 5, 'II.B': 8, 'II.C': 2}

counts111 = sma.countMultiMotifs(G, 1, 1, 1)
# {0: 63, 1: 36, 2: 53, 3: 40, 4: 5, 5: 6, 6: 5, 7: 2}

motifsD = list(sma.MultiMotifs(G, 1, 1, 1) 
               & sma.isClass(sma.MultiMotifClassificator(G, 1, 1, 1), 6))
# [(('Actor 1',), ('Venue 1',), ('Issue 5',)),…]

motifsE = list(sma.MultiMotifs(G, 1, 1, 1) 
               & sma.isClass(sma.MultiMotifClassificator(G, 1, 1, 1), 7))
# [(('Actor 4',), ('Venue 0',), ('Issue 2',)),…]

counts220 = sma.countMultiMotifs(G, 2, 2, 0)
print('Motifs of class (j): %d' % counts220['I.B']) # 0
print('Motifs of class (o): %d' % counts220['V.B']) # 5

# (h) and (i)
counts121 = sma.countMultiMotifs(G, 1, 2, 1)
print('Motifs of class (h): %d' % counts121[2]) # 5
print('Motifs of class (i): %d' % counts121[1]) # 18
# Which motif is of class (h)? Let's draw it!
motif = next(sma.MultiMotifs(G, 1, 2, 1) 
             & sma.isClass(sma.MultiMotifClassificator(G, 1, 2, 1), 2))
subgraph = G.subgraph(motif[0]+motif[1]+motif[2])
sma.drawSEN(subgraph, color_map = cm, pos = sma.layer_layout(subgraph))    

# (k) and (l)
counts221 = sma.countMultiMotifs(G, 2, 2, 1)
print('Motifs of class (k): %d' % counts221['I.B.2']) # 0
print('Motifs of class (l): %d' % counts221['I.A.2']) # 24

# (m) and (n)
counts222 = sma.countMultiMotifs(G, 2, 2, 2)
print('Motifs of class (m): %d' % counts222[2]) # 0
print('Motifs of class (n): %d' % counts222[1]) # 0