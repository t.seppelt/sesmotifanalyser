#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Script for creating the digraph used for testing.
"""

import networkx as nx
import sma

G = nx.DiGraph()
G.add_nodes_from(((k, {"sesType" : k % 2}) for k in range(6)))
G.add_edges_from([(0, 1), (0, 2), (0, 3), (0, 4), (0, 5), 
                  (1, 3), (1, 4), (1, 5), (4, 5), (3, 2),
                  (4, 1), (4, 2), (4, 0), (3, 0)])

sma.saveSEN(G, 'test/data/digraph_adj.csv', 'test/data/digraph_attr.csv')