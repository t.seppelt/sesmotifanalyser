Input/Output
============

This module provided input/output functions for reading and writing
SENs in multiple formats using CSV files. Be aware, that the networkx
package provides a wide range of other sophisticated input and output
facilities.

Here functions for interacting with R's statnet are not listed. See
:py:meth:`sma.translateGraph` and the respective section for more details
on this.

Most input functions will detect inconsistencies in the input data only to a
certain extent. For example, it may happen that when inconsistent files are read
nodes without ``sesType``-attribute are added to the graph. Such nodes let many
function used in analyses swallop up. Use :py:meth:`sma.untypedNodes` to detect
them. Statistical functions such as :py:meth:`sma.edgesCount`, :py:meth:`sma.nodesCount`
or :py:meth:`sma.edgesCountMatrix` (for multilevel SENs) can help to verify that
data has been read correctly.

Input
-----

.. autofunction:: sma.loadSEN
.. autofunction:: sma.loadDiSEN
.. autofunction:: sma.loadMultifileSEN
.. autofunction:: sma.loadMPNetSEN

Output
------

.. autofunction:: sma.saveSEN


