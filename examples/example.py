#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import sma
import matplotlib.pyplot as plt
import numpy, scipy.stats

# load graph
G = sma.loadSEN('data/dummy_adj.csv', 'data/dummy_attr.csv', delimiter=',')

# draw the graph
sma.drawSEN(G)

# compute and plot linear regression between triangle coefficients and nodal attribute EFFB
plt.figure()
data = numpy.array([[G.nodes[i]['EFFB'], sma.triangleCoefficient(G, i)] for i in sma.sesSubgraph(G, sma.NODE_TYPE_ECO)]).astype(float)
plt.scatter(*data.T)
slope, intercept, r_value, p_value, std_err = scipy.stats.linregress( *data.T )
x = numpy.linspace(min(data[:,0]), max(data[:,0]), 2)
plt.plot(x, slope*x +intercept)

# standard 4-motif counting
print(sma.count4Motifs(G))

# count 4-motifs with nodal attribute EXP > 2:
cond = sma.hasProperty('EXP', lambda x : float(x) > 3)
it = sma.FourMotifs(G) & cond
lit = list(it)
print(len(lit)) # 234

# classify these 4-motifs
print(sma.countMotifs(G, 
                      iterator = lit, 
                      classificator = sma.FourMotifClassificator(G)))

# compute a random graph similiar to G (w.r.t. the amount of edges in each domain and plot it)
plt.figure()
G_rand = next(sma.randomSimilarSENs(G))
sma.drawSEN(G_rand)