.. _sec-generate: 

Generation of random SENs
=========================

*See also* :ref:`sec-distribution` and :ref:`sec-simulate`.

Graphs can be generated randomly for statistical analysis. For answering
more sophisticated questions the set of possible graphs can be refined,
for example to only those graphs with a certain amount of edges per between
specific levels.

Several baseline models are implemented in this package:

    - :py:const:`sma.MODEL_FIXED_DENSITIES` random graphs contain a fixed number
      of edges on the different levels and between the various levels. Most functions
      are based on this model, e.g. :py:meth:`sma.randomSimilarSENs`, 
      :py:meth:`sma.randomMultiSENsFixedDensities`, etc.
    - :py:const:`sma.MODEL_ERDOS_RENYI` random graphs whose edges are chosen
      independently at random with respect to fixed probabilities. The refined 
      Erdős-Rényi model used here accounts for the multitude of levels by working
      with separate edge probabilities for each pair of level. See
      :py:meth:`sma.randomMultiSENsErdosRenyi`.
    - :py:const:`sma.MODEL_ACTORS_CHOICE` random graphs whose edges are chosen
      only on one level at random (there according to :py:const:`sma.MODEL_ERDOS_RENYI`).
      The other levels are fixed.

Random graphs drawn from a less refined baseline model (no control over edges, or
just overall density) can be generated using :py:meth:`sma.randomSENs`.

**Technical remark**
Note that all functions in this sections are from the technical point of
view Python generators. They do not return a single graph object but a 
generator object which can provide (infinitely) many graph objects.
Have a look at the following example:

.. code-block :: Python

	import sma
	
	# get one random graph
	G = next(sma.randomSENs(10,5))
	
	# get a list of 10 random graphs
	import itertools
	n = 10
	graphList = list(itertools.islice(sma.randomSENS(10,5), n)
	
	# generate and draw random graph, cf. figure:
	sma.drawSEN(next(sma.randomSENs(3, 5, 0.2)))

Random SENs from scratch
------------------------

Most simply, randoms SENs can be generated based on elementary properties,
e.g. the number of social / ecological nodes, the density or the number of
edges per domain.

**Example:** Random network with three ecological and five social nodes, density 0.2.

.. image :: _static/randomSEN.pdf
	:align: center
	:width: 50%

.. autofunction :: sma.randomSENs
.. autofunction :: sma.randomSpecialSENs
.. autofunction :: sma.randomDiSENs

Random SENs based on given SENs
-------------------------------

For more complex statistical analysis it may be desirable to compute a set 
of random graphs which shares certain properties with a given SEN. These
properties can again be the number of social / ecological nodes or the density.
Furthermore, random SENs which inherit all nodal attributes can be generated.
This is especially useful when analysing the distribution of nodal properties
in motifs.

**Example:** Consider a SEN which contains major German cities as nodes (the edges
are chosen randomly). The network is not only aware of the social ecological type 
of the nodes (here chosen randomly) but also of their geographical location. 
Then :py:meth:`sma.randomSimilarAttributedSENs` can be used to generate a new
SEN of the same cities at their original location but with new random edges.
The following figure shows the original SEN and a randomly generated one plotted
with :py:meth:`sma.drawGeoSEN`.

.. image :: _static/random_geo.pdf
	:align: center
	:width: 70%

.. autofunction :: sma.randomSimilarSENs
.. autofunction :: sma.randomSimilarAttributedSENs


Multi-level SENs
----------------

The front-end function :py:meth:`sma.randomSimilarMultiSENs` can be used to call
various generators for several baseline models listed in this section.
	
.. autofunction :: sma.randomSimilarMultiSENs

The random multi SEN in the example in :py:meth:`sma.randomMultiSENsFixedDensities` 
could look as follows:

.. image :: _static/randommulti.pdf
	:align: center
	:width: 50%

.. autofunction :: sma.randomMultiSENsFixedDensities
.. autofunction :: sma.randomMultiSENsErdosRenyi
.. autofunction :: sma.randomMultiSENsActorsChoice