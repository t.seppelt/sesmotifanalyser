.. _sec-zoo:

Appendix: The Motif Zoo
=======================

Motifs are identified using *motif identifiers*, e.g. ``1,2[I.C]``, the open triangle
with one node on level 0 and two nodes on level 1. They describe the position of
the motif in the network (*positions*) and the structure of the motif itself
(*signature* and  *motif class*). See :ref:`sec-terminology` for a complete definition.

This section lists the motif classes for the signatures supported by this software.
The motifs are grouped by the number of levels they span across.

Undirected motifs
*****************

Motifs in this section occur in undirected networks.

One level motifs
----------------

Motifs in this section contain nodes from only one level.

1-motifs
++++++++

- **Signature:** ``1``
- **Motif Info Object**: :py:class:`sma.Motif1Info`

A 1-motif consists of just one node. Therefore there is also only one motif class
called ``1``.

Plain 2-motifs
++++++++++++++

- **Signature:** ``2``
- **Motif Info Object**: :py:class:`sma.Motif2Info`

Plain 2-motifs contain two nodes from the same level.

.. image :: _static/twoPMotifs.pdf
	:align: center

Plain 3-motifs
++++++++++++++

- **Signature:** ``3``
- **Motif Info Object**: :py:class:`sma.Motif3pInfo`

Plain 3-motifs contain three nodes from the same level.

.. image :: _static/threePMotifs.pdf
	:align: center

Two level motifs
----------------

Motifs in this section contain nodes from two distinct levels.

2-motifs
++++++++

- **Signature:** ``1,1``
- **Motif Info Object**: :py:class:`sma.Motif11Info`

2-motifs contain two nodes from distinct levels.

.. image :: _static/twoMotifs.pdf
	:align: center

.. _sec-zoo3:

3-motifs
++++++++

- **Signature:** ``1,2``
- **Motif Info Object**: :py:class:`sma.Motif3Info`

3-motifs can be classified in 6 classes which will be called
I.A to II.C. The following figure shows the configurations for the classes 
of 3S-motifs, i.e. the distinct node is social (colouring ecological nodes green 
and social nodes red). 3E-motifs are classified analogously. In the figure only 
the colours would have to be swapped.

See also the classification for the corresponding directed motifs in :ref:`sec-zoo3di`.

.. image :: _static/threeMotifs.pdf
	:align: center

.. _sec-zoo4:

4-motifs
++++++++

- **Signature:** ``2,2``
- **Motif Info Object**: :py:class:`sma.Motif4Info`

The classification for 4-motifs used in this software was proposed by Ö. Bodin and M. Tengö.
The following illustration is taken from their original paper.

**Warning**
The order of the levels in 4-motifs may deviate from the order illustrated in the
figure below:

    - When calling functions like :py:meth:`sma.count4Motifs` or 
      :py:meth:`sma.count4MotifsSparse` with standard parameters, the roles of the
      levels are as indicated in the figure, i.e. green nodes
      have ``sesType = 0`` and red nodes have ``sesType = 1``.
    - When using :py:meth:`sma.countMotifsAuto`, :py:meth:`sma.countMultiMotifs`, 
      or working with 4-motifs from R, the first level is red and the second level green.

**Reference** Ö. Bodin, M. Tengö: Disentangling intangible social–ecological systems
in Global Environmental Change 22 (2012) 430–439
http://dx.doi.org/10.1016/j.gloenvcha.2012.01.005

.. image :: _static/fourMotifs.pdf
	:align: center

(1,3)-motifs
++++++++++++

- **Signature:** ``1,3``
- **Motif Info Object**: :py:class:`sma.Motif13Info`

There is only one classified motif with this signiture. All other motifs are unclassified (``-1``).

.. image :: _static/oneThreeMotifs.pdf
    :align: center

(2,3)-motifs
++++++++++++

- **Signature:** ``2,3``
- **Motif Info Object**: :py:class:`sma.Motif23Info`

There is only one classified motif with this signiture. All other motifs are unclassified (``-1``).

.. image :: _static/twoThreeMotifs.pdf
    :align: center


Three level motif
-----------------

Motifs in this section contain nodes from three distinct levels.

(1,1,1)-motifs
++++++++++++++

- **Signature:** ``1,1,1``
- **Motif Info Object**: :py:class:`sma.Motif111Info`

The levels are numbered from the top to the bottom.

.. image :: _static/multi111Motifs.pdf
	:align: center

(1,2,1)-motifs
++++++++++++++

- **Signature:** ``1,2,1``
- **Motif Info Object**: :py:class:`sma.Motif121Info`

The levels are numbered from the top to the bottom. :math:`-1` represents all other
motifs, i.e. the motifs that do not fall into the classes specified by :math:`1, \dots, 4`.

.. image :: _static/multi121Motifs.pdf
	:align: center

(2,2,1)-motifs
++++++++++++++

- **Signature:** ``2,2,1``
- **Motif Info Object**: :py:class:`sma.Motif221Info`

The levels are numbered from the top to the bottom. The grey box is a placeholder
for a 4-motif (see :ref:`sec-zoo4`) consisting for nodes of two top levels. If for example the gray box
contains a III.D motif (complete graph), then the (2,2,1)-motif is either of class
III.D.0 (no edges to the bottom level), III.D.1 (only both top-bottom edges), 
III.D.2 (only both middle-bottom edges), III.D.3 (all top-bottom and middle-bottom edges)
or Unclassified (if it does not fall into one of the preceding classes).

Unclassified represents all other motifs, i.e. the motifs that do not fall into the other classes.

.. image :: _static/multi221Motifs.pdf
	:align: center

(2,2,2)-motifs
++++++++++++++

- **Signature:** ``2,2,2``
- **Motif Info Object**: :py:class:`sma.Motif222Info`

The levels are numbered from the top to the bottom. :math:`-1` represents all other
motifs, i.e. the motifs that do not fall into the classes specified by :math:`1, \dots, 4`.

.. image :: _static/multi222Motifs.pdf
	:align: center

Directed motifs
***************

Motifs in this section occur in directed networks.

One level motifs
----------------

Plain 2-motifs
++++++++++++++

- **Signature:** ``2``
- **Motif Info Object**: :py:class:`sma.DiMotif2Info`

.. image :: _static/dimotif2.pdf
	:align: center

Two level motifs
----------------

.. _sec-zoo2di:

2-motifs
++++++++

- **Signature:** ``1,1``
- **Motif Info Object**: :py:class:`sma.DiMotif11Info`

.. image :: _static/dimotif11.pdf
	:align: center

.. _sec-zoo3di:

(1,2)-motifs
++++++++++++

- **Signature:** ``1,2``
- **Motif Info Object**: :py:class:`sma.DiMotif12Info`

Directed (1,2)-motifs consist of three nodes, of which two are located on the same level.
The :math:`2^6 = 64` possible directed graphs on three nodes fall into 36 classes
which are listed below. The nomenclature follows the naming scheme for undirected
:ref:`sec-zoo3`. When considering the underlying undirected graphs, class I.A
corresponds to I.A, classes I.B.* to I.B, I.C.* to I.C, II.A to II.A, II.B.* to
II.B, II.C.* to II.C and III.A to II.A, III.B.* to II.B, III.C.* to II.C. 

.. image :: _static/dimotif12a.pdf
	:align: center

The first figure shows the motifs of classes I.* (i.e., I.A to I.C.6) and III.*
(i.e., III.A to III.C.6). Motifs of classes I.* have no edges between the two green
vertices on the non-distinct level. Motifs of classes III.* have both possible 
vertices on this level.

.. image :: _static/dimotif12b.pdf
	:align: center
	
The second figure illustrates the remaining motifs of classes II.*. These motifs
have only one of the two possible edges between the two green vertices.

Note that the motifs of classes II.* occur exactly twice among the 64 possible
directed graphs on three nodes while in I.* and III.* only the motifs of classes 
I.B.1, I.B.2, I.B.3, I.C.2, I.C.4, I.C.5 (and respectively III.*) occur twice. 
All other motifs have exactly one representative.

(2,2)-motifs
++++++++++++

- **Signature:** ``2,2``
- **Motif Info Object**: :py:class:`sma.DiMotif22Info`

Directed (2,2)-motifs consist of four nodes, of which two are located on the
upper level and two are located on the lower level. The :math:`2^{12} = 4069` possible
directed graphs with such vertices mostly admit only few symmetries. This results
in a rather complicated classification comprising 960 classes of motifs.

At a first glance, there are nine main groups of such motifs. These are illustrated
below:

.. image :: _static/dimotif22_overview.pdf
	:align: center

The question marks indicate that within the nine groups every possible configuration
of edges spanning across the two levels occurs. Each main group comes with a 
different classification for its motifs. The class names are of the form *MAIN.SUB*.
Where *MAIN* is one of the main groups I, II, III, IV, V, VI, VII, VIII, IX. The
possible values of *SUB* depend on the main groups and are stated in the following
subsections.

In the following figures the edges which are determined by the main group are
drawn in gray. The black edges are the ones which affect the classification.

.. _sec-zoo4di-asym:

Asymmetric group V
##################

In this group the edges on the top and on the bottom level induce a canonical order
of the nodes (and hence of the edges). There are :math:`2^8 = 256` motif classes in
this group. The (underlying undirected) inter-level edges are numbered as follows:

.. image :: _static/dimotif22_asymmetrical.pdf
	:align: center
	
The names of the classes are the 256 length four strings on letters *n*, *d*, *u* and
*b*. Where *n* stands for "none", *d* for "down", *u* for "up" and *b* for "both". According
to the order of the edges given above, each edge is assigned one of the letters 
(*n* if its is not present, *d* if it is oriented downwards, *u* if upwards and *b* if
both edges, upwards and downwards, occur in the motif).

.. _sec-zoo4di-simply-sym:

Simply-symmetric groups II, IV, VI and VIII
###########################################

In these groups a single edge on one of the levels induces an order of its end
nodes (tail and head). For the purpose of this section this level is called the yellow level
while the other level is called the blue level. The nodes on the blue level are 
not ordered. It remains to classify edges in two stars, the tail star and the head 
star (see below). The tail (head resp.) star consists of the edges between the tail (head resp.) 
and the nodes on the blue level. 

.. image :: _static/dimotif22_simply-symmetrical.pdf
	:align: center
	
The names of the classes follow the nomenclature for directed :ref:`sec-zoo3di`.
The two stars are classified separately as (1,2)-motifs of classes I.* or III.*
without considering the edges on the opposing (blue) level. This yields class names A to C.6
(forgetting about the I or III since these stem from the edges on the blue level).
The class of the motif is then the concatenation of the class of the tail star
and the class of the head star omitting all full stops. This results in 100
classes AA, AB1, ..., AC5, AC6, B1A, ..., B1C6, ..., C6A, ..., C6C6.

.. _sec-zoo4di-double-sym:

Double-symmetric groups I, III, VII and IX
##########################################

In these groups the edges on the top and on the bottom level do not induce an 
orientation. Each group is subdivided into the following subgroups according to
the underlying undirected bipartite graphs of the motifs. Class names are composed of the name
of the subgroup (see figure) and of specifiers depending on the subgroup. These
specifiers are defined below (cf. second component of nomenclature for :ref:`sec-zoo4`).

.. image :: _static/dimotif22_4gadget.pdf
	:align: center
	
In **subcase A**, the classification is complete. This yields a single motif class.

In **subcase B**, the classification is completed by invoking the classificator for
directed :ref:`sec-zoo2di`. This yields 3 motif classes (1, 2, 3).

In **subcases C and D**, the classification is completed by classifying the directed
star as in :ref:`sec-zoo4di-simply-sym`. Note that the classes A to B3 do not occur. This
yields for each subcase 6 motif classes (1 to 6 abbreviating C1 to C6).

In **subcase E**, the following classification is used. There are 6 motif classes 
called 1, ..., 6.

.. image :: _static/dimotif22_subclassE.pdf
	:align: center

In **subcase F**, the task is to classify a path (i.e. a path in the underlying undirected
graph) starting in one of the upper nodes and ending after 3 links in one of the lower
nodes. When unfolded, this path looks as follows. The possible edges inherit canonical
indices from the order of the nodes on the path. For the purpose of this classification
the direction from left most red node to the right most green node is called *downwards*.

.. image :: _static/dimotif22_subclassF.pdf
	:align: center

The class names are obtained by assigning to each of the nodes in the given order
one of the letters *d*, *u* or *b*; *d* when the edge is oriented downwards, *u*
when it is oriented upwards or *b* when both edges exist in the motif. This yields
classes ddd to bbb (27 in total), cf. the following figure which depicts a similar
classification.

Be aware that the directions used here might be confusing. When the first edge
is oriented downwards it indeed links an upper node to a lower node. But when the
second is oriented downwards it links a lower node to an upper node.

In **subcase G**, the underlying undirected bipartite graph is complete. Considering
another undirected bipartite graph, this subcase is divided into 7 further subcases.
More precisely, the classification for undirected bipartite graph in classes A to
G which is illustrated at the beginning of this section is applied to the underlying undirected
graph (the *double graph*) of the graph obtained
from the motif by removing all directed edges which do not occur together with
the edge in inverse direction. These subsubclasses are called Ga to Gg where the
lower case letters are stemming from the classification of the double graph.

- In **subsubcase Ga**, the bipartite subgraph of the motif is a tournament, i.e.
  no edge occurs together with its counterpart in inverse direction. Here the 
  classification of undirected bipartite graphs from the beginning of the section
  is applied again to the *downward graph*, i.e. the undirected bipartite graph
  obtained from the motif by considering only the edges in downward direction 
  (and forgetting about their direction). This yields classes A to G (7 in total).
- In **subsubcase Gb**, the following classification (cf. subcase F) is applied
  to the directed path obtained from the motif by removing the two edges that correspond
  to the unique edge in the double graph. It remains to consider the directions of the 
  three edges on the path. To each of the edges one of the letters *u* (upwards) 
  or *d* (downwards) is assigned. This yields 8 possible classes called uuu to ddd.

.. image :: _static/dimotif22_subclassGb.pdf
	:align: center

- In **subsubcases Gc and Gd**, the directed star (cf. :ref:`sec-zoo4di-simply-sym`)
  which remains when removing the four edges that correspond to the two edges in the
  double graph has to be classified. This is done as above. The only occurring 
  classes are C1, C2 and C3, which are abbreviated as 1, 2 and 3 (3 classes in total).
- In **subsubcase Ge**, the pair of two parallel directed edges remaining when removing
  the four edges that correspond to the two edges in the double graph has to be 
  classified. This is done as in subcase E. Note that the only possible classes
  are 1, 2 and 4 (3 classes in total).
- In **subsubcase Gf**, the edge classification (cf. subcase B, :ref:`sec-zoo2di`) 
  is applied to the remaining edges when removing the six edges corresponding to the
  three edges in the double graph. This yields classes 1, 2 (2 in total).
- In **subsubcase Gg**, the classification is complete (1 class in total). Nothing 
  is added to the class name.

In total, case G comprises 27 classes. The class names from the subclasses are concatenated
to the specifiers Ga to Gg separated by a full stop.

In total, the double-symmetric groups I, III, VII, IX each comprise 76 classes.

Remarks
#######

As indicated above, the :math:`2^{12} = 4069` directed graphs on four vertices on
two levels fall into 984 motif classes. As a sanity check, the number of representatives
in each class can be counted:

- The :ref:`sec-zoo4di-asym` comprises 256 motif classes each with 4 representatives depending on
  the orientation of the edge on the upper and the edge on the lower level. In
  total there are hence 1024 representatives.
- The four :ref:`sec-zoo4di-simply-sym` each comprise 100 motif classes. There 
  are 16 stars (falling into 9 classes) and hence :math:`2 \cdot 16^2 = 512`
  representatives in each group where the factor 2 stems from the orientation of
  the distinct intra-level edge. In total, 2048 representatives fall into these groups.
- The four :ref:`sec-zoo4di-double-sym` each comprise 76 motif classes.

  - In subcase A, there is only 1 representative.
  - In subcase B, there are :math:`4 \cdot 3 = 12` representatives.
  - In subcase C, there are :math:`2 \cdot 9 = 18` representatives.
  - In subcase D, there are :math:`2 \cdot 9 = 18` representatives.
  - In subcase E, there are :math:`2 \cdot 9 = 18` representatives.
  - In subcase F, there are :math:`4 \cdot 27 = 108` representatives.
  - In subcase G, there are 81 representatives subdivided as follows:
    
    - In subsubcase Ga, there are :math:`2^4 = 16` representatives.
    - In subsubcase Gb, there are :math:`4 \cdot 8 =32` representatives.
    - In subsubcase Gc, there are :math:`2 \cdot 4 = 8` representatives.
    - In subsubcase Gd, there are :math:`2 \cdot 4 = 8` representatives.
    - In subsubcase Ge, there are :math:`2 \cdot 4 = 8` representatives.
    - In subsubcase Gf, there are :math:`2 \cdot 4 = 8` representatives.
    - In subsubcase Gg, there is only 1 representatives.
  
  In total, there are 1024 representatives in these groups.
