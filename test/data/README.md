# Dummy Data Files

This directory contains dummy data files that are used for testing both for sma and for the R package motifr.

## Two Level Dummy Net

![twolevel](twolevel.svg)

## Three Level Dummy Net

![threelevel](threelevel.svg)

## Directed Dummy Net

![digraph](digraph.svg)

See [digraph.py](digraph.py) for the generating Python script.

## Large Directed Dummy Net

![large digraph](large_digraph.svg)

This graph was randomly generated using

```Python

next(sma.randomDiSENs(15, 15))

````