Introduction
============

Social-Ecological Systems have been analysed in may different ways using several
statistical approaches. This software package, the SESMotifAnalyser or sma, was developed
in the context of the work of Ö. Bodin et. al. Most of the terminology is taken
from :cite:`bodin_disentangling_2012` and :cite:`bodin_formation_2016`.

This software package consists of two major parts: a Python module for analysing
SES' which is as also accessible in R and an extension of the R package ``ergm.userterms``
for analysing SES' in the framework of Exponential Random Graph Modelling.

This package is the result of two internship carried out at the Stockholm Resilience
Centre in October 2018 and August 2019 by Tim Seppelt (t.seppelt-dev@posteo.de) under
the supervision of Örjan Bodin (orjan.bodin@su.se). It is published under GNU GPLv3.
Contact the author in case of any questions.

This file aims to provide a complete documentation of all components of sma. Particularly
interesting are the following sections: :ref:`sec-terminology`, :ref:`sec-multi`
and :ref:`sec-zoo`.

.. bibliography:: _static/literature.bib

Installation
------------

The Python package can be installed from PyPI using ``pip``.

.. code :: bash
    
    pip3 install sma
    
Per default, dependencies for drawing SENs will not be installed. Use the following
command instead:

.. code :: bash
    
    pip3 install sma[plots]

    
The version in PyPI might be slightly older than the most recent version on Gitlab.
    
Please follow these instructions for installing the R ``ergm.userterms``.

    1. download this repository, either with git or as archive.
    2. call in R: ``download.packages("ergm.userterms", type="source", destdir=".")``
    3. unzip the just downloaded archive
    4. copy the files from this repository from the folder ``ergm.userterms/`` to
       the respective folders in the unzipped archive
    5. open a terminal in the folder containing the ``ergm.userterms`` folder
    6. call ``RCMD INSTALL ergm.userterms`` or in Unix-like operating systems
       ``R CMD INSTALL ergm.userterms``.
    7. now the package is available in R and can be used after calling ``library(ergm.userterms)``,
       see ``example.R`` for examples.
       
Check the documentation of R's ``ergm.userterm`` package for further details on 
the installation. See also :ref:`sec-rbridge`.

Motifr
------

Many functions of this package are available from within R. The corresponding R
package is `motifr <https://marioangst.github.io/motifr/>`_ developed by Mario Angst
and the author of this package.

.. _sec-terminology: 

Terminology
-----------

A *social-ecological network (SEN)* is a simple directed or undirected graph [#f1]_ with two-coloured 
vertices and without self-loops. These 
two colours are called *ecological* or *social* and encoded using the nodal attribute
``sesType`` in :py:mod:`networkx` graph objects. The colour of a node is often
referred to as its *level*. Per default, the ecological level is level zero (``sesType = 0``)
and the social level is level one (``sesType = 1``). All nodes of one level form
a *subsystem*, e.g. the *ecological / social subsystem*.

A *multi-level network* is a simple graph coloured vertices. In such a network
more than two colours can occur, e.g. there are *three-level networks* with three
levels and *two-level networks*, which are just SENs. The levels are named 
:math:`0, 1, 2, \dots` and encoded using the nodal attribute ``sesType``.
Often the number of levels is not important and the terms multi-level network and
SEN are used synonymously. 

A *motif* is a subgraph of network typically consisting only of few nodes. The main
purpose of this package is to count motifs and to analyse their distribution. There
exist a large variety of motifs, and this package supports some of them. A full list
of all supported motifs is given in :ref:`sec-zoo`.

Motifs are identified by three pieces of information. These are,
    
    1. the *motif class* which describes the isomorphism class of the subgraph induced by 
       the motif (e.g., closed triangle, open triangle, etc.),
    2. the *signature* which indicates of how many nodes from how many distinct levels
       the motif consists,
    3. the *positions* which indicate from which levels the nodes are taken.

The motif class is specified by names, which can be looked up in :ref:`sec-zoo`.
Signatures are ordered tuples of integers, e.g. ``(1, 2)`` for triangles,
``(1, 2, 1)`` for a certain multi-level motif. The length of this tuple is the
number of levels which occur in the motif. The entries specify how many nodes are
taken from the levels. A full list of all supported signatures can be accessed
by calling :py:meth:`sma.supportedSignatures`.

Specifying the position of a motif is slightly more complicated and requires more
subtle notions.

The difference between signature and positions becomes clearer when thinking of
the signature as the amounts of nodes from each level in an abstract motif while
viewing the positions as the actual levels (``sesTypes``) in concrete SEN.

Signature and motif class together specify the abstract structure of the motif. These
two properties are often used to refer to a motif. For example, (1,2)-motifs of 
class II.C are closed triangles with one node on one level and the two other nodes
on another level (these motifs are also called 3-motifs). Another example are
(2,2)-motifs (or 4-motifs) of type I.D, i.e. four-cycles ranging across two levels. 
See the following list of detailed examples:
    
    - motif class II.C, signature 1, 2, positions 0, 1:
      closed triangle with ecological distinct node and social non-distinct nodes
      (3E-motif)
    - motif class I.C, signature 1, 2, positions 1, 0:
      open triangle with social distinct node and ecological non-distinct nodes
      (3S-motif)
    - motif class I.D, signature 2, 2, positions 1,2:
      four-cycle with two nodes from level 1 and two nodes from level 2 in a (at least) three level network
      (4-motif)

Often the positions of a motif are implicit from the signature. This package implements
a process for determining positions automatically called :ref:`sec-position_matching`.
In order to specify motifs in full generality two further objects, arities and roles,
are used.

Arities and roles always come together. *Arities* are an ordered list of integers 
defining the number of nodes taken from specific levels in a motif. *Roles* a levels
assigned to the non-zero entries in the list of arities.

Motif classes, signatures and positions can be stated using motif identifier strings.
The *motif identifier* or *motif identifier string* of a motif is a string of the
form ``HEAD[CLASS]`` or ``HEAD``, where ``CLASS`` specifies the arities and roles 
of a motif and ``CLASS`` its motif class. ``HEAD`` is a comma-separated list of arities
(e.g., ``1,2,2``) or a comma-separated list of arities and roles, separated by colons
(e.g., ``1:0,2:2,2:1``). The example above has the motif identifier ``0,2,1``, or
more succinctly ``1:2,2:1``. When referring in this case to open triangles, one gets
``1:2,2:1[I.C]``.

.. rubric:: Footnotes

.. [#f1] This package was created for analysing undirected SENs and later extended to cover
    also directed networks. Hence most older functions work only for undirected SENs.


.. _sec-position_matching: 

Position matching
+++++++++++++++++

Position matching is the process of assigning concrete positions to the entries of
the signature of a motif. This technical step is necessary for example when counting
motifs. It is implemented in :py:meth:`sma.matchPositions`.

Position matching takes as input arities and optionally roles. The output is a list
of positions (list of ``sesType``'s for every entry in the signature). The procedure
works as follows:

    1. Determine the signature of the motif. Based on the arities a supported
       signature is found. This is done by calling :py:meth:`sma.multiSignature`
       and querying :py:meth:`sma.motifInfo`, i.e. the database of all implemented motifs. 
       :py:meth:`sma.multiSignature` returns the entries of the signature in ascending
       order. The :py:class:`sma.MotifInfo` returned by :py:meth:`sma.motifInfo`
       contains the signature in proper order.
    2. Determine the positions associated with the entries of the signature. This
       is done by calling :py:meth:`sma.matchPositions`. Two cases are distinguished:
       
       - no roles given (``roles = []``): the position associated with the :math:`i`-th
         entry :math:`s_i` of the signature is the index of the :math:`j`-th 
         occurrence of the value :math:`s_i` in the list of arities where the value
         :math:`s_i` occurs :math:`j-1` times in :math:`s_0, \dots, s_{i-1}`.
       - roles given: it is assumed that roles and arities are of the same length,
         i.e. arities does not contain zero and is in fact a signature (perhaps 
         in wrong order). Two steps are needed:
         
         1. Determine the indices of the entries of the signature in the list of
            arities. This is done by calling :py:meth:`sma.matchPositions` without
            specifying roles, cf. bullet point above. This results in *prepositions*.
         2. Determine the levels corresponding to the signature entries. Roles are
            given in the order of the arities, but the signature has possibly a 
            different order. The levels are selected in the order defined by the
            prepositions.

Consider the following examples:
    
.. code:: Python
    
    # Example 1
    arities            = [1, 2]
    roles              = [] # no roles
    ordered_signature  = sma.multiSignature(arities) # gives [1, 2]
    motif_info         = sma.motifInfo(ordered_signature) # gives sma.Motif3Info
    signature          = motif_info.signature # gives [1, 2]
    positions          = sma.matchPositions(signature, arities, roles) # gives [0, 1]
    # in the (1,2)-motif specified here the first level has sesType 0,
    # the second sesType 1. This is a 3E-motif.

    # Example 2
    arities            = [2, 1]
    roles              = [] # no roles
    ordered_signature  = sma.multiSignature(arities) # gives [1, 2]
    motif_info         = sma.motifInfo(ordered_signature) # gives sma.Motif3Info
    signature          = motif_info.signature # gives [1, 2]
    positions          = sma.matchPositions(signature, arities, roles) # gives [1, 0]
    # in the (1,2)-motif specified here the first level has sesType 1,
    # the second sesType 0. This is a 3S-motif.
    
    # Example 3
    arities            = [2, 0, 2]
    roles              = [] # no roles
    ordered_signature  = sma.multiSignature(arities) # gives [2, 2]
    motif_info         = sma.motifInfo(ordered_signature) # gives sma.Motif4Info
    signature          = motif_info.signature # gives [2, 2]
    positions          = sma.matchPositions(signature, arities, roles) # gives [0, 2]
    # in the (2,2)-motif specified here the first level has sesType 0,
    # the second sesType 2.

    # Example 4
    arities            = [2, 1, 2]
    roles              = [0, 2, 1] # no roles
    ordered_signature  = sma.multiSignature(arities) # gives [1, 2, 2]
    motif_info         = sma.motifInfo(ordered_signature) # gives sma.Motif221Info
    signature          = motif_info.signature # gives [2, 2, 1]
    positions          = sma.matchPositions(signature, arities, roles) # gives [0, 1, 2]
    # in the (2,2,1)-motif specified here the first level has sesType 0,
    # the second sesType 1, the third sesType 2.

In order to test how the position matching works, call :py:meth:`sma.exemplifyMotif`.

Baseline models
+++++++++++++++

This package contains various tools for computing random graphs with respect to 
a specific baseline model and for analysing motif distributions in these baselines.

    - *Fixed Densities Model* In this model it is assumed that the number of edges
      between level :math:`i` and level :math:`j` is fixed for all :math:`i,j`.
      The specified number of edges is chosen from the set of all possible edges.
      This model is identified by :py:const:`sma.MODEL_FIXED_DENSITIES`.
    - *Erdős-Rényi Model* In this model edges between the various levels are drawn
      independently at random with respect to a fixed probability :math:`p_{ij}`.
      The (refined) Erdős-Rényi model accounts for the various levels by allowing
      to specify different probabilities for the different levels. This model is 
      identified by :py:const:`sma.MODEL_ERDOS_RENYI`.
    - *Actor's Choice Model* In this model all edges but the edges on one level
      (the level of the actors) are fixed. The edges on the level of the actors
      are chosen indepently with fixed probability as in Erdős-Rényi. This model
      is identified by :py:const:`sma.MODEL_ACTORS_CHOICE`.
    
See also :ref:`sec-distribution`, :ref:`sec-simulate` and :ref:`sec-generate`.

Data format
-----------

On the Python side, the SES' objects are stored as :py:class:`networkx.Graph`
objects. The :py:mod:`networkx` module provides a wide range of well 
documented functions. Most of them are applicable to SES', although SES' have
the special properties that they nodes belong to either the social or the ecological
type. 

This distinction is done by introducing the nodal attribute ``sesType``
which takes integers :math:`\geq 0` as values. For two-level networks, these values
are :py:const:`sma.NODE_TYPE_SOC` (1) or :py:const:`sma.NODE_TYPE_ECO` (0).
Please follow this naming schema in order to ensure compatibility.

Some comments on MPNet
----------------------

This software is somehow compatible to MPNet :cite:`wang_pnet_2009` 
(version as of Aug 2019). It can read files that MPNet can read as well, 
cf. :py:meth:`sma.loadMultifileSEN`, and files that are produced in MPNet's simulation 
mode, cf. :py:meth:`sma.loadMPNetSEN`.

MPNet distinguishes a social subsystem (A) and an ecological subsystem (B) and a system
connecting A and B, the X system. We will not refer to the latter in this documentation.

The edges in a SEN can be either between two A nodes (:py:const:`sma.EDGE_TYPE_SOC_SOC`),
between two B nodes (:py:const:`sma.EDGE_TYPE_ECO_ECO`) or in the X subsystem
(:py:const:`sma.EDGE_TYPE_ECO_SOC`). These constants are for example used by
:py:meth:`sma.edgesCount`.

Note that the patterns recognised by MPNet are different to the motifs distinguished
in our terminology. Since MPNet does not care about the existence of an edge whenever
it is not required by a pattern, its patterns are in a way more general. For example,
in MPNet's nomenclature a Star2AX motif is a 3E-motif of class II.B or II.C, since the
additional edge is not of any interest. This leads to the following selection of
correspondences between our quantities and the quantities measured in MPNet:

    - The number of Star2AX patterns in MPNet equals the number of 3E-motifs of class
      II.B plus twice the number of 3E-motifs of class II.C.
    - The number of C4AXB patterns in MPNet equals the number of 4-motifs of class
      I.D plus the number of 4-motifs of class V.D plus twice the number of 4-motifs
      of class III.D.
      
This list could be expanded to monstrous length.

Version history
---------------

Version 1.0 of this package was developed in summer 2018. In the following year
it has been continuously extended. Continuous development comes with breaking changes:

    - Version 2.0: The order and types of parameters of :py:meth:`sma.countMotifs` 
      have been changed. Functions like :py:meth:`sma.count4Motifs` have been adapted accordingly.
    - Version 2.0.4: In order to introduce a clearer nomenclature, ``sma.isType``,
      ``sma.is3Type`` and ``sma.is4Type`` were renamed to :py:class:`sma.isClass`,
      :py:class:`sma.is3Class` and :py:class:`sma.is4Class`.

Please see `Gitlab Tags <https://gitlab.com/t.seppelt/sesmotifanalyser/-/tags>`_ and `CHANGELOG <https://gitlab.com/t.seppelt/sesmotifanalyser/-/blob/master/CHANGELOG.md>`_ for more recent
developments.