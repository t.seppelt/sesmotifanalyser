#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import sma
import time, multiprocessing, itertools, numpy

# load graph
G = sma.loadSEN('data/dummy_adj.csv', 'data/dummy_attr.csv', delimiter=',')

n = 5000 # samples
processes = 6 # cores

def mapper(G):
     return sma.count3EMotifs(G, array = True)

start = time.perf_counter()
with multiprocessing.Pool(processes = processes) as p:
    res = p.imap_unordered(mapper,
                           itertools.islice(sma.randomSimilarSENs(G), n), 
                           chunksize = n // processes // 4)
    p.close()
    p.join()
    
end = time.perf_counter()
print('Calculated %d samples in %f sec' % (n, end-start))

results = numpy.array(list(res))

means = numpy.mean(results, axis = 0)
print('Simulating %d random networks gave the following means/expectations:' % n)
print(means)
print("Now lets analytically determine the expections:")
means_er = sma.expected3EMotifs(G, array = True)
print(means_er)