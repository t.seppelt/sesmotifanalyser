#include "changestats.users.h"
#include "stdbool.h"

#define SOC_NODE 1
#define ECO_NODE 0

/**
* Change statistics for counting open triangles.
* The term takes one paremeter which specifies which type
* the point of the triangle, i.e. the distinct single node
* must have.
* 
* When the type is set to 0 (ECO_NODE) the 3E-motif I.C is counted.
* Otherwise the function returns the number of 3S-motifs I.C.
*/
CHANGESTAT_FN(d_openTriangles) {
  Vertex t, h, other, point, notPoint;
  int i;
  Edge e;
  int pointType = INPUT_PARAM[0];
  
  ZERO_ALL_CHANGESTATS(i);
  FOR_EACH_TOGGLE(i) {
    t = TAIL(i); h = HEAD(i);
    if(INPUT_PARAM[t] == pointType && INPUT_PARAM[h] == pointType) {
      // both must be points, doesn't matter
    } else if(INPUT_PARAM[t] != pointType && INPUT_PARAM[h] != pointType) {
      STEP_THROUGH_OUTEDGES(t, e, other) {
        if(INPUT_PARAM[other] == pointType && IS_UNDIRECTED_EDGE(h, other)) {
          CHANGE_STAT[0] += IS_UNDIRECTED_EDGE(t, h) ? +1 : -1;
        }
      }
      STEP_THROUGH_INEDGES(t, e, other) {
        if(INPUT_PARAM[other] == pointType && IS_UNDIRECTED_EDGE(h, other)) {
          CHANGE_STAT[0] += IS_UNDIRECTED_EDGE(t, h) ? +1 : -1;
        }
      }
    } else {
      if(INPUT_PARAM[t] == pointType) {
        point = t, notPoint = h;
      }else{
        point = h, notPoint = t;
      }
      
      STEP_THROUGH_OUTEDGES(point, e, other) {
        if(other == notPoint || INPUT_PARAM[other] == pointType) {
          continue;
        }
        if(IS_UNDIRECTED_EDGE(notPoint, other)) {
          // cannot be an open triangle
        }else{
          CHANGE_STAT[0] += IS_UNDIRECTED_EDGE(point, notPoint) ? -1 : +1;
        }
      }
      STEP_THROUGH_INEDGES(point, e, other) {
        if(other == notPoint || INPUT_PARAM[other] == pointType) {
          continue;
        }
        if(IS_UNDIRECTED_EDGE(notPoint, other)) {
          // cannot be an open triangle
        }else{
          CHANGE_STAT[0] += IS_UNDIRECTED_EDGE(point, notPoint) ? -1 : +1;
        }
      }
      // neighb||s of notPoint don't matter
    }
    
    TOGGLE_IF_MORE_TO_COME(i);
  }
  UNDO_PREVIOUS_TOGGLES(i);
}

/**
* Change statistics for counting closed triangles.
* The term takes one paremeter which specifies which type
* the point of the triangle, i.e. the distinct single node
* must have.
* 
* When the type is set to 0 (ECO_NODE) the 3E-motif II.C is counted.
* Otherwise the function returns the number of 3S-motifs II.C.
*/
CHANGESTAT_FN(d_closedTriangles) {
  Vertex t, h, other, point, notPoint;
  int i;
  Edge e;
  int pointType = INPUT_PARAM[0];
  
  ZERO_ALL_CHANGESTATS(i);
  FOR_EACH_TOGGLE(i) {
    t = TAIL(i); h = HEAD(i);
    if(INPUT_PARAM[t] == pointType && INPUT_PARAM[h] == pointType) {
      // both must be points, doesn't matter
    } else if(INPUT_PARAM[t] != pointType && INPUT_PARAM[h] != pointType) {
      STEP_THROUGH_OUTEDGES(t, e, other) {
        if(INPUT_PARAM[other] == pointType && IS_UNDIRECTED_EDGE(h, other)) {
          CHANGE_STAT[0] += IS_UNDIRECTED_EDGE(t, h) ? -1 : +1; // diff: closing +1
        }
      }
      STEP_THROUGH_INEDGES(t, e, other) {
        if(INPUT_PARAM[other] == pointType && IS_UNDIRECTED_EDGE(h, other)) {
          CHANGE_STAT[0] += IS_UNDIRECTED_EDGE(t, h) ? -1 : +1; // diff: closing +1
        }
      }
    } else {
      if(INPUT_PARAM[t] == pointType) {
        point = t, notPoint = h;
      }else{
        point = h, notPoint = t;
      }
      
      STEP_THROUGH_OUTEDGES(point, e, other) {
        if(other == notPoint || INPUT_PARAM[other] == pointType) {
          continue;
        }
        if(IS_UNDIRECTED_EDGE(notPoint, other)) {
          // diff: can be closed triangle in this case
          CHANGE_STAT[0] += IS_UNDIRECTED_EDGE(point, notPoint) ? -1 : +1;
        }else{
          // diff: cannot be closed triangle
        }
      }
      STEP_THROUGH_INEDGES(point, e, other) {
        if(other == notPoint || INPUT_PARAM[other] == pointType) {
          continue;
        }
        if(IS_UNDIRECTED_EDGE(notPoint, other)) {
          // diff: can be closed triangle in this case
          CHANGE_STAT[0] += IS_UNDIRECTED_EDGE(point, notPoint) ? -1 : +1;
        }else{
          // diff: cannot be closed triangle
        }
      }
      // neighb||s of notPoint don't matter
    }
    
    TOGGLE_IF_MORE_TO_COME(i);
  }
  UNDO_PREVIOUS_TOGGLES(i);
}

/**
* Change statistics for edges per domain. The function
* returns three values representing the number of
* edges between two social, two ecological and one social and 
* one ecological node.
* 
* Don't use this term together with edges, since they are highly
* dependent.
*/
CHANGESTAT_FN(d_edgesPerDomain) {
  Vertex t, h;
  int i;
  
  ZERO_ALL_CHANGESTATS(i);
  FOR_EACH_TOGGLE(i) {
    t = TAIL(i); h = HEAD(i);
    if(INPUT_PARAM[t-1] == ECO_NODE && INPUT_PARAM[h-1] == ECO_NODE) {
      CHANGE_STAT[0] += IS_UNDIRECTED_EDGE(t,h) ? -1 : +1;
    }else if(INPUT_PARAM[t-1] == SOC_NODE && INPUT_PARAM[h-1] == SOC_NODE) {
      CHANGE_STAT[2] += IS_UNDIRECTED_EDGE(t,h) ? -1 : +1;
    }else{
      CHANGE_STAT[1] += IS_UNDIRECTED_EDGE(t,h) ? -1 : +1;
    }
    TOGGLE_IF_MORE_TO_COME(i);
  }
  UNDO_PREVIOUS_TOGGLES(i);
}

/**
* function for classifying 4-motifs. A list of booleans
* specifying whether one of the 6 distinct edges exist in a 4-motif
* is mapped to a numerical code of the motif's class counting
* I.A to VII.D (0...27).
*/
int classify4Motif(bool s1s2,
                   bool s2e2,
                   bool e1e2,
                   bool s1e1,
                   bool s1e2,
                   bool s2e1) {
  // compute configuration code
  unsigned int digits = 0;
  digits = digits * 2 + s1s2;
  digits = digits * 2 + s2e2;
  digits = digits * 2 + e1e2;
  digits = digits * 2 + s1e1;
  digits = digits * 2 + s1e2;
  digits = digits * 2 + s2e1;
  
  if(digits == 0b000000) {
    return 12; // IV.A
  } 
  if(digits == 0b000100 ||
     digits == 0b010000 ||
     digits == 0b000001 ||
     digits == 0b000010) {
    return 24; // 'VII.A'
  }
  if(digits == 0b001000) {
    return 14; // 'IV.C'
  }
  if(digits == 0b100000) {
    return 13; // 'IV.B' 
  }
  if(digits == 0b000101 ||
     digits == 0b010010) {
    return 4; // 'II.A'
  }
  if(digits == 0b000110 ||
     digits == 0b010001) {
    return 26; // 'VII.C'
  }
  if(digits == 0b010100 ||
     digits == 0b000011) {
    return 0; // 'I.A'
  }
  if(digits == 0b001100 ||
     digits == 0b011000 ||
     digits == 0b001010 ||
     digits == 0b001001){
    return 25; //'VII.B'
  }
  if(digits == 0b100100 ||
     digits == 0b100001 ||
     digits == 0b100010 ||
     digits == 0b110000) {
    return 20; // 'VI.A'
  }
  if(digits == 0b101000) {
    return 15; // 'IV.D'
  }
  if(digits == 0b001101 ||
     digits == 0b011010) {
    return 6; // 'II.C'
  }
  if(digits == 0b100101 ||
     digits == 0b110010) {
    return 5; // 'II.B'
  }
  if(digits == 0b101100 ||
     digits == 0b111000 ||
     digits == 0b101001 ||
     digits == 0b101010) {
    return 21; // 'VI.B'
  }
  if(digits == 0b001110 ||
     digits == 0b011001) {
    return 27; // 'VII.D'
  }
  if(digits == 0b010110 ||
     digits == 0b010011 ||
     digits == 0b010101 ||
     digits == 0b000111){
    return 16; // 'V.A'
  }
  if(digits == 0b100110 ||
     digits == 0b110001){
    return 22; // 'VI.C' 
  }
  if(digits == 0b011100 ||
     digits == 0b001011){
    return 2; // 'I.C'
  }
  if(digits == 0b110100 ||
     digits == 0b100011){
    return 1; // 'I.B'
  }
  if(digits == 0b010111) {
    return 8; // 'III.A'
  }
  if(digits == 0b101101 ||
     digits == 0b111010){
    return 7; // 'II.D'
  }
  if(digits == 0b011110 ||
     digits == 0b011011 ||
     digits == 0b011101 ||
     digits == 0b001111){
    return 18; // 'V.C'
  }
  if(digits == 0b101110 ||
     digits == 0b111001) {
    return 23; // 'VI.D'
  }
  if(digits == 0b110110 ||
     digits == 0b110011 ||
     digits == 0b110101 ||
     digits == 0b100111){
    return 17; // 'V.B'
  }
  if(digits == 0b111100 ||
     digits == 0b101011) {
    return 3; // 'I.D'
  }
  if(digits == 0b011111) {
    return 10; // 'III.C'
  }
  if(digits == 0b110111){
    return 9; // 'III.B'
  }
  if(digits == 0b111110 ||
     digits == 0b111101 ||
     digits == 0b101111 ||
     digits == 0b111011) {
    return 19; // 'V.D'
  }
  if(digits == 0b111111) {
    return 11; // 'III.D'
  }
  return -1;  
}


#define fourMotifsInputOffset -1

CHANGESTAT_FN(d_fourMotifs) {
  Vertex t, h, s1, s2, e1, e2;
  int i;
  
  int motifClassBefore, motifClassToggled;
  
  ZERO_ALL_CHANGESTATS(i);
  FOR_EACH_TOGGLE(i) {
    t = TAIL(i); h = HEAD(i);
    for(s1 = 1; s1 <= N_NODES; s1++) {
      if(INPUT_PARAM[s1 + fourMotifsInputOffset] == SOC_NODE) {
        for(s2 = 1; s2 < s1; s2++) {
          if(INPUT_PARAM[s2 + fourMotifsInputOffset] == SOC_NODE) {
            // now s1 and s2 are both social nodes
            for(e1 = 1; e1 <= N_NODES; e1++) {
              if(INPUT_PARAM[e1 + fourMotifsInputOffset] == ECO_NODE) {
                for(e2 = 1; e2 < e1; e2++) {
                  if(INPUT_PARAM[e2 + fourMotifsInputOffset] == ECO_NODE) {
                    // now e1 and e2 are both ecological nodes
                    if((t == e1 || t == e2 || t == s1 || t == s2) &&
                       (h == e1 || h == e2 || h == s1 || h == s2)) {
                      motifClassBefore = classify4Motif(IS_INEDGE(s1,s2),
                                                        IS_UNDIRECTED_EDGE(s2,e2),
                                                        IS_INEDGE(e1,e2),
                                                        IS_UNDIRECTED_EDGE(s1,e1),
                                                        IS_UNDIRECTED_EDGE(s1,e2),
                                                        IS_UNDIRECTED_EDGE(s2,e1));
                      motifClassToggled = classify4Motif((t == s1 || t == s2) && (h == s1 || h == s2) ? ! IS_OUTEDGE(t,h) : IS_INEDGE(s1,s2),
                                                         (t == s2 || t == e2) && (h == s2 || h == e2) ? ! IS_OUTEDGE(t,h) : IS_UNDIRECTED_EDGE(s2,e2),
                                                         (t == e1 || t == e2) && (h == e1 || h == e2) ? ! IS_OUTEDGE(t,h) : IS_INEDGE(e1,e2),
                                                         (t == s1 || t == e1) && (h == s1 || h == e1) ? ! IS_OUTEDGE(t,h) : IS_UNDIRECTED_EDGE(s1,e1),
                                                         (t == s1 || t == e2) && (h == s1 || h == e2) ? ! IS_OUTEDGE(t,h) : IS_UNDIRECTED_EDGE(s1,e2),
                                                         (t == s2 || t == e1) && (h == s2 || h == e1) ? ! IS_OUTEDGE(t,h) : IS_UNDIRECTED_EDGE(s2,e1));
                      CHANGE_STAT[motifClassBefore]--;
                      CHANGE_STAT[motifClassToggled]++;
                    }
                  }
                }
              }
            }
          }
        }        
      }
    }
    TOGGLE_IF_MORE_TO_COME(i);
  }
  UNDO_PREVIOUS_TOGGLES(i);
}

#define fourMotifInputOffset 0

CHANGESTAT_FN(d_fourMotif) {
  Vertex t, h, s1, s2, e1, e2;
  int i;
  int motifClass = INPUT_PARAM[0];
  int motifClassBefore, motifClassToggled;
  
  ZERO_ALL_CHANGESTATS(i);
  FOR_EACH_TOGGLE(i) {
    t = TAIL(i); h = HEAD(i);
    for(s1 = 1; s1 <= N_NODES; s1++) {
      if(INPUT_PARAM[s1 + fourMotifInputOffset] == SOC_NODE) {
        for(s2 = 1; s2 < s1; s2++) {
          if(INPUT_PARAM[s2 + fourMotifInputOffset] == SOC_NODE) {
            // now s1 and s2 are both social nodes
            for(e1 = 1; e1 <= N_NODES; e1++) {
              if(INPUT_PARAM[e1 + fourMotifInputOffset] == ECO_NODE) {
                for(e2 = 1; e2 < e1; e2++) {
                  if(INPUT_PARAM[e2 + fourMotifInputOffset] == ECO_NODE) {
                    // now e1 and e2 are both ecological nodes
                    if((t == e1 || t == e2 || t == s1 || t == s2) &&
                       (h == e1 || h == e2 || h == s1 || h == s2)) {
                      motifClassBefore = classify4Motif(IS_INEDGE(s1,s2),
                                                        IS_UNDIRECTED_EDGE(s2,e2),
                                                        IS_INEDGE(e1,e2),
                                                        IS_UNDIRECTED_EDGE(s1,e1),
                                                        IS_UNDIRECTED_EDGE(s1,e2),
                                                        IS_UNDIRECTED_EDGE(s2,e1));
                      motifClassToggled = classify4Motif((t == s1 || t == s2) && (h == s1 || h == s2) ? ! IS_OUTEDGE(t,h) : IS_INEDGE(s1,s2),
                                                         (t == s2 || t == e2) && (h == s2 || h == e2) ? ! IS_OUTEDGE(t,h) : IS_UNDIRECTED_EDGE(s2,e2),
                                                         (t == e1 || t == e2) && (h == e1 || h == e2) ? ! IS_OUTEDGE(t,h) : IS_INEDGE(e1,e2),
                                                         (t == s1 || t == e1) && (h == s1 || h == e1) ? ! IS_OUTEDGE(t,h) : IS_UNDIRECTED_EDGE(s1,e1),
                                                         (t == s1 || t == e2) && (h == s1 || h == e2) ? ! IS_OUTEDGE(t,h) : IS_UNDIRECTED_EDGE(s1,e2),
                                                         (t == s2 || t == e1) && (h == s2 || h == e1) ? ! IS_OUTEDGE(t,h) : IS_UNDIRECTED_EDGE(s2,e1));
                      if(motifClass == motifClassBefore) {
                        CHANGE_STAT[0]--;
                      }
                      if(motifClass == motifClassToggled) {
                        CHANGE_STAT[0]++;
                      }
                    }
                  }
                }
              }
            }
          }
        }        
      }
    }
    TOGGLE_IF_MORE_TO_COME(i);
  }
  UNDO_PREVIOUS_TOGGLES(i);
}

int classify3Motif(bool b1b2, bool ab1, bool ab2) {
  return b1b2 * 3 + ab1 + ab2;
}

#define threeMotifsInputOffset 0

CHANGESTAT_FN(d_threeMotifs) {
  Vertex t, h, a, b1, b2;
  int i;
  
  int pointType = INPUT_PARAM[0];
  int motifClassBefore, motifClassToggled;
  
  ZERO_ALL_CHANGESTATS(i);
  FOR_EACH_TOGGLE(i) {
    t = TAIL(i); h = HEAD(i);
    for(a = 1; a <= N_NODES; a++) {
      if(INPUT_PARAM[a + threeMotifsInputOffset] == pointType) {
        for(b1 = 1; b1 <= N_NODES; b1++) {
          if(INPUT_PARAM[b1 + threeMotifsInputOffset] != pointType) {
            for(b2 = 1; b2 < b1; b2++) {
              if(INPUT_PARAM[b2 + threeMotifsInputOffset] != pointType) {
                if((t == a || t == b1 || t == b2) && (h == a || h == b1 || h == b2 )) {
                  motifClassBefore = classify3Motif(IS_INEDGE(b1, b2),
                                                    IS_UNDIRECTED_EDGE(a, b1),
                                                    IS_UNDIRECTED_EDGE(a, b2));
                  motifClassToggled =classify3Motif((t == b1 || t == b2) && (h == b1 || h == b2) ? ! IS_OUTEDGE(t, h) : IS_INEDGE(b1, b2),
                                                    (t == a  || t == b1) && (h == a  || h == b1) ? ! IS_OUTEDGE(t, h) : IS_UNDIRECTED_EDGE(a, b1),
                                                    (t == a  || t == b2) && (h == a  || h == b2) ? ! IS_OUTEDGE(t, h) : IS_UNDIRECTED_EDGE(a, b2));
                  CHANGE_STAT[motifClassBefore]--;
                  CHANGE_STAT[motifClassToggled]++;
                }
              }
            }
          }
        }
      }
    }
    TOGGLE_IF_MORE_TO_COME(i);
  }
  UNDO_PREVIOUS_TOGGLES(i);
}

#define threeMotifInputOffset 1

CHANGESTAT_FN(d_threeMotif) {
  Vertex t, h, a, b1, b2;
  int i;
  
  int pointType = INPUT_PARAM[0];
  int motifClass = INPUT_PARAM[1];
  int motifClassBefore, motifClassToggled;
  
  ZERO_ALL_CHANGESTATS(i);
  FOR_EACH_TOGGLE(i) {
    t = TAIL(i); h = HEAD(i);
    for(a = 1; a <= N_NODES; a++) {
      if(INPUT_PARAM[a + threeMotifInputOffset] == pointType) {
        for(b1 = 1; b1 <= N_NODES; b1++) {
          if(INPUT_PARAM[b1 + threeMotifInputOffset] != pointType) {
            for(b2 = 1; b2 < b1; b2++) {
              if(INPUT_PARAM[b2 + threeMotifInputOffset] != pointType) {
                if((t == a || t == b1 || t == b2) && (h == a || h == b1 || h == b2 )) {
                  motifClassBefore = classify3Motif(IS_INEDGE(b1, b2),
                                                    IS_UNDIRECTED_EDGE(a, b1),
                                                    IS_UNDIRECTED_EDGE(a, b2));
                  motifClassToggled =classify3Motif((t == b1 || t == b2) && (h == b1 || h == b2) ? ! IS_OUTEDGE(t, h) : IS_INEDGE(b1, b2),
                                                    (t == a  || t == b1) && (h == a  || h == b1) ? ! IS_OUTEDGE(t, h) : IS_UNDIRECTED_EDGE(a, b1),
                                                    (t == a  || t == b2) && (h == a  || h == b2) ? ! IS_OUTEDGE(t, h) : IS_UNDIRECTED_EDGE(a, b2));
                  if(motifClass == motifClassBefore) {
                    CHANGE_STAT[0]--;
        				  }
        				  if(motifClass == motifClassToggled) {
        					  CHANGE_STAT[0]++;
        				  }
                }
              }
            }
          }
        }
      }
    }
    TOGGLE_IF_MORE_TO_COME(i);
  }
  UNDO_PREVIOUS_TOGGLES(i);
}
