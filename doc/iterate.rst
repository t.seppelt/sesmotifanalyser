Advanced Iteration
==================

Motif iterators are a sophisticated way for modelling sets of motifs. They
can be used for 3- and 4-motifs and even for plain sets of nodes (1-motifs).
These sets of motifs consist of a source (where the motifs are taken from)
and conditions (which refine the source). Several sources and conditions
are provided in this package. Users might extend them. See :py:class:`sma.MotifIterator`
for an introduction to the concept and to the technical background.

.. inheritance-diagram:: sma.iterate
	:caption: Inheritance diagram of the motif iterators implemented in this
		module. Note, that all classes can be accessed by simply importing
		sma. Importing sma.iterate is not necessary as it is imported implicitely.
		
Example:

.. code-block :: Python

	import sma
	
	# some random graph
	G = next(sma.randomSENs(10,5, socName = 'City'))
	
	# how many 4-motifs of class II.C exist with node 'City 0'?
	it = sma.FourMotifs(G) & sma.is4Class('II.C') & sma.hasNode('City 0')
	print(len(list(it))) 
	
These iteratators can be piped into the motif classification methods :py:meth:`sma.count4Motifs`
or :py:meth:`sma.count3Motifs`:

.. code-block :: Python

	import sma
	
	# some random graph
	G = next(sma.randomSENs(10,5, socName = 'City'))
	
	# get the number of occurences of the motif classes which contain
	# the node 'City 0':
	it = sma.FourMotifs(G) & sma.hasNode('City 0')
	print(sma.count4Motifs(G, it)) 



Abstract classes
----------------

.. autoclass :: sma.MotifIterator
	:members:
	
.. autoclass :: sma.ComplexMotifIterator
	:members:

.. autoclass :: sma.ConditionMotifIterator
	:members:
	
.. autoclass :: sma.SourceMotifIterator
	:members:

.. _sec-motif-sources:

Motif sources
-------------

.. autoclass :: sma.MultiMotifs
    :members:
.. autoclass :: sma.MultiMotifsNormalized
    :members:
.. autoclass :: sma.FourMotifs
	:members:
.. autoclass :: sma.ThreeEMotifs
	:members:
.. autoclass :: sma.ThreeSMotifs
	:members:
.. autoclass :: sma.OneMotifs
	:members:
.. autoclass :: sma.TwoMotifs
	:members:
.. autoclass :: sma.DenseMulti221Motifs
    :members:
.. autoclass :: sma.Dense13Motifs
    :members:

Conditions
----------
.. autoclass :: sma.hasProperty
	:members:
.. autoclass :: sma.hasPropertyGreaterThan
	:members:
.. autoclass :: sma.hasPropertyMatching
	:members:
.. autoclass :: sma.hasNode
	:members:
.. autoclass :: sma.isClass
    :members:
.. autoclass :: sma.is4Class
	:members:
.. autoclass :: sma.is3Class
	:members:
.. autoclass :: sma.matchesPropertyPattern4Motif
    :members:
.. autoclass :: sma.matchesPropertyPattern4MotifGreaterThan
    :members:
.. autoclass :: sma.matchesPropertyPattern3Motif
    :members:
.. autoclass :: sma.matchesPropertyPattern3MotifGreaterThan
    :members: