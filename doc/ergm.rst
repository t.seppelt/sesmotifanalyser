Terms for ERGM
==============

This section describes the terms which have been added to R's ``ergm`` 
package in order support the analysis of SES. See the documentation of 
the R packages ``statnet``, ``ergm`` for ``ergm.userterms`` for mathematical
and technical details.

All terms work only for undirected graphs. All of them have a ``typeAttr``
parameter which must be set to the name of the nodal attribute which states
whether a node is ecological or social. This attribute must be either 0 for
ecological nodes or 1 for social nodes. This guarantees compatability with
the Python functions described in the other sections. On the Python side,
this parameter is per default called ``sesType``. Following this naming convention
is strongly recommended.

The following terms are included in this package:

	- ``edgesPerDomain(typeAttr = 'sesType')`` adds three change statistics
	  counting the number of edges in each "domain", i.e. number of edges
	  linking social and social, ecological and ecological and social and ecological
	  nodes. See :py:meth:`sma.edgesCount` for a similar Python function.
	- ``openTriangles(pointType, typeAttr = 'sesType')`` adds one change 
	  statistic counting the number of open triangles (3-motifs I.C).
	  ``pointType`` is the social-ecological type of the distinct node at
	  the point of the triangle. Set this parameter to 0 to count the 3E-motif
	  I.C and to 1 to count the 3S-motif I.C.
	- ``closedTriangles(pointType, typeAttr = 'sesType')`` same as ``openTriangles``,
	  but for counting II.C motifs (closed triangles).
	- ``threeMotifs(pointType, typeAttr = 'sesType')`` adds 6 change statistics
	  for the number of all 3-motifs (I.A, ..., II.C). ``pointType`` determines
	  the social-ecological type of the distinct node. Set it to 0 to get
	  the counts of 3E-motifs or to 1 to get the counts of 3S-motifs. Note,
	  that this is a brute-force implementation. If you are just interested
	  in the number of open or closed triangles (I.C or II.C motifs), use
	  the terms ``openTriangles`` or ``closedTriangles`` which are implemented
	  much more elegantly. 
	- ``threeMotif(pointType, class, typeAttr = 'sesType')`` adds one change statistic
	  for the number of the 3-motifs specified by ``class`` (as a string, e.g ``'I.C'``). 
	  ``pointType`` determines
	  the social-ecological type of the distinct node. Set it to 0 to get
	  the counts of 3E-motifs or to 1 to get the counts of 3S-motifs. Note,
	  that this is a brute-force implementation. If you are just interested
	  in the number of open or closed triangles (I.C or II.C motifs), use
	  the terms ``openTriangles`` or ``closedTriangles`` which are implemented
	  much more elegantly. Using this term is not more efficient than using
	  ``threeMotifs`` because all motifs are classified in either case.
	  Cf. :py:meth:`sma.count3EMotifs` and :py:meth:`sma.count3SMotifs`.
	- ``fourMotifs(typeAttr = 'sesType')`` adds 28 change statistics counting
	  the number of all 4-motifs (I.A, II.B, ..., VII.D). Note, that this
	  term is rather inefficiently implemented. In most cases, calling
	  :py:meth:`sma.count4Motifs` will be faster.
	- ``fourMotif(class, typeAttr = 'sesType')`` adds one change statistic
	  counting the number of motifs of the class defined by ``class`` (as a string, e.g ``'I.C'``). Thus,
	  this term returns one of the values computed by ``fourMotifs``. Due 
	  to the complex nature of the motifs, using this term is not faster than
	  computing the counts for all motifs. This term may be used for model 
	  fitting.
	  
The terms can be used in all steps for ERGM modelling. For example, an 
ERGM model can be fitted using the terms:

.. code-block :: R
	
	library(ergm.userterms)
	
	# let net be some network
	model <- ergm(net ~ edgesPerDomain())

The terms can also be used for computing the network characteristics they
represent:

.. code-block :: R
	
	# let net be some network
	summary(net ~ fourMotifs())
