#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import sma
import matplotlib.pyplot as plt
import networkx as nx

G1 = sma.loadDiSEN('test/data/digraph_adj.csv', 
                   'test/data/digraph_attr.csv',
                    delimiter = ',',
                    create_using = nx.DiGraph())
plt.figure()
sma.drawSEN(G1)
plt.savefig("test/data/digraph.svg")

G2 = sma.loadSEN('test/data/threelevel_adj.csv', 
                 'test/data/threelevel_attr.csv',
                 delimiter = ',')
plt.figure()
sma.drawSEN(G2)
plt.savefig("test/data/threelevel.svg")

G3 = sma.loadSEN('test/data/twolevel_adj.csv',
                 'test/data/twolevel_attr.csv', 
                  delimiter = ',')
plt.figure()
sma.drawSEN(G3)
plt.savefig("test/data/twolevel.svg")

G4 = sma.loadSEN('test/data/large_digraph_adj.csv',
                 'test/data/large_digraph_attr.csv', 
                  delimiter = ',',
                  create_using = nx.DiGraph())
plt.figure()
sma.drawSEN(G4)
plt.savefig("test/data/large_digraph.svg")
