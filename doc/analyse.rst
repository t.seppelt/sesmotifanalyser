Analyse SENs, Count and Classify Motifs
=======================================

Count motifs
------------

Front-end functions
+++++++++++++++++++

.. autofunction :: sma.countMotifsAuto
.. autofunction :: sma.countMultiMotifs
.. autofunction :: sma.countMultiMotifsSparse

Dense networks
++++++++++++++

This section lists the most basic counting functions.

.. autofunction :: sma.countMotifs
.. autofunction :: sma.count4Motifs
.. autofunction :: sma.count3Motifs
.. autofunction :: sma.count3EMotifs
.. autofunction :: sma.count3SMotifs
.. autofunction :: sma.count3pMotifsLinalg
.. autofunction :: sma.count2pMotifs

Sparse networks
+++++++++++++++

All functions based on :py:meth:`sma.countMotifs`, e.g. :py:meth:`sma.count3Motifs`,
iterate over all vertices to classify the motifs. For example, when classifying
motifs consisting of :math:`n` vertices the execution time of these functions
is roughly :math:`O(|V|^n)`. This is rather inefficient in most applications
since networks in nature tend to be sparse, i.e. have only few edges.

Some functions return partial results only. For example, :py:meth:`sma.count4MotifsSparse`
with default parameters cannot compute correct counts for motifs IV.A-IV.D. Read
the descriptions carefully.

Warning: The input graph must not contain self loops. Check using :py:meth:`networkx.nodes_with_selfloops`.

.. autofunction :: sma.count3MotifsSparse
.. autofunction :: sma.count3SMotifsSparse
.. autofunction :: sma.count3EMotifsSparse
.. autofunction :: sma.count4MotifsSparse
.. autofunction :: sma.count13MotifsSparse
.. autofunction :: sma.count121MotifsSparse
.. autofunction :: sma.count221MotifsSparse
.. autofunction :: sma.count222MotifsSparse

Auxiliary functions
++++++++++++++++++++

.. autofunction :: sma.findIdealCounter

Iterate through motifs
----------------------

In this section basic motif iterators are described. They are sufficient
for basal uses. See :py:class:`sma.MotifIterator` for more advanced iterators
and the functionalities for modelling abstract sets of motifs.

.. autofunction :: sma.iterate4Motifs
.. autofunction :: sma.iterate3Motifs
.. autofunction :: sma.iterate3EMotifs
.. autofunction :: sma.iterate3SMotifs
.. autofunction :: sma.sesSubgraph


Classify motifs
---------------
This section features first basal functions which map an instance of 
a SEN and a motif (tuple of vertices) to its motif class (e.g. 'II.B').
In the second subsection motif classificators are introduced. Classificators
are objects which wrap the former functions and provide additional structural
information.


Front-end functions
+++++++++++++++++++

.. autofunction :: sma.motifTable

.. _sec-motif-classificator:

Motif classificators
++++++++++++++++++++

.. autoclass :: sma.MotifClassificator
    :members:
.. autoclass :: sma.MultiMotifClassificator
.. autoclass :: sma.FourMotifClassificator
.. autoclass :: sma.ThreeMotifClassificator
.. autoclass :: sma.ThreePMotifClassificator
.. autoclass :: sma.TwoMotifClassificator
.. autoclass :: sma.TwoThreeMotifClassificator
.. autoclass :: sma.OneThreeMotifClassificator
.. autoclass :: sma.Multi111MotifClassificator
.. autoclass :: sma.Multi121MotifClassificator
.. autoclass :: sma.Multi221MotifClassificator
.. autoclass :: sma.Multi222MotifClassificator
.. autoclass :: sma.OneMotifClassificator
.. autoclass :: sma.DiMotif11Classificator
.. autoclass :: sma.DiMotif2Classificator
.. autoclass :: sma.DiMotif12Classificator
.. autoclass :: sma.DiMotif22Classificator

Basal functions
+++++++++++++++
.. autofunction :: sma.classify4Motif
.. autofunction :: sma.classify3Motif
.. autofunction :: sma.classify3pMotif
.. autofunction :: sma.classify2Motif
.. autofunction :: sma.classify23Motif
.. autofunction :: sma.classify13Motif
.. autofunction :: sma.classify111Motif
.. autofunction :: sma.classify121Motif
.. autofunction :: sma.classify221Motif
.. autofunction :: sma.classify222Motif

Analyse triangles and higher order nodal properties
---------------------------------------------------

.. autofunction :: sma.triangleCoefficient
.. autofunction :: sma.triangleCoefficients
.. autofunction :: sma.connectedOpposingSystem
.. autofunction :: sma.overlappingCoefficients

Co-occurrences
--------------

Co-occurrence is a way of describing the relations among motifs. Two motifs co-occur
whenever they share a common vertex. The number of these co-occurring vertices can
then be determined and is an indicator for meso-level properties of the SEN.

.. image :: _static/cooccurrence.pdf
	:align: center
	:width: 50%
	
In the above example two 3E-motifs co-occur. The II.C motif on the left and the II.A
motif on the right share a common vertex labelled as "master" in this example. Hence the
contribution of the above configuration to the II.A-II.C entry in the result of
:py:meth:`sma.cooccurrenceTableFull` would be 1. 

.. autofunction :: sma.cooccurrenceTable
.. autofunction :: sma.cooccurrenceTableFull
.. autofunction :: sma.cooccurrenceEdgeTableFull

Motif Graphs
------------

Motif Multigraph
++++++++++++++++
The motif multigraph contains one vertex for each of the original graph's motifs.
Contrarily, the motif class graph contains one vertex for each class of vertices.
Since any multigraph can be translated naturally to a weighted simple graph, both
graphs can be represented as such weighted simple graphs.

The following example demonstrates how :py:meth:`sma.motifMultigraph` is supposed
to be used:

.. code-block :: Python
    
    import sma
    # Let G be some SEN (here Madagascar dataset)
    motifs = [sma.ThreeEMotifs(G) & sma.is3Class('I.C'),
              sma.ThreeEMotifs(G) & sma.is3Class('II.C')]
    M = sma.motifWeightedGraph(G, *motifs)
    # some manipulations for plotting

The result is shown below. The multigraph contains one node for each 3E-motif
of class I.C (yellow) and II.C (green). Two motifs are connected with an edge if they share a vertex.
This edge is bold if not only one but two vertices are shared by the two motifs.

.. image :: _static/motifmultigraph.pdf
	:align: center
	:width: 50%


.. autofunction :: sma.motifMultigraph
.. autofunction :: sma.motifWeightedGraph

Motif Class Graph
+++++++++++++++++

The motif class graph contains one vertex for each class of motifs. The edges are
weighted by the sum of the numbers of shared vertices of distinct motifs of the 
respective classes.

The following example demonstrates the usage of :py:meth:`sma.cooccurrenceTableFull`
which lays the groundwork for :py:meth:`sma.motifClassMatrix` and :py:meth:`sma.motifClassGraph`.

.. code-block :: Python

    import sma    
    # Let G be some SEN, here Madagascar dataset
        
    mat1 = sma.cooccurrenceTableFull(G, 
                                     sma.ThreeEMotifs(G), 
                                     sma.ThreeMotifClassificator(G), 
                                     to_array=True)
    # one row for each vertex, one column for each motif class
    #   array([[13, 51,  6,  6, 16,  6],
    #          [44, 37,  3, 10,  2,  2],
    #          ...,
    #          [10, 11,  0,  5,  1,  1]])
    
    mat2 = sma.motifClassMatrix(G, 
                                sma.ThreeEMotifs(G), 
                                sma.ThreeMotifClassificator(G))
    # upper triangular matrix, one column/row for each motif class
    #   array([[ 6556, 11563,   465,  3938,  2202,   986],
    #          [    0,  5744,   641,  3726,  2612,  1131],
    #          ...
    #          [    0,     0,     0,     0,     0,    51]])
    
    G2 = sma.motifClassGraph(G, sma.ThreeEMotifs(G), sma.ThreeMotifClassificator(G))

The resulting graph ``G2`` is shown below. Thicker edges carry heavier weight. Loops
are hidden.

.. image :: _static/motifclassgraph.pdf
	:align: center
	:width: 50%

.. autofunction :: sma.motifClassMatrix
.. autofunction :: sma.motifClassGraph


Gaps
----

Flipping a dyad results in a changed number of motifs. When fixing a dyad on a specific
level, motifs might be grouped into pairs of open and closed motifs based on the
presence of the fixed dyad. Flipping the dyad turns a open motif into a closed motif
and vice versa. One might ask which dyads contribute the most to the amount of a
specific motif when being flipped. Functions in this section analyse the contribution
of these *gaps* to the number of motifs.

.. autofunction :: sma.identifyGaps
.. autofunction :: sma.isClosedMotif

.. _sec-distribution:

Distribution of motifs in Erdős-Rényi random graphs
---------------------------------------------------

The Erdős-Rényi model provides a straightforward idea for modelling random graphs.
Its basic assumption is that the edges in a random graph are chosen uniformly and
independently, i.e. an edge is present in a graph with fixed probability :math:`p`.

The Erdős-Rényi model fails to model more complicated processes that govern the 
creation of edges in most real world graphs. For example, it cannot be used to model
triadic closure.

Nevertheless, the model provides a starting point for a probabilistic analysis of a
SEN. Questions like "Are the numbers of 4-motifs in a given real world network higher/lower
than expected?" can be answered assuming that the set of all graphs the real world
network is compared with is only restricted by the density of the graph. In fact, 
the question should be reformulated as "Are the numbers of 4-motifs in a given real 
world network higher/lower than expected in a graph with same expected density?".

For our purposes we consider a refined version of the Erdős-Rényi model 
(:py:const:`sma.MODEL_ERDOS_RENYI`). Instead of fixing one probability we fix 
probabilities :math:`p_{ij}` for all pairs 
:math:`i \leq j \in \{0, \dots, n-1\}` where :math:`0, \dots, n-1` refer to the levels
in the SEN (e.g. 0 for ecological nodes, 1 for social nodes in a two level SEN). Then
the assumption is that edges linking levels :math:`i` and :math:`j` are chosen 
independently with probability :math:`p_{ij}`. Hence the values :math:`p_{ij}` are
the densities of the respective subsystem.

Under these assumptions we are able to compute the expected numbers of motifs of a
given class analytically, i.e. without computing thousands of random graphs. The
functions described in this sections make use of the following observation:

**Mathematical Background** Fix a set of ecological nodes :math:`V_0` and a set 
of social nodes :math:`V_1` and let :math:`\mathcal{G}` denote the set of all graphs 
with vertex set :math:`V_0 \cup V_1`. Turn :math:`\mathcal{G}` into a probability 
space by taking a (refined) Erdős-Rényi model on two levels with probabilities :math:`p_{00}, p_{01}, p_{11}` 
where 0 refers to the ecological level and 1 to the social level. We want to compute the expected
number of 3E-motifs of class I.C (open triangle). Let :math:`X: \mathcal{G} \to \mathbb{N}`
be a random variable mapping a graph to the number of I.C motifs that it contains.
We observe that :math:`X(G) = \sum_{v_0 \in V_0} \sum_{v_1 \neq v_2 \in V_1} Y(v_0, v_1, v_2; G)`
where :math:`Y(v_0, v_1, v_2; G)` is one whenever the given triple forms a I.C motif in
:math:`G` and zero otherwise. Then by linearity of the expectation the following holds,

.. math ::
    
    \mathbb{E} \left[ X(G) \right] 
    &= \mathbb{E} \left[ \sum_{v_0 \in V_0} \sum_{v_1 \neq v_2 \in V_1} Y(v_0, v_1, v_2; G) \right] \\
    &= \sum_{v_0 \in V_0} \sum_{v_1 \neq v_2 \in V_1} \mathbb{E} \left[ Y(v_0, v_1, v_2; G) \right] \\
    &= \sum_{v_0 \in V_0} \sum_{v_1 \neq v_2 \in V_1} \mathbb{P}(\text{edge } v_0, v_1) \mathbb{P}(\text{edge } v_0, v_2) \mathbb{P}(\text{no edge } v_1, v_2)  \\
    &= \sum_{v_0 \in V_0} \sum_{v_1 \neq v_2 \in V_1} p_{01}^2 (1-p_{11})  \\
    &= |V_0| \binom{|V_1|}{2} p_{01}^2 (1-p_{11}).

Note that :math:`|V_0| \binom{|V_1|}{2}` represents the total number of 3E-motifs in the graph.

The functions in this section can either compute densities, i.e. number of motifs
divided by the total number of motifs, e.g. :math:`p_{01}^2 (1-p_{11})` in this 
example, or the expected number of motifs as above.

It is also possible to compute the variance :math:`\mathbb{V}[X]` in this way. In 
particular, after having computed the expectation as above it remains to compute 
the second moment :math:`\mathbb{E}[X^2]` since the variance satisfies 
:math:`\mathbb{V}[X] = \mathbb{E}[X^2] - (\mathbb{E}[X])^2`. Again, by linearity
of the expectation,

.. math::
    
    \mathbb{E} \left[ X(G)^2 \right] 
    &= \mathbb{E} \left[ \left( \sum_{v_0 \in V_0} \sum_{v_1 \neq v_2 \in V_1} Y(v_0, v_1, v_2; G) \right)^2 \right] \\
    &= \mathbb{E} \left[ \sum_{v_0 \in V_0} \sum_{v_1 \neq v_2 \in V_1} \sum_{v'_0 \in V_0} \sum_{v'_1 \neq v'_2 \in V_1} Y(v_0, v_1, v_2; G)Y(v'_0, v'_1, v'_2; G) \right] \\
    &= \sum_{v_0 \in V_0} \sum_{v_1 \neq v_2 \in V_1} \sum_{v'_0 \in V_0} \sum_{v'_1 \neq v'_2 \in V_1} \mathbb{E} \left[ Y(v_0, v_1, v_2; G)Y(v'_0, v'_1, v'_2; G) \right].

At this point, are more subtle argument is requirement because the expectation is
not multiplicative. The fourfold sum is over :math:`\left(|V_0| \binom{|V_1|}{2}\right)^2`
choices for :math:`v_0,v_1,v_2, v'_0, v'_1, v'_2`. We split the sum according to
:math:`\{v_0,v_1,v_2\} \cap \{v'_0, v'_1, v'_2\}` and compute the individual probabilities.
In this way, we obtain the desired second moment, see :ref:`sec-appendix-erdos`
for details.

**Comparison with SEN generators** Functions like :py:meth:`sma.randomSENs` and
:py:meth:`sma.randomMultiSENs` can be used to compute random graphs with same number
of edges (per subsystems) as a given graph. For the purpose of this package this
model is called *Fixed Densities Model* (:py:const:`sma.MODEL_FIXED_DENSITIES`).

Note that this approach and the (refined) Erdős-Rényi model are slightly different. 
:py:meth:`sma.randomSENs` computes only graphs with exactly the same amount of 
edges (per subsytems) assuming a uniform distribution on the set of all these graphs. 
In Erdős-Rényi the graphs have only in expectation the same amount of edges. 
This relaxation leads the combinatorial argument above. Using Erdős-Rényi is 
therefore roughly equivalent to the ERGM model 
``ergm(G ~ nodemix(each combination of levels in the network))`` in R, cf. :ref:`sec-rbridge`.

**The Actor's Choice Model** Besides the general Erdős-Rényi model which is outlined
above, a more refined baseline model, the *Actor's Choice Model* is supported. In
this model, it is assumed that all but one subsystem (the actor subsystem) are fixed.
The edges in the actor subsystem are chosen independently and uniformly with a certain
fixed probability. In other words, only the edges between actors are assumed to be random.
Edges linking actors with other levels of the network and dyads in these other levels
are assumed to be fixed. This allows for an analysis which better models decision
making processes in the actor level.

Characteristics of Actor's Choice distribution can be computed using 
:py:meth:`sma.distributionMotifsActorsChoice` and the front-end function 
:py:meth:`sma.distributionMotifsAuto`. The mathematical justification for these
methods is as follows: Let for an edge :math:`e` in the actor level :math:`C(e)`
denote the contribution of an edge to a pair of open/closed motifs, cf. :py:meth:`sma.edgeContributionList`. 
That is, the number
of times this edge occurs in a (fixed) motif :math:`M` or in the motif :math:`\bar M`
which differs from the fixed motif only in the edge on the actor level.
Suppose that :math:`M` contains an edge on the actor level (i.e., it is *closed*)
and that :math:`\bar M` does not contain any edge between actors (it is *open*).
Examples for such pairs of motifs are the open and closed ecological triangle (motifs I.C and II.C). 
Furthermore, let :math:`X` denote the random variable giving the number of closed
motifs of class :math:`M`. Let as above :math:`Y` denote the random variable on the
set of edges in the actor level giving :math:`1` if the edge occurs and :math:`0`
otherwise. Then,

.. math::
    
    \mathbb{E} \left[ X \right] 
    = \mathbb{E} \left[ \sum_{e} Y(e) C(e) \right]
    =  \sum_{e} \mathbb{E} \left[ Y(e) C(e) \right]
    =  \sum_{e} p C(e)
    = p \sum_{e} C(e),

where :math:`p` denotes the density of the actor level. Similarly,

.. math::
    
    \mathbb{V} \left[ X \right] 
    &= \mathbb{E} \left( \left[ \sum_{e} Y(e) C(e) \right] \right)^2 - \left( \mathbb{E}  \left[ \sum_{e} Y(e) C(e) \right] \right)^2 \\
    &= \sum_{e, e'} \mathbb{E} \left[ Y(e) C(e) Y(e') C(e') \right] - p^2 \sum_{e,e'} C(e)C(e') \\
    &= p^2 \sum_{e \neq e'} C(e) C(e') + p\sum_e C(e)^2 - p^2 \sum_{e,e'} C(e)C(e') \\
    &= p\sum_e C(e)^2 - p^2 \sum_{e} C(e)^2 \\
    &= p(1-p) \sum_{e} C(e)^2.



**References** See :cite:`ryder_distribution_2018`.

**Example** The functions in this section can replace expensive simulations of random graphs:

.. literalinclude:: ../examples/example_distribution.py

Front-end functions
+++++++++++++++++++

.. autofunction :: sma.expectedMultiMotifs
.. autofunction :: sma.varMultiMotifs
.. autofunction :: sma.distributionMotifsAuto

Erdős-Rényi expectations
++++++++++++++++++++++++

.. autofunction :: sma.expected3Motifs
.. autofunction :: sma.expected3EMotifs
.. autofunction :: sma.expected3SMotifs
.. autofunction :: sma.expected4Motifs
.. autofunction :: sma.expected121Motifs
.. autofunction :: sma.expected221Motifs
.. autofunction :: sma.expected222Motifs
.. autofunction :: sma.expected111Motifs

Erdős-Rényi variances
+++++++++++++++++++++

.. autofunction :: sma.var3Motifs
.. autofunction :: sma.var3EMotifs
.. autofunction :: sma.var3SMotifs
.. autofunction :: sma.var4Motifs

Actor's Choice Model
++++++++++++++++++++

.. autofunction :: sma.edgeContributionList
.. autofunction :: sma.distributionMotifsActorsChoice

Auxiliary functions
++++++++++++++++++++

.. autofunction :: sma.markovBound
.. autofunction :: sma.cantelliBound

.. _sec-simulate:

Simulate baselines
------------------

.. autofunction :: sma.simulateBaselineAuto


Other simple network properties
-------------------------------
.. autofunction :: sma.density
.. autofunction :: sma.densityMatrix
.. autofunction :: sma.nodesCount
.. autofunction :: sma.edgesCount
.. autofunction :: sma.edgesCountMatrix
.. autofunction :: sma.degreeDistribution
.. autofunction :: sma.nodesByType
.. autofunction :: sma.untypedNodes
.. autofunction :: sma.sesTypes
.. autofunction :: sma.adjacentEdgesCount
.. autofunction :: sma.total4Motifs
.. autofunction :: sma.total3EMotifs
.. autofunction :: sma.total3SMotifs
.. autofunction :: sma.totalMultiMotifs