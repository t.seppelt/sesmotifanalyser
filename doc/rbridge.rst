.. _sec-rbridge:

Integration in R
================

Many functions of this package are made available in R by the R package
`motifr <https://marioangst.github.io/motifr/>`_. See its documentation for a
user-friendly introduction. This section describes the technicalities of the
underlying interface.

All functions provided by this module can be used in R. On the R side
we use the ``statnet`` package for modelling networks. The ``reticulate``
package is used for making the Python functions accessible in R.

Note, that this is written in Python 3 and makes use of its language features.
Running it with Python 2 is not possible. Thus, the user is requested to
assure that Python 3 is installed and accessible.

The following code is needed for setting up the bridge between R and Python.
You may need to adjust the paths ll. 4, 6.

.. code-block :: r
	:linenos:

	library(statnet)

	# set-up python interface
	Sys.setenv(RETICULATE_PYTHON = "/usr/bin/python3") # ensure Python version 3
	library("reticulate")
	sma <<- import_from_path('sma', path = '.')

	toPyGraph <- function(g, typeAttr = 'sesType') {
		# function for translating a statnet network object into a Python  
		# compatible networkx object
		adjacencyMatrix = as.matrix(g)
		attributeNames  = list.vertex.attributes(g)
		attributeValues = lapply(list.vertex.attributes(g),function(x) get.vertex.attribute(g,x))

		sma$translateGraph(adjacencyMatrix, attributeNames, attributeValues, typeAttr)
	}

Since SENs -- and not arbitrary networks -- are considered, the social-ecological
type of the nodes is of crucial importance. Therefore, the user needs to specify
the nodal attribute which shall be interpreted as this type as second parameter of 
the function ``toPyGraph``. In the last line :py:meth:`sma.translateGraph` is called.
See its documentation for technical background information.

Note, that the social-ecological type must be modelled in accordance with the constants
:py:const:`sma.NODE_TYPE_ECO` and :py:const:`sma.NODE_TYPE_SOC`.

Example (cont.), cf. :py:meth:`sma.count4Motifs` and :py:meth:`sma.triangleCoefficients`:

.. code-block :: r

	# load graph
	load("firefighters.RData")
	g = firefighters # statnet network object from R data file

	# get python equivalent
	pyGraph = toPyGraph(g)

	# perform analysis
	sma$count4Motifs(pyGraph, array=TRUE) # count 4-motifs
	sma$triangleCoefficients(pyGraph, array=TRUE) # compute triangle coefficients

Some Python specific features used by the advanced iteration functions
are not available in R. The functions listed in the following section
can be used as substitute for these features.

Example:

.. code-block :: r
    
    set = sma$motifSet(sma$FourMotifs(pyGraph), sma$is4Class('II.C'), sma$hasNode('City 0'))
    # count 4-motifs of class II.C with node 'City 0'
    sma$countAnyMotifs(set)
    
Note, that a set object, as returned by :py:meth:`sma.motifSet`, can only be used
once. Functions like :py:meth:`sma.count4Motifs` or :py:meth:`sma.listMotifs` empty
the iterators when they are executed. 

Auxiliary functions
--------------------

.. autofunction :: sma.translateGraph
.. autofunction :: sma.countAnyMotifs
.. autofunction :: sma.motifSet
.. autofunction :: sma.listMotifs
.. autofunction :: sma.countMotifsAutoR
.. autofunction :: sma.distributionMotifsAutoR
.. autofunction :: sma.simulateBaselineAutoR
.. autofunction :: sma.identifyGapsR
.. autofunction :: sma.isClosedMotifR