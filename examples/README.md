# Examples

This folder contains examples which are meant to illustrate the usability of the SESMotifAnalyser.

* [``example.py``](./example.py) loads a dummy SEN from two CSV files and performs some calculations.
* The R file [``example.R``](./example.R) demonstrates how the Python scripts can be used within R projects.
* [``example_multilevel.py``](./example_multilevel.py) contains an analysis of a multilevel network as described in the documentation.
* [``example_distribution.py``](./example_distribution.py) illustrates how random SENs can be computed