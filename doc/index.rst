.. SESMotifAnalyser documentation master file, created by
   sphinx-quickstart on Thu Sep 27 10:49:42 2018.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to SESMotifAnalyser's documentation!
============================================

.. toctree::
	:maxdepth: 2
	:caption: Contents:

	intro
	multilevel
	ergm
	io
	generate
	analyse
	iterate
	rbridge
	auxiliary
	appendix_zoo
	appendix_erdos

