#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import unittest
import sma
import numpy, itertools
import pandas.testing
import networkx as nx

class TestPositionMatching(unittest.TestCase):
    def test_match_positions(self):
        values = [
                #  sign,   arities,  roles, expected
                ([1, 2],    [1, 2],     [], [0, 1]),
                ([1, 2], [0, 1, 2],     [], [1, 2]),
                ([1, 2], [0, 2, 1],     [], [2, 1]),
                ([1, 2], [2, 0, 1],     [], [2, 0]),
                ([1, 2],    [1, 2], [2, 1], [2, 1]),
                ([1, 2], [0, 1, 2], [1, 2], [1, 2]),
                ([1, 2], [0, 2, 1], [2, 1], [1, 2]),
                ([1, 2], [2, 0, 1], [2, 0], [0, 2]),
                ([1, 2],    [2, 1], [1, 0], [0, 1]), # critical case
                ([1, 2],    [1, 2], [1, 0], [1, 0]),
                ([2, 2], [0, 2, 2],     [], [1, 2])
                ]
        for signature, arities, roles, expected in values:
            with self.subTest(signature = signature, 
                              arities = arities, 
                              roles = roles, 
                              expected = expected):
                self.assertEqual(expected, sma.matchPositions(signature, arities, roles))
    def test_closed_open(self):
        self.assertEqual(sma.isClosedMotif('1,2[I.C]'), False)
        self.assertEqual(sma.isClosedMotif('1,2[II.C]'), True)
        self.assertEqual(sma.isClosedMotif('2,1[I.C]'), False)
        self.assertEqual(sma.isClosedMotif('2,1[II.C]'), True)
        self.assertEqual(sma.isClosedMotif('2:2,1:3[I.C]'), False)
        self.assertEqual(sma.isClosedMotif('2:2,1:3[II.C]'), True)
        self.assertEqual(sma.isClosedMotif('2:2,1:3[I.C]', level = 2), False)
        self.assertEqual(sma.isClosedMotif('2:2,1:3[II.C]', level = 2), True)
        self.assertEqual(sma.isClosedMotif('2,2[III.C]'), False)
        self.assertEqual(sma.isClosedMotif('2,2[III.C]', level = 1), True)
        self.assertEqual(sma.isClosedMotif('2:1,2:0[III.C]', level = 1), False)
        self.assertEqual(sma.isClosedMotif('2:1,2:0[III.C]', level = 0), True)
        self.assertEqual(sma.isClosedMotif('0,2,2[III.C]'), False)
        self.assertEqual(sma.isClosedMotif('0,2,2[III.C]', level = 1), False)
        self.assertEqual(sma.isClosedMotif('0,2,2[III.C]', level = 2), True)
        self.assertEqual(sma.isClosedMotif('2:3,2:4[III.C]', level = 3), False)
        self.assertEqual(sma.isClosedMotif('2:3,2:4[III.C]', level = 4), True)

class TestRandomGraphGenerator(unittest.TestCase):
    def test_two_level_number_vertices(self):
        for i in range(10):
            ecological = numpy.random.randint(1, 100)
            social = numpy.random.randint(1, 100)
            with self.subTest(ecological = ecological, social = social):
                G = next(sma.randomSENs(ecological, social))
                vertices = sma.nodesCount(G)
                self.assertEqual(vertices[sma.NODE_TYPE_ECO], ecological)
                self.assertEqual(vertices[sma.NODE_TYPE_SOC], social)
    def test_multi_level_number_vertices_edges(self):
        for i in range(10):
            nVertices = numpy.random.randint(1, 100, (3))
            with self.subTest(nVertices = nVertices):
                edges = sma.randomEdgeCountMatrix(nVertices)
                G = next(sma.randomMultiSENsFixedDensities(nVertices, edges))
                self.assertTrue(numpy.all(nVertices == sma.nodesCount(G, array = True)))
                self.assertTrue(numpy.all(edges == sma.edgesCountMatrix(G)))

class MotifTest(unittest.TestCase):
    def assertEqualDict(self, a, b, keys = None, msg = None):
        if keys is None:
            self.assertEqual(a, b, msg = msg)
        else:
            for k in keys:
                self.assertEqual(a[k], b[k], msg = msg)
    def assertMotifIdentifiers(self, expected, heads, motifs, keys_dense = None):
        for mid in heads:
            ids = list(map(lambda m : '%s[%s]' % (mid, m), motifs))
            
            with self.subTest(identifiers = ids):
                auto_partial_sparse, auto_total_sparse = sma.countMotifsAuto(self.G, 
                                                                             *ids, 
                                                                             assume_sparse = True)
                auto_partial_dense, auto_total_dense = sma.countMotifsAuto(self.G, 
                                                                           *ids, 
                                                                           assume_sparse = False)
                self.assertEqualDict(expected, 
                                     auto_total_sparse[mid], 
                                     keys_dense)
                self.assertEqual(expected, auto_total_dense[mid])
                for motif, identifier in zip(motifs, ids):
                    self.assertEqual(expected[motif], auto_partial_sparse[identifier])
                    self.assertEqual(expected[motif], auto_partial_dense[identifier])

class TestMotifCountingTwoLevel(MotifTest):
    """
    Tests based on a random two-level SEN genereated using
    
    .. code :: Python
        
        G = next(sma.randomSENs(20,15,.6))
        sma.saveSEN(G, 'test/data/twolevel_adj.csv', 'test/data/twolevel_attr.csv')
    
    """
    def setUp(self):
        self.G = sma.loadSEN('test/data/twolevel_adj.csv',
                             'test/data/twolevel_attr.csv', 
                             delimiter = ',')
    def test_number_vertices(self):
        vertices = sma.nodesCount(self.G)
        self.assertEqual(vertices[sma.NODE_TYPE_ECO], 20)
        self.assertEqual(vertices[sma.NODE_TYPE_SOC], 15)
    def test_type_vertices(self):
        self.assertEqual(sma.sesTypes(self.G), {0,1})
    def test_number_edges(self):
        edges = sma.edgesCount(self.G)
        self.assertEqual(edges, {0: 112, 1: 185, 2: 60})
        edges_matrix = sma.edgesCountMatrix(self.G)
        edges_matrix_expected = numpy.array([[112, 185],
                                             [  0,  60]])
        self.assertTrue(numpy.all(edges_matrix == edges_matrix_expected))
    def test_density(self):
        self.assertEqual(sma.density(self.G), 0.6)
        density_matrix_expected = numpy.array([[0.58947368, 0.61666667],
                                               [0.        , 0.57142857]])
        self.assertTrue(numpy.all(numpy.isclose(sma.densityMatrix(self.G), 
                                                density_matrix_expected)))
    def test_number_3E_motifs(self):
        expected = {'I.A': 145, 
                    'I.B': 428, 
                    'I.C': 327, 
                    'II.A': 172, 
                    'II.B': 548, 
                    'II.C': 480}
        self.assertEqual(sum(expected.values()), sma.total3EMotifs(self.G))
        self.assertEqual(expected, sma.count3EMotifs(self.G))
        self.assertEqual(expected, sma.count3EMotifsSparse(self.G))
        self.assertEqual(expected, sma.countMultiMotifs(self.G, 1, 2))
        self.assertEqual(expected, sma.countMultiMotifsSparse(self.G, 1, 2))
        self.assertEqual(expected, sma.countMultiMotifs(self.G, 1, 2, roles=[0, 1]))
        self.assertEqual(expected, sma.countMultiMotifsSparse(self.G, 1, 2, roles=[0, 1]))
        self.assertEqual(expected, sma.countMultiMotifs(self.G, 2, 1, roles=[1, 0]))
        self.assertEqual(expected, sma.countMultiMotifsSparse(self.G, 2, 1, roles=[1, 0]))
        
        # try several equivalent motif identifier strings
        self.assertMotifIdentifiers(expected, 
                                    ['1,2', '1:0,2:1', '2:1,1:0'], 
                                    ['II.B'])
        

    def test_number_3S_motifs(self):
        expected = {'I.A': 161, 
                    'I.B': 552, 
                    'I.C': 457, 
                    'II.A': 244, 
                    'II.B': 823, 
                    'II.C': 613}
        self.assertEqual(sum(expected.values()), sma.total3SMotifs(self.G))
        self.assertEqual(expected, sma.count3SMotifs(self.G))
        self.assertEqual(expected, sma.count3SMotifsSparse(self.G))
        self.assertEqual(expected, sma.countMultiMotifs(self.G, 2, 1))
        self.assertEqual(expected, sma.countMultiMotifsSparse(self.G, 2, 1))
        self.assertEqual(expected, sma.countMultiMotifs(self.G, 2, 1, roles=[0, 1]))
        self.assertEqual(expected, sma.countMultiMotifsSparse(self.G, 2, 1, roles=[0, 1]))
        self.assertEqual(expected, sma.countMultiMotifs(self.G, 1, 2, roles=[1, 0]))
        self.assertEqual(expected, sma.countMultiMotifsSparse(self.G, 1, 2, roles=[1, 0]))
        
        # try several equivalent motif identifier strings
        # this must be a dense motif, ideally not invariant under twisting
        self.assertMotifIdentifiers(expected, 
                                    ['2,1', '2:0,1:1', '1:1,2:0'], 
                                    ['II.B'])
        
    def test_number_4_motifs(self):
        expected = {'I.A': 376, 'I.B': 487, 'I.C': 638, 'I.D': 743, 'II.A': 417, 
                    'II.B': 569, 'II.C': 638, 'II.D': 810, 'III.A': 456, 'III.B': 829, 
                    'III.C': 640, 'III.D': 942, 'IV.A': 86, 'IV.B': 73, 'IV.C': 132, 
                    'IV.D': 139, 'V.A': 1238, 'V.B': 1723, 'V.C': 1728, 'V.D': 2476, 
                    'VI.A': 535, 'VI.B': 930, 'VI.C': 464, 'VI.D': 680, 'VII.A': 534, 
                    'VII.B': 730, 'VII.C': 403, 'VII.D': 534}
        self.assertEqual(sum(expected.values()), sma.total4Motifs(self.G))
        dense_motifs = sma.MOTIF4_NAMES[:12] + sma.MOTIF4_NAMES[16:]
        self.assertEqual(expected, sma.count4Motifs(self.G))
        self.assertEqual(expected, sma.count4MotifsSparse(self.G, guess_IV_A=expected['IV.A']))
        self.assertEqualDict(expected, sma.count4MotifsSparse(self.G), dense_motifs)
        # 4-motifs are twisted
        self.assertEqual(expected, sma.countMultiMotifs(self.G, 
                                                        2, 2, 
                                                        roles = [1, 0]))
        self.assertEqual(expected, sma.countMultiMotifsSparse(self.G, 
                                                              2, 2, 
                                                              roles = [1, 0], 
                                                              guess_IV_A=expected['IV.A']))
        self.assertEqualDict(expected,
                             sma.countMultiMotifs(self.G, 2, 2, roles = [1, 0]),
                             dense_motifs)
        
        # try several equivalent motif identifier strings
        # this must be a dense motif, ideally not invariant under twisting
        self.assertMotifIdentifiers(expected, 
                                    ['2:1,2:0'], 
                                    ['II.B'],
                                    dense_motifs)
        
    def test_number_4_motifs_twisted(self):
        expected = {'I.A': 376, 'I.B': 638, 'I.C': 487, 'I.D': 743, 'II.A': 403, 
                    'II.B': 534, 'II.C': 464, 'II.D': 680, 'III.A': 456, 'III.B': 640, 
                    'III.C': 829, 'III.D': 942, 'IV.A': 86, 'IV.B': 132, 'IV.C': 73,
                    'IV.D': 139, 'V.A': 1238, 'V.B': 1728, 'V.C': 1723, 'V.D': 2476, 
                    'VI.A': 730, 'VI.B': 930, 'VI.C': 638, 'VI.D': 810, 'VII.A': 534, 
                    'VII.B': 535, 'VII.C': 417, 'VII.D': 569}
        dense_motifs = sma.MOTIF4_NAMES[:12] + sma.MOTIF4_NAMES[16:]
        self.assertEqual(expected, sma.count4Motifs(self.G, 
                                                    sma.NODE_TYPE_ECO, sma.NODE_TYPE_SOC))
        self.assertEqual(expected, sma.count4MotifsSparse(self.G, 
                                                          level0 = sma.NODE_TYPE_ECO,
                                                          level1 = sma.NODE_TYPE_SOC,
                                                          guess_IV_A=expected['IV.A']))
        self.assertEqualDict(expected,
                             sma.count4MotifsSparse(self.G, 
                                                    level0 = sma.NODE_TYPE_ECO,
                                                    level1 = sma.NODE_TYPE_SOC),
                             dense_motifs)
        
        # 4-motifs are twisted
        self.assertEqual(expected, sma.countMultiMotifs(self.G, 
                                                        2, 2))
        self.assertEqual(expected, sma.countMultiMotifs(self.G, 
                                                        2, 2,
                                                        roles = [0, 1]))
        self.assertEqual(expected, sma.countMultiMotifsSparse(self.G, 
                                                              2, 2, 
                                                              roles = [0, 1], 
                                                              guess_IV_A=expected['IV.A']))
        self.assertEqual(expected, sma.countMultiMotifsSparse(self.G, 
                                                              2, 2, 
                                                              guess_IV_A=expected['IV.A']))
        self.assertEqualDict(expected,
                             sma.countMultiMotifs(self.G, 2, 2),
                             dense_motifs)
        self.assertEqualDict(expected,
                             sma.countMultiMotifs(self.G, 2, 2, roles = [0, 1]),
                             dense_motifs)
        
        # try several equivalent motif identifier strings
        # this must be a dense motif, ideally not invariant under twisting
        self.assertMotifIdentifiers(expected, 
                                    ['2,2', '2:0,2:1'], 
                                    ['II.B'],
                                    dense_motifs)
    def test_number_13_motifs(self):
        expected =  {-1: 7655, 1: 1445}
        self.assertEqual(expected, sma.countMultiMotifs(self.G, 1,3))
        self.assertEqual(expected[1], sma.countMultiMotifsSparse(self.G, 1, 3)[1])
        
    def test_number_23_motifs(self):
        expected = {-1: 86122, 1: 328}
        self.assertEqual(expected, sma.countMultiMotifs(self.G, 2,3))
        
    def test_r_interface(self):
        # testing 1,2 motifs
        expected_df_omit = pandas.DataFrame(data = {'1,2[I.A]': 145}, index = ['count'])
        pandas.testing.assert_frame_equal(expected_df_omit,
                                          sma.countMotifsAutoR(self.G, ['1,2[I.A]'], omit_total_result = True))
        expected_df_full = pandas.DataFrame(data = {'1,2[I.A]': 145,
                                                    '1,2[I.B]': 428,
                                                    '1,2[I.C]': 327,
                                                    '1,2[II.A]': 172,
                                                    '1,2[II.B]': 548,
                                                    '1,2[II.C]': 480}, index = ['count'])
        pandas.testing.assert_frame_equal(expected_df_full,
                                          sma.countMotifsAutoR(self.G, ['1,2[I.A]'], omit_total_result = False))
        

class TestMotifCountingThreeLevel(MotifTest):
    """
    Tests based on a random two-level SEN genereated using
    
    .. code :: Python
        
        nVertices = [20, 25, 15]
        nEdges = numpy.array([[90, 240, 250],
                              [ 0, 170, 260],
                              [ 0,   0,  25]])
        G = next(sma.randomMultiSENs(nVertices, nEdges))
        sma.saveSEN(G, 'test/data/threelevel_adj.csv', 'test/data/threelevel_attr.csv')
    
    """
    def setUp(self):
       self.G = sma.loadSEN('test/data/threelevel_adj.csv', 
                            'test/data/threelevel_attr.csv',
                            delimiter = ',')
    def test_number_vertices(self):
        self.assertEqual({0: 20, 1: 25, 2: 15}, sma.nodesCount(self.G))
    def test_type_vertices(self):
        self.assertEqual({0, 1, 2}, sma.sesTypes(self.G))
    def test_number_edges(self):
        edges_matrix = sma.edgesCountMatrix(self.G)
        edges_matrix_expected = numpy.array([[ 90, 240, 250],
                                             [  0, 170, 260],
                                             [  0,   0,  25]])
        self.assertTrue(numpy.all(edges_matrix == edges_matrix_expected))
    def test_density(self):
        density_matrix_expected = numpy.array([[0.47368421, 0.48      , 0.83333333],
                                               [0.        , 0.56666667, 0.69333333],
                                               [0.        , 0.        , 0.23809524]])
        self.assertTrue(numpy.all(numpy.isclose(sma.densityMatrix(self.G), 
                                                density_matrix_expected)))
    def test_3_motifs(self):
        # distinct node in level 0, non-distinct nodes in level 2
        expected = {'I.A': 49, 'I.B': 433, 'I.C': 1118, 
                    'II.A': 13, 'II.B': 143, 'II.C': 344}
        self.assertEqual(sum(expected.values()), sma.totalMultiMotifs(self.G, 1, 0, 2))
        self.assertEqual(expected, sma.count3Motifs(self.G, 0, 2))
        self.assertEqual(expected, sma.countMultiMotifs(self.G, 1, 0, 2))
        self.assertEqual(expected, sma.countMultiMotifsSparse(self.G, 1, 0, 2))
        self.assertEqual(expected, sma.countMultiMotifs(self.G, 1, 2, roles = [0, 2]))
        self.assertEqual(expected, sma.countMultiMotifsSparse(self.G, 1, 2, roles = [0, 2]))
        self.assertEqual(expected, sma.countMultiMotifs(self.G, 2, 1, roles = [2, 0]))
        self.assertEqual(expected, sma.countMultiMotifsSparse(self.G, 2, 1, roles = [2, 0]))
        
        self.assertMotifIdentifiers(expected, 
                                    ['1,0,2','1:0,2:2','2:2,1:0'], 
                                    ['II.B'])
        
        # test edgeContributionList
        edge_contribution = numpy.sum(sma.edgeContributionList(self.G, 1, 0, 2, level = 2)[0], axis=1)
        self.assertEqual(edge_contribution[0], expected['I.A'] + expected['II.A'])
        self.assertEqual(edge_contribution[1], expected['I.B'] + expected['II.B'])
        self.assertEqual(edge_contribution[2], expected['I.C'] + expected['II.C'])

        edge_contribution = numpy.sum(sma.edgeContributionList(self.G, 1, 2, roles=[0, 2], level = 2)[0], axis=1)
        self.assertEqual(edge_contribution[0], expected['I.A'] + expected['II.A'])
        self.assertEqual(edge_contribution[1], expected['I.B'] + expected['II.B'])
        self.assertEqual(edge_contribution[2], expected['I.C'] + expected['II.C'])
        
        edge_contribution = numpy.sum(sma.edgeContributionList(self.G, 2, 1, roles=[2, 0], level = 2)[0], axis=1)
        self.assertEqual(edge_contribution[0], expected['I.A'] + expected['II.A'])
        self.assertEqual(edge_contribution[1], expected['I.B'] + expected['II.B'])
        self.assertEqual(edge_contribution[2], expected['I.C'] + expected['II.C'])
        
    def test_4_motifs(self):
        # on levels 1 and 2
        expected = {'I.A': 901, 'I.B': 1317, 'I.C': 341, 'I.D': 500, 'II.A': 1004, 
                    'II.B': 1164, 'II.C': 361, 'II.D': 443, 'III.A': 2484,
                    'III.B': 2997, 'III.C': 675, 'III.D': 819, 'IV.A': 81, 
                    'IV.B': 120, 'IV.C': 23, 'IV.D': 16, 'V.A': 4290, 'V.B': 5772, 
                    'V.C': 1397, 'V.D': 1803, 'VI.A': 1015, 'VI.B': 326, 
                    'VI.C': 1215, 'VI.D': 343, 'VII.A': 791, 'VII.B': 240, 
                    'VII.C': 849, 'VII.D': 213}
        dense_motifs = sma.MOTIF4_NAMES[:12] + sma.MOTIF4_NAMES[16:]
        self.assertEqual(sum(expected.values()), sma.totalMultiMotifs(self.G, 0, 2, 2))
        # direct call
        self.assertEqual(expected, sma.count4Motifs(self.G, 1, 2))
        self.assertEqual(expected, sma.count4MotifsSparse(self.G, 
                                                          level0 = 1,
                                                          level1 = 2,
                                                          guess_IV_A=expected['IV.A']))
        self.assertEqualDict(expected, sma.count4MotifsSparse(self.G, level0 = 1, level1 = 2), dense_motifs)
        
        # multi-level call, without roles
        self.assertEqual(expected, sma.countMultiMotifs(self.G, 0, 2, 2))
        self.assertEqual(expected, sma.countMultiMotifsSparse(self.G, 0, 2, 2, guess_IV_A=expected['IV.A']))
        self.assertEqualDict(expected, sma.countMultiMotifsSparse(self.G, 0, 2, 2), dense_motifs)
        
        # multi-level call, with roles, no trailing zero arity
        self.assertEqual(expected, sma.countMultiMotifs(self.G, 2, 2, roles = [1,2]))
        self.assertEqual(expected, sma.countMultiMotifsSparse(self.G, 2, 2, roles = [1,2], guess_IV_A=expected['IV.A']))
        self.assertEqualDict(expected, sma.countMultiMotifsSparse(self.G, 2, 2, roles = [1,2]), dense_motifs)
        
        # multi-level call, with roles, trailing zero arity
        self.assertEqual(expected, sma.countMultiMotifs(self.G, 0, 2, 2, roles = [1,2]))
        self.assertEqual(expected, sma.countMultiMotifsSparse(self.G, 0, 2, 2, roles = [1,2], guess_IV_A=expected['IV.A']))
        self.assertEqualDict(expected, sma.countMultiMotifsSparse(self.G, 0, 2, 2, roles = [1,2]), dense_motifs)
        
        self.assertMotifIdentifiers(expected, 
                                    ['0,2,2','2:1,2:2'], 
                                    ['II.B'],
                                    dense_motifs)
    
    def test_4_motifs_twisted(self):
        # level 2 and 0 twisted
        expected = {'I.A': 289, 'I.B': 106, 'I.C': 261, 'I.D': 99, 'II.A': 341, 
                    'II.B': 105, 'II.C': 329, 'II.D': 69, 'III.A': 3906, 
                    'III.B': 1167, 'III.C': 3465, 'III.D': 1070, 'IV.A': 5, 
                    'IV.B': 0, 'IV.C': 5, 'IV.D': 1, 'V.A': 3089, 'V.B': 998, 
                    'V.C': 2741, 'V.D': 890, 'VI.A': 35, 'VI.B': 36, 'VI.C': 89, 
                    'VI.D': 85, 'VII.A': 118, 'VII.B': 123, 'VII.C': 252, 'VII.D': 276}
        dense_motifs = sma.MOTIF4_NAMES[:12] + sma.MOTIF4_NAMES[16:]
        self.assertEqual(sum(expected.values()), sma.totalMultiMotifs(self.G, 2, 0, 2))
        
        # direct call
        self.assertEqual(expected, sma.count4Motifs(self.G, 2, 0))
        self.assertEqual(expected, sma.count4MotifsSparse(self.G, 
                                                          level0 = 2,
                                                          level1 = 0,
                                                          guess_IV_A=expected['IV.A']))
        self.assertEqualDict(expected, sma.count4MotifsSparse(self.G, level0 = 2, level1 = 0), dense_motifs)
        
        # multi-level call, with roles and zero
        self.assertEqual(expected, sma.countMultiMotifs(self.G, 2, 2, roles = [2,0]))
        self.assertEqual(expected, sma.countMultiMotifsSparse(self.G, 2, 2, roles = [2,0], guess_IV_A=expected['IV.A']))
        self.assertEqualDict(expected, sma.countMultiMotifsSparse(self.G, 2, 2, roles = [2,0]), dense_motifs)
        
        # multi-level call, with roles and zero
        self.assertEqual(expected, sma.countMultiMotifs(self.G, 2, 0, 2, roles = [2,0]))
        self.assertEqual(expected, sma.countMultiMotifsSparse(self.G, 2, 0, 2, roles = [2,0], guess_IV_A=expected['IV.A']))
        self.assertEqualDict(expected, sma.countMultiMotifsSparse(self.G, 2, 0, 2, roles = [2,0]), dense_motifs)
        
        self.assertMotifIdentifiers(expected, 
                                    ['2:2,2:0'], 
                                    ['II.B'],
                                    dense_motifs)
    
    def test_121_motifs(self):
        # two on level 0, one each on the other levels
        expected =  {-1: 57413, 1: 4013, 2: 1635, 3: 4141, 4: 4048}
        dense_motifs = [3,4]
        self.assertEqual(sum(expected.values()), sma.totalMultiMotifs(self.G, 2, 1, 1))
        
        self.assertEqual(expected, sma.count121MotifsSparse(self.G, 1, 0, 2))
        self.assertEqual(expected, sma.countMultiMotifs(self.G, 2, 1, 1))
        self.assertEqual(expected, sma.countMultiMotifsSparse(self.G, 2, 1, 1))
        self.assertEqualDict(expected, sma.countMultiMotifsSparse(self.G, 2, 1, 1, optimize_top_adjacent = True), dense_motifs)
        self.assertMotifIdentifiers(expected, 
                                    ['2,1,1','1:1,2:0,1:2','2:0,1:1,1:2','1:1,1:2,2:0'], 
                                    [1,3])
        self.assertMotifIdentifiers(expected, 
                                    ['2,1,1'], 
                                    [3,4],
                                    dense_motifs)
    def test_111_motifs(self):
        # default positions
        expected = {0: 207, 1: 181, 2: 446, 3: 416, 4: 1023, 5: 889, 6: 2224, 7: 2114}
        self.assertEqual(sum(expected.values()), sma.totalMultiMotifs(self.G, 1, 1, 1))
        self.assertEqual(expected, sma.countMultiMotifs(self.G, 1, 1, 1))
        self.assertEqual(expected, sma.countMotifs(self.G,
                                                   sma.Multi111MotifClassificator(self.G),
                                                   sma.MultiMotifs(self.G, 1, 1, 1, flatten = True)))
        self.assertMotifIdentifiers(expected, 
                                    ['1,1,1','1:0,1:1,1:2'], 
                                    [4])
    def test_221_motifs(self):
        # positions 2-1-2
        expected = {'Unclassified': 358716, 'I.A.0': 205, 'I.A.1': 123, 'I.A.2': 859, 
                    'I.A.3': 763, 'I.B.0': 191, 'I.B.1': 150, 'I.B.2': 732, 'I.B.3': 796, 
                    'I.C.0': 85, 'I.C.1': 44, 'I.C.2': 325, 'I.C.3': 238, 'I.D.0': 59, 
                    'I.D.1': 41, 'I.D.2': 299, 'I.D.3': 278, 'II.A.0': 158, 'II.A.1': 105,
                    'II.A.2': 796, 'II.A.3': 740, 'II.B.0': 175, 'II.B.1': 129, 
                    'II.B.2': 866, 'II.B.3': 796, 'II.C.0': 64, 'II.C.1': 29, 
                    'II.C.2': 308, 'II.C.3': 194, 'II.D.0': 51, 'II.D.1': 33, 
                    'II.D.2': 275, 'II.D.3': 194, 'III.A.0': 2731, 'III.A.1': 1581, 
                    'III.A.2': 12283, 'III.A.3': 11128, 'III.B.0': 2238, 'III.B.1': 1559, 
                    'III.B.2': 10815, 'III.B.3': 10733, 'III.C.0': 723, 'III.C.1': 404, 
                    'III.C.2': 3323, 'III.C.3': 3165, 'III.D.0': 602, 'III.D.1': 413, 
                    'III.D.2': 2975, 'III.D.3': 3147, 'IV.A.0': 3, 'IV.A.1': 2,
                    'IV.A.2': 18, 'IV.A.3': 12, 'IV.B.0': 2, 'IV.B.1': 1, 'IV.B.2': 20, 
                    'IV.B.3': 16, 'IV.C.0': 0, 'IV.C.1': 0, 'IV.C.2': 0, 'IV.C.3': 0, 
                    'IV.D.0': 1, 'IV.D.1': 0, 'IV.D.2': 4, 'IV.D.3': 1, 'V.A.0': 2123, 
                    'V.A.1': 1336, 'V.A.2': 9559, 'V.A.3': 8725, 'V.B.0': 1903, 
                    'V.B.1': 1313, 'V.B.2': 8284, 'V.B.3': 8482, 'V.C.0': 646, 
                    'V.C.1': 383, 'V.C.2': 2919, 'V.C.3': 2526, 'V.D.0': 576, 
                    'V.D.1': 392, 'V.D.2': 2572, 'V.D.3': 2356, 'VI.A.0': 79, 
                    'VI.A.1': 60, 'VI.A.2': 347, 'VI.A.3': 333, 'VI.B.0': 27, 
                    'VI.B.1': 14, 'VI.B.2': 129, 'VI.B.3': 78, 'VI.C.0': 252, 
                    'VI.C.1': 171, 'VI.C.2': 980, 'VI.C.3': 936, 'VI.D.0': 45, 
                    'VI.D.1': 37, 'VI.D.2': 219, 'VI.D.3': 149, 'VII.A.0': 88, 
                    'VII.A.1': 45, 'VII.A.2': 378, 'VII.A.3': 313, 'VII.B.0': 21, 
                    'VII.B.1': 18, 'VII.B.2': 131, 'VII.B.3': 82, 'VII.C.0': 207, 
                    'VII.C.1': 132, 'VII.C.2': 983, 'VII.C.3': 924, 'VII.D.0': 52, 
                    'VII.D.1': 42, 'VII.D.2': 396, 'VII.D.3': 270}
        self.assertEqual(sum(expected.values()), sma.totalMultiMotifs(self.G, 2, 1, 2))
        
        dense_motifs = list(map(lambda x : '%s.%d' % x, itertools.product(sma.MOTIF3_NAMES, [2, 3])) )
        extremely_dense_motifs = list(map(lambda x : '%s.%d' % x, itertools.product(sma.MOTIF3_NAMES, [3])) )
        
        self.assertEqual(expected, sma.countMotifs(self.G,
                                                   sma.Multi221MotifClassificator(self.G),
                                                   sma.MultiMotifs(self.G, 2, 2, 1, 
                                                                   flatten = True,
                                                                   subsystems = [0, 2, 1])))
        self.assertEqualDict(expected, sma.count221MotifsSparse(self.G, 0, 2, 1, optimize_top_adjacent=False), dense_motifs)
        self.assertEqualDict(expected, sma.count221MotifsSparse(self.G, 0, 2, 1, optimize_top_adjacent=True), extremely_dense_motifs)
        self.assertEqual(expected, sma.countMultiMotifs(self.G, 2, 1, 2))
        self.assertEqualDict(expected, sma.countMultiMotifsSparse(self.G, 2, 1, 2), dense_motifs)
        self.assertEqualDict(expected, sma.countMultiMotifsSparse(self.G, 2, 1, 2, optimize_top_adjacent=True), extremely_dense_motifs)
        
        self.assertEqual(expected, sma.countMultiMotifs(self.G, 1, 2, 2, roles=[1, 0, 2]))
        self.assertEqualDict(expected, sma.countMultiMotifsSparse(self.G, 1, 2, 2, roles=[1, 0, 2]), dense_motifs)
        self.assertEqualDict(expected, sma.countMultiMotifsSparse(self.G, 1, 2, 2, roles=[1, 0, 2], optimize_top_adjacent=True), extremely_dense_motifs)
        
        self.assertMotifIdentifiers(expected, 
                                    ['2,1,2','2:0,2:2,1:1','1:1,2:0,2:2'], 
                                    ['II.B.2'],
                                    dense_motifs)
        # test precision levels in sma.findIdealCounter
        self.assertMotifIdentifiers(expected, 
                                    ['2,1,2'], 
                                    ['II.B.2','II.B.3'],
                                    dense_motifs)
        self.assertMotifIdentifiers(expected, 
                                    ['2,1,2'], 
                                    ['II.C.3','II.B.3'],
                                    extremely_dense_motifs)
        
    def test_222_motifs(self):
        # inverse positions
        expected = {-1: 5976021, 1: 10, 2: 4, 3: 7106, 4: 1859}
        dense_motifs = [3,4]
        self.assertEqual(sum(expected.values()), sma.totalMultiMotifs(self.G, 2, 2, 2))
        self.assertEqual(expected, sma.countMultiMotifs(self.G, 2, 2, 2, roles = [2, 1, 0]))
        self.assertEqualDict(expected, sma.count222MotifsSparse(self.G, 2, 1, 0), dense_motifs)
        self.assertEqual(expected, sma.countMotifs(self.G,
                                                   sma.Multi222MotifClassificator(self.G),
                                                   sma.MultiMotifs(self.G, 2, 2, 2, 
                                                                   flatten = True,
                                                                   subsystems = [2, 1, 0])))
        self.assertEqualDict(expected, sma.countMultiMotifsSparse(self.G, 2, 2, 2, roles = [2, 1, 0]), dense_motifs)
        self.assertMotifIdentifiers(expected, 
                                    ['2:2,2:1,2:0'], 
                                    [3, 4],
                                    dense_motifs)
    def test_plain_3_motifs(self):
        expected = {0: 201, 1: 191, 2: 55, 3: 8}
        self.assertEqual(expected, sma.countMultiMotifs(self.G, 0, 0, 3))
    def test_distribution(self):
        # 3-motifs on levels 0 (distinct), 1 (non-distinct)
        expected3 = numpy.array([ 703.04, 1297.92,  599.04,  919.36, 1697.28,  783.36])
        self.assertTrue(numpy.all(numpy.isclose(expected3, sma.expected3EMotifs(self.G, array = True))))
        self.assertTrue(numpy.all(numpy.isclose(expected3, sma.expected3Motifs(self.G, 0, 1, array = True))))
        self.assertTrue(numpy.all(numpy.isclose(expected3, sma.expectedMultiMotifs(self.G, 1, 2, array = True))))
        self.assertTrue(numpy.all(numpy.isclose(expected3, sma.expectedMultiMotifs(self.G, 1, 2, roles = [0, 1], array = True))))
        for idt in ['1,2','1:0,2:1','2:1,1:0']:
            motif = 'I.A'
            request = '%s[%s]' % (idt, motif)
            with self.subTest(identifer = request):
                _, total = sma.distributionMotifsAuto(self.G, request, model = 'erdos_renyi')
                for (k, (e, _)), expected in zip(total[idt].items(), expected3):
                    self.assertTrue(numpy.isclose(e, expected), 
                                    msg = 'For motif %s, expectations %f != %f' % (k, e, expected))
    def test_distribution_actors_choice(self):
        # test only sums so far
        self.assertTrue(numpy.isclose(
                sum(sma.distributionMotifsActorsChoice(self.G, 2, 2, array = True)[0]),
                sma.totalMultiMotifs(self.G, 2, 2)
                ))
        self.assertTrue(numpy.isclose(
                sum(sma.distributionMotifsActorsChoice(self.G, 0, 2, 2, array = True)[0]),
                sma.totalMultiMotifs(self.G, 0, 2, 2)
                ))
        self.assertTrue(numpy.isclose(
                sum(sma.distributionMotifsActorsChoice(self.G, 2, 2, roles = [0, 2], array = True)[0]),
                sma.totalMultiMotifs(self.G, 2, 0, 2)
                ))
    
    def test_motifr_bug68(self):
        g = nx.Graph()
        g.add_node(1, sesType= 0)
        g.add_node(2, sesType=  0)
        g.add_node(3, sesType=  1)
        g.add_node(4, sesType=  2)
        g.add_node(5, sesType=  1)
        g.add_edges_from([(1,4),(1,5),(2,4), (2,5), (3,5)])
        
        d1 = sma.countMotifsAuto(g, '2,2,1[II.C.1]', assume_sparse=False)
        d2 = sma.countMotifsAuto(g, '2,2,1[II.C.1]', assume_sparse=True)
        
        self.assertEqual(d1[0]['2,2,1[II.C.1]'], 1)
        self.assertEqual(d2[0]['2,2,1[II.C.1]'], 1)
        
class TestDiGraphFacilites(MotifTest):
    def setUp(self):
       self.G = sma.loadDiSEN('test/data/digraph_adj.csv', 
                              'test/data/digraph_attr.csv',
                              delimiter = ',')
    def test_io(self):
        self.assertEqual(self.G.number_of_edges(), 14)
        self.assertEqual(self.G.number_of_nodes(), 6)
        self.assertEqualDict(sma.nodesCount(self.G), {0: 3, 1: 3})
    def test_counting(self):
        counts2 = {0: 0, 1: 2, 2: 1}
        self.assertEqualDict(sma.countMultiMotifs(self.G, 2), counts2)
        self.assertEqualDict(sma.countMotifsAuto(self.G, "2[1]")[1]['2'], counts2)
        
        counts02 = {0: 1, 1: 2, 2: 0}
        self.assertEqualDict(sma.countMultiMotifs(self.G, 0, 2), counts02)
        self.assertEqualDict(sma.countMotifsAuto(self.G, "0,2[1]")[1]['0,2'], counts02)
        
        counts11 =  {0: 3, 1: 1, 2: 3, 3: 2}
        self.assertEqualDict(sma.countMultiMotifs(self.G, 1, 1), counts11)
        self.assertEqualDict(sma.countMotifsAuto(self.G, "1,1[1]")[1]['1,1'], counts11)
    def test_counting12(self):
        counts12 = {'I.A': 0, 'I.B.1': 1, 'I.B.2': 1, 'I.B.3': 0, 'I.C.1': 0, 
                    'I.C.2': 0, 'I.C.3': 0, 'I.C.4': 0, 'I.C.5': 1, 'I.C.6': 0, 
                    'II.A': 1, 'II.B.1': 1, 'II.B.2': 0, 'II.B.3': 0, 'II.B.4': 0, 
                    'II.B.5': 0, 'II.B.6': 1, 'II.C.1': 0, 'II.C.2': 0, 'II.C.3': 0, 
                    'II.C.4': 0, 'II.C.5': 1, 'II.C.6': 1, 'II.C.7': 0, 'II.C.8': 1, 
                    'II.C.9': 0, 'III.A': 0, 'III.B.1': 0, 'III.B.2': 0, 'III.B.3': 0, 
                    'III.C.1': 0, 'III.C.2': 0, 'III.C.3': 0, 'III.C.4': 0, 'III.C.5': 0, 
                    'III.C.6': 0}
        self.assertEqualDict(sma.countMultiMotifs(self.G, 1, 2), counts12)
        self.assertEqualDict(sma.countMotifsAuto(self.G, "1,2[I.A]")[1]['1,2'], counts12)
        
        counts21 = {'I.A': 0, 'I.B.1': 0, 'I.B.2': 0, 'I.B.3': 0, 'I.C.1': 0, 
                    'I.C.2': 0, 'I.C.3': 0, 'I.C.4': 0, 'I.C.5': 0, 'I.C.6': 0, 
                    'II.A': 0, 'II.B.1': 0, 'II.B.2': 1, 'II.B.3': 0, 'II.B.4': 3, 
                    'II.B.5': 0, 'II.B.6': 1, 'II.C.1': 0, 'II.C.2': 0, 'II.C.3': 0, 
                    'II.C.4': 0, 'II.C.5': 0, 'II.C.6': 0, 'II.C.7': 0, 'II.C.8': 1, 
                    'II.C.9': 0, 'III.A': 0, 'III.B.1': 0, 'III.B.2': 0, 'III.B.3': 1, 
                    'III.C.1': 1, 'III.C.2': 0, 'III.C.3': 0, 'III.C.4': 1, 'III.C.5': 0, 
                    'III.C.6': 0}
        self.assertEqualDict(sma.countMultiMotifs(self.G, 2, 1), counts21)
        self.assertEqualDict(sma.countMotifsAuto(self.G, "2,1[I.A]")[1]['2,1'], counts21)

def TestLargeDiGraph(MotifTest):
    def setUp(self):
       self.G = sma.loadDiSEN('test/data/large_digraph_adj.csv', 
                              'test/data/large_digraph_attr.csv',
                              delimiter = ',')
    def test_counting22(self):
        counts = {'I.A': 17, 'I.B.1': 51, 'I.B.2': 58, 'I.B.3': 38, 'I.C.1': 16, 'I.C.2': 51, 'I.C.3': 11, 'I.C.4': 32, 'I.C.5': 18, 'I.C.6': 9, 'I.D.1': 21, 'I.D.2': 36, 'I.D.3': 19, 'I.D.4': 22, 'I.D.5': 27, 'I.D.6': 9, 'I.E.1': 24, 'I.E.2': 44, 'I.E.3': 14, 'I.E.4': 11, 'I.E.5': 23, 'I.E.6': 11, 'I.F.ddd': 27, 'I.F.ddu': 35, 'I.F.ddb': 21, 'I.F.dud': 26, 'I.F.duu': 24, 'I.F.dub': 22, 'I.F.dbd': 20, 'I.F.dbu': 18, 'I.F.dbb': 13, 'I.F.udd': 18, 'I.F.udu': 11, 'I.F.udb': 9, 'I.F.uud': 27, 'I.F.uuu': 31, 'I.F.uub': 18, 'I.F.ubd': 11, 'I.F.ubu': 13, 'I.F.ubb': 8, 'I.F.bdd': 9, 'I.F.bdu': 14, 'I.F.bdb': 5, 'I.F.bud': 21, 'I.F.buu': 15, 'I.F.bub': 19, 'I.F.bbd': 8, 'I.F.bbu': 7, 'I.F.bbb': 5, 'I.Ga.A': 3, 'I.Ga.B': 6, 'I.Ga.C': 6, 'I.Ga.D': 8, 'I.Ga.E': 10, 'I.Ga.F': 7, 'I.Ga.G': 3, 'I.Gb.ddd': 15, 'I.Gb.ddu': 10, 'I.Gb.dud': 12, 'I.Gb.duu': 7, 'I.Gb.udd': 6, 'I.Gb.udu': 12, 'I.Gb.uud': 15, 'I.Gb.uuu': 3, 'I.Gc.1': 1, 'I.Gc.2': 10, 'I.Gc.3': 2, 'I.Gd.1': 0, 'I.Gd.2': 6, 'I.Gd.3': 6, 'I.Ge.1': 6, 'I.Ge.2': 8, 'I.Ge.4': 1, 'I.Gf.1': 2, 'I.Gf.2': 6, 'I.Gg': 2, 'II.AA': 21, 'II.AB1': 31, 'II.AB2': 35, 'II.AB3': 21, 'II.AC1': 9, 'II.AC2': 18, 'II.AC3': 16, 'II.AC4': 13, 'II.AC5': 15, 'II.AC6': 6, 'II.B1A': 23, 'II.B1B1': 39, 'II.B1B2': 48, 'II.B1B3': 27, 'II.B1C1': 17, 'II.B1C2': 28, 'II.B1C3': 13, 'II.B1C4': 22, 'II.B1C5': 15, 'II.B1C6': 9, 'II.B2A': 39, 'II.B2B1': 51, 'II.B2B2': 69, 'II.B2B3': 35, 'II.B2C1': 28, 'II.B2C2': 41, 'II.B2C3': 14, 'II.B2C4': 24, 'II.B2C5': 33, 'II.B2C6': 4, 'II.B3A': 21, 'II.B3B1': 29, 'II.B3B2': 33, 'II.B3B3': 19, 'II.B3C1': 14, 'II.B3C2': 18, 'II.B3C3': 21, 'II.B3C4': 10, 'II.B3C5': 14, 'II.B3C6': 6, 'II.C1A': 8, 'II.C1B1': 19, 'II.C1B2': 17, 'II.C1B3': 8, 'II.C1C1': 2, 'II.C1C2': 9, 'II.C1C3': 5, 'II.C1C4': 7, 'II.C1C5': 10, 'II.C1C6': 3, 'II.C2A': 20, 'II.C2B1': 39, 'II.C2B2': 34, 'II.C2B3': 13, 'II.C2C1': 17, 'II.C2C2': 15, 'II.C2C3': 7, 'II.C2C4': 7, 'II.C2C5': 19, 'II.C2C6': 3, 'II.C3A': 12, 'II.C3B1': 21, 'II.C3B2': 31, 'II.C3B3': 10, 'II.C3C1': 10, 'II.C3C2': 17, 'II.C3C3': 8, 'II.C3C4': 7, 'II.C3C5': 4, 'II.C3C6': 0, 'II.C4A': 13, 'II.C4B1': 17, 'II.C4B2': 12, 'II.C4B3': 14, 'II.C4C1': 6, 'II.C4C2': 8, 'II.C4C3': 3, 'II.C4C4': 5, 'II.C4C5': 13, 'II.C4C6': 4, 'II.C5A': 16, 'II.C5B1': 23, 'II.C5B2': 22, 'II.C5B3': 10, 'II.C5C1': 12, 'II.C5C2': 15, 'II.C5C3': 10, 'II.C5C4': 10, 'II.C5C5': 10, 'II.C5C6': 3, 'II.C6A': 5, 'II.C6B1': 5, 'II.C6B2': 8, 'II.C6B3': 10, 'II.C6C1': 4, 'II.C6C2': 5, 'II.C6C3': 3, 'II.C6C4': 1, 'II.C6C5': 0, 'II.C6C6': 0, 'III.A': 7, 'III.B.1': 24, 'III.B.2': 48, 'III.B.3': 13, 'III.C.1': 7, 'III.C.2': 23, 'III.C.3': 10, 'III.C.4': 19, 'III.C.5': 14, 'III.C.6': 3, 'III.D.1': 16, 'III.D.2': 31, 'III.D.3': 15, 'III.D.4': 10, 'III.D.5': 13, 'III.D.6': 6, 'III.E.1': 16, 'III.E.2': 33, 'III.E.3': 12, 'III.E.4': 12, 'III.E.5': 8, 'III.E.6': 1, 'III.F.ddd': 24, 'III.F.ddu': 20, 'III.F.ddb': 8, 'III.F.dud': 18, 'III.F.duu': 13, 'III.F.dub': 7, 'III.F.dbd': 17, 'III.F.dbu': 13, 'III.F.dbb': 9, 'III.F.udd': 10, 'III.F.udu': 18, 'III.F.udb': 2, 'III.F.uud': 25, 'III.F.uuu': 17, 'III.F.uub': 8, 'III.F.ubd': 18, 'III.F.ubu': 8, 'III.F.ubb': 8, 'III.F.bdd': 11, 'III.F.bdu': 10, 'III.F.bdb': 6, 'III.F.bud': 14, 'III.F.buu': 9, 'III.F.bub': 3, 'III.F.bbd': 9, 'III.F.bbu': 9, 'III.F.bbb': 1, 'III.Ga.A': 1, 'III.Ga.B': 13, 'III.Ga.C': 2, 'III.Ga.D': 5, 'III.Ga.E': 5, 'III.Ga.F': 9, 'III.Ga.G': 2, 'III.Gb.ddd': 6, 'III.Gb.ddu': 9, 'III.Gb.dud': 5, 'III.Gb.duu': 4, 'III.Gb.udd': 5, 'III.Gb.udu': 8, 'III.Gb.uud': 10, 'III.Gb.uuu': 8, 'III.Gc.1': 0, 'III.Gc.2': 4, 'III.Gc.3': 2, 'III.Gd.1': 3, 'III.Gd.2': 6, 'III.Gd.3': 4, 'III.Ge.1': 2, 'III.Ge.2': 6, 'III.Ge.4': 0, 'III.Gf.1': 3, 'III.Gf.2': 4, 'III.Gg': 0, 'IV.AA': 39, 'IV.AB1': 48, 'IV.AB2': 46, 'IV.AB3': 43, 'IV.AC1': 19, 'IV.AC2': 31, 'IV.AC3': 13, 'IV.AC4': 25, 'IV.AC5': 15, 'IV.AC6': 9, 'IV.B1A': 41, 'IV.B1B1': 59, 'IV.B1B2': 63, 'IV.B1B3': 55, 'IV.B1C1': 19, 'IV.B1C2': 47, 'IV.B1C3': 16, 'IV.B1C4': 39, 'IV.B1C5': 16, 'IV.B1C6': 8, 'IV.B2A': 50, 'IV.B2B1': 54, 'IV.B2B2': 56, 'IV.B2B3': 30, 'IV.B2C1': 22, 'IV.B2C2': 29, 'IV.B2C3': 10, 'IV.B2C4': 30, 'IV.B2C5': 9, 'IV.B2C6': 4, 'IV.B3A': 22, 'IV.B3B1': 46, 'IV.B3B2': 39, 'IV.B3B3': 34, 'IV.B3C1': 14, 'IV.B3C2': 33, 'IV.B3C3': 11, 'IV.B3C4': 46, 'IV.B3C5': 12, 'IV.B3C6': 9, 'IV.C1A': 12, 'IV.C1B1': 23, 'IV.C1B2': 15, 'IV.C1B3': 11, 'IV.C1C1': 6, 'IV.C1C2': 19, 'IV.C1C3': 5, 'IV.C1C4': 9, 'IV.C1C5': 3, 'IV.C1C6': 1, 'IV.C2A': 26, 'IV.C2B1': 40, 'IV.C2B2': 39, 'IV.C2B3': 25, 'IV.C2C1': 18, 'IV.C2C2': 21, 'IV.C2C3': 13, 'IV.C2C4': 18, 'IV.C2C5': 6, 'IV.C2C6': 5, 'IV.C3A': 14, 'IV.C3B1': 4, 'IV.C3B2': 11, 'IV.C3B3': 10, 'IV.C3C1': 3, 'IV.C3C2': 7, 'IV.C3C3': 1, 'IV.C3C4': 7, 'IV.C3C5': 2, 'IV.C3C6': 6, 'IV.C4A': 26, 'IV.C4B1': 41, 'IV.C4B2': 35, 'IV.C4B3': 22, 'IV.C4C1': 6, 'IV.C4C2': 23, 'IV.C4C3': 14, 'IV.C4C4': 24, 'IV.C4C5': 11, 'IV.C4C6': 7, 'IV.C5A': 12, 'IV.C5B1': 24, 'IV.C5B2': 11, 'IV.C5B3': 13, 'IV.C5C1': 7, 'IV.C5C2': 13, 'IV.C5C3': 7, 'IV.C5C4': 13, 'IV.C5C5': 3, 'IV.C5C6': 7, 'IV.C6A': 9, 'IV.C6B1': 6, 'IV.C6B2': 8, 'IV.C6B3': 6, 'IV.C6C1': 4, 'IV.C6C2': 4, 'IV.C6C3': 5, 'IV.C6C4': 5, 'IV.C6C5': 2, 'IV.C6C6': 1, 'V.nnnn': 42, 'V.nnnd': 39, 'V.nnnu': 23, 'V.nnnb': 31, 'V.nndn': 39, 'V.nndd': 39, 'V.nndu': 24, 'V.nndb': 32, 'V.nnun': 22, 'V.nnud': 18, 'V.nnuu': 7, 'V.nnub': 16, 'V.nnbn': 21, 'V.nnbd': 19, 'V.nnbu': 20, 'V.nnbb': 19, 'V.ndnn': 33, 'V.ndnd': 18, 'V.ndnu': 22, 'V.ndnb': 8, 'V.nddn': 42, 'V.nddd': 16, 'V.nddu': 14, 'V.nddb': 8, 'V.ndun': 22, 'V.ndud': 17, 'V.nduu': 12, 'V.ndub': 5, 'V.ndbn': 21, 'V.ndbd': 11, 'V.ndbu': 10, 'V.ndbb': 7, 'V.nunn': 31, 'V.nund': 15, 'V.nunu': 13, 'V.nunb': 9, 'V.nudn': 17, 'V.nudd': 13, 'V.nudu': 15, 'V.nudb': 6, 'V.nuun': 20, 'V.nuud': 11, 'V.nuuu': 11, 'V.nuub': 4, 'V.nubn': 17, 'V.nubd': 5, 'V.nubu': 9, 'V.nubb': 3, 'V.nbnn': 16, 'V.nbnd': 10, 'V.nbnu': 7, 'V.nbnb': 12, 'V.nbdn': 23, 'V.nbdd': 8, 'V.nbdu': 10, 'V.nbdb': 9, 'V.nbun': 11, 'V.nbud': 6, 'V.nbuu': 2, 'V.nbub': 6, 'V.nbbn': 4, 'V.nbbd': 9, 'V.nbbu': 6, 'V.nbbb': 2, 'V.dnnn': 42, 'V.dnnd': 27, 'V.dnnu': 20, 'V.dnnb': 22, 'V.dndn': 22, 'V.dndd': 16, 'V.dndu': 13, 'V.dndb': 19, 'V.dnun': 21, 'V.dnud': 27, 'V.dnuu': 6, 'V.dnub': 9, 'V.dnbn': 13, 'V.dnbd': 8, 'V.dnbu': 8, 'V.dnbb': 5, 'V.ddnn': 37, 'V.ddnd': 13, 'V.ddnu': 10, 'V.ddnb': 7, 'V.dddn': 13, 'V.dddd': 12, 'V.dddu': 10, 'V.dddb': 4, 'V.ddun': 29, 'V.ddud': 5, 'V.dduu': 3, 'V.ddub': 8, 'V.ddbn': 10, 'V.ddbd': 5, 'V.ddbu': 7, 'V.ddbb': 3, 'V.dunn': 25, 'V.dund': 14, 'V.dunu': 28, 'V.dunb': 9, 'V.dudn': 14, 'V.dudd': 8, 'V.dudu': 13, 'V.dudb': 6, 'V.duun': 18, 'V.duud': 7, 'V.duuu': 8, 'V.duub': 3, 'V.dubn': 8, 'V.dubd': 1, 'V.dubu': 3, 'V.dubb': 0, 'V.dbnn': 13, 'V.dbnd': 9, 'V.dbnu': 11, 'V.dbnb': 9, 'V.dbdn': 13, 'V.dbdd': 5, 'V.dbdu': 5, 'V.dbdb': 4, 'V.dbun': 10, 'V.dbud': 8, 'V.dbuu': 4, 'V.dbub': 3, 'V.dbbn': 3, 'V.dbbd': 2, 'V.dbbu': 2, 'V.dbbb': 2, 'V.unnn': 28, 'V.unnd': 25, 'V.unnu': 9, 'V.unnb': 12, 'V.undn': 15, 'V.undd': 13, 'V.undu': 7, 'V.undb': 6, 'V.unun': 18, 'V.unud': 19, 'V.unuu': 6, 'V.unub': 11, 'V.unbn': 3, 'V.unbd': 8, 'V.unbu': 4, 'V.unbb': 12, 'V.udnn': 23, 'V.udnd': 12, 'V.udnu': 10, 'V.udnb': 2, 'V.uddn': 12, 'V.uddd': 5, 'V.uddu': 10, 'V.uddb': 7, 'V.udun': 14, 'V.udud': 6, 'V.uduu': 4, 'V.udub': 9, 'V.udbn': 13, 'V.udbd': 7, 'V.udbu': 4, 'V.udbb': 5, 'V.uunn': 9, 'V.uund': 8, 'V.uunu': 5, 'V.uunb': 4, 'V.uudn': 4, 'V.uudd': 3, 'V.uudu': 4, 'V.uudb': 4, 'V.uuun': 10, 'V.uuud': 4, 'V.uuuu': 3, 'V.uuub': 3, 'V.uubn': 7, 'V.uubd': 3, 'V.uubu': 4, 'V.uubb': 2, 'V.ubnn': 11, 'V.ubnd': 7, 'V.ubnu': 6, 'V.ubnb': 9, 'V.ubdn': 13, 'V.ubdd': 5, 'V.ubdu': 3, 'V.ubdb': 2, 'V.ubun': 3, 'V.ubud': 4, 'V.ubuu': 0, 'V.ubub': 1, 'V.ubbn': 3, 'V.ubbd': 2, 'V.ubbu': 0, 'V.ubbb': 1, 'V.bnnn': 21, 'V.bnnd': 16, 'V.bnnu': 9, 'V.bnnb': 11, 'V.bndn': 13, 'V.bndd': 9, 'V.bndu': 6, 'V.bndb': 9, 'V.bnun': 13, 'V.bnud': 9, 'V.bnuu': 4, 'V.bnub': 10, 'V.bnbn': 7, 'V.bnbd': 7, 'V.bnbu': 3, 'V.bnbb': 10, 'V.bdnn': 18, 'V.bdnd': 13, 'V.bdnu': 10, 'V.bdnb': 6, 'V.bddn': 9, 'V.bddd': 8, 'V.bddu': 5, 'V.bddb': 1, 'V.bdun': 8, 'V.bdud': 4, 'V.bduu': 5, 'V.bdub': 1, 'V.bdbn': 3, 'V.bdbd': 7, 'V.bdbu': 5, 'V.bdbb': 1, 'V.bunn': 14, 'V.bund': 5, 'V.bunu': 9, 'V.bunb': 2, 'V.budn': 8, 'V.budd': 4, 'V.budu': 7, 'V.budb': 3, 'V.buun': 10, 'V.buud': 1, 'V.buuu': 3, 'V.buub': 2, 'V.bubn': 8, 'V.bubd': 1, 'V.bubu': 3, 'V.bubb': 2, 'V.bbnn': 8, 'V.bbnd': 9, 'V.bbnu': 4, 'V.bbnb': 10, 'V.bbdn': 3, 'V.bbdd': 1, 'V.bbdu': 2, 'V.bbdb': 1, 'V.bbun': 4, 'V.bbud': 3, 'V.bbuu': 1, 'V.bbub': 2, 'V.bbbn': 4, 'V.bbbd': 6, 'V.bbbu': 3, 'V.bbbb': 3, 'VI.AA': 22, 'VI.AB1': 37, 'VI.AB2': 21, 'VI.AB3': 15, 'VI.AC1': 7, 'VI.AC2': 16, 'VI.AC3': 7, 'VI.AC4': 16, 'VI.AC5': 10, 'VI.AC6': 6, 'VI.B1A': 43, 'VI.B1B1': 47, 'VI.B1B2': 35, 'VI.B1B3': 26, 'VI.B1C1': 18, 'VI.B1C2': 26, 'VI.B1C3': 16, 'VI.B1C4': 28, 'VI.B1C5': 16, 'VI.B1C6': 4, 'VI.B2A': 35, 'VI.B2B1': 54, 'VI.B2B2': 24, 'VI.B2B3': 25, 'VI.B2C1': 18, 'VI.B2C2': 26, 'VI.B2C3': 8, 'VI.B2C4': 29, 'VI.B2C5': 12, 'VI.B2C6': 5, 'VI.B3A': 16, 'VI.B3B1': 25, 'VI.B3B2': 21, 'VI.B3B3': 9, 'VI.B3C1': 3, 'VI.B3C2': 13, 'VI.B3C3': 6, 'VI.B3C4': 21, 'VI.B3C5': 10, 'VI.B3C6': 5, 'VI.C1A': 7, 'VI.C1B1': 9, 'VI.C1B2': 14, 'VI.C1B3': 7, 'VI.C1C1': 7, 'VI.C1C2': 9, 'VI.C1C3': 4, 'VI.C1C4': 6, 'VI.C1C5': 3, 'VI.C1C6': 1, 'VI.C2A': 20, 'VI.C2B1': 28, 'VI.C2B2': 28, 'VI.C2B3': 8, 'VI.C2C1': 13, 'VI.C2C2': 8, 'VI.C2C3': 5, 'VI.C2C4': 17, 'VI.C2C5': 5, 'VI.C2C6': 3, 'VI.C3A': 6, 'VI.C3B1': 7, 'VI.C3B2': 8, 'VI.C3B3': 7, 'VI.C3C1': 3, 'VI.C3C2': 6, 'VI.C3C3': 0, 'VI.C3C4': 6, 'VI.C3C5': 2, 'VI.C3C6': 0, 'VI.C4A': 11, 'VI.C4B1': 38, 'VI.C4B2': 25, 'VI.C4B3': 12, 'VI.C4C1': 8, 'VI.C4C2': 18, 'VI.C4C3': 7, 'VI.C4C4': 11, 'VI.C4C5': 11, 'VI.C4C6': 4, 'VI.C5A': 10, 'VI.C5B1': 18, 'VI.C5B2': 16, 'VI.C5B3': 8, 'VI.C5C1': 10, 'VI.C5C2': 12, 'VI.C5C3': 3, 'VI.C5C4': 9, 'VI.C5C5': 5, 'VI.C5C6': 1, 'VI.C6A': 0, 'VI.C6B1': 5, 'VI.C6B2': 6, 'VI.C6B3': 3, 'VI.C6C1': 0, 'VI.C6C2': 3, 'VI.C6C3': 1, 'VI.C6C4': 4, 'VI.C6C5': 1, 'VI.C6C6': 0, 'VII.A': 10, 'VII.B.1': 26, 'VII.B.2': 30, 'VII.B.3': 11, 'VII.C.1': 8, 'VII.C.2': 22, 'VII.C.3': 7, 'VII.C.4': 18, 'VII.C.5': 4, 'VII.C.6': 5, 'VII.D.1': 4, 'VII.D.2': 21, 'VII.D.3': 6, 'VII.D.4': 8, 'VII.D.5': 8, 'VII.D.6': 4, 'VII.E.1': 13, 'VII.E.2': 26, 'VII.E.3': 6, 'VII.E.4': 7, 'VII.E.5': 15, 'VII.E.6': 2, 'VII.F.ddd': 16, 'VII.F.ddu': 13, 'VII.F.ddb': 10, 'VII.F.dud': 10, 'VII.F.duu': 16, 'VII.F.dub': 9, 'VII.F.dbd': 10, 'VII.F.dbu': 7, 'VII.F.dbb': 7, 'VII.F.udd': 7, 'VII.F.udu': 11, 'VII.F.udb': 8, 'VII.F.uud': 11, 'VII.F.uuu': 12, 'VII.F.uub': 4, 'VII.F.ubd': 5, 'VII.F.ubu': 3, 'VII.F.ubb': 4, 'VII.F.bdd': 5, 'VII.F.bdu': 3, 'VII.F.bdb': 1, 'VII.F.bud': 4, 'VII.F.buu': 6, 'VII.F.bub': 9, 'VII.F.bbd': 7, 'VII.F.bbu': 3, 'VII.F.bbb': 0, 'VII.Ga.A': 1, 'VII.Ga.B': 7, 'VII.Ga.C': 1, 'VII.Ga.D': 2, 'VII.Ga.E': 5, 'VII.Ga.F': 5, 'VII.Ga.G': 1, 'VII.Gb.ddd': 3, 'VII.Gb.ddu': 5, 'VII.Gb.dud': 4, 'VII.Gb.duu': 2, 'VII.Gb.udd': 2, 'VII.Gb.udu': 2, 'VII.Gb.uud': 2, 'VII.Gb.uuu': 4, 'VII.Gc.1': 0, 'VII.Gc.2': 4, 'VII.Gc.3': 0, 'VII.Gd.1': 1, 'VII.Gd.2': 1, 'VII.Gd.3': 2, 'VII.Ge.1': 4, 'VII.Ge.2': 2, 'VII.Ge.4': 1, 'VII.Gf.1': 2, 'VII.Gf.2': 0, 'VII.Gg': 0, 'VIII.AA': 8, 'VIII.AB1': 12, 'VIII.AB2': 16, 'VIII.AB3': 9, 'VIII.AC1': 5, 'VIII.AC2': 15, 'VIII.AC3': 5, 'VIII.AC4': 6, 'VIII.AC5': 9, 'VIII.AC6': 4, 'VIII.B1A': 14, 'VIII.B1B1': 17, 'VIII.B1B2': 28, 'VIII.B1B3': 11, 'VIII.B1C1': 10, 'VIII.B1C2': 13, 'VIII.B1C3': 4, 'VIII.B1C4': 8, 'VIII.B1C5': 6, 'VIII.B1C6': 3, 'VIII.B2A': 22, 'VIII.B2B1': 28, 'VIII.B2B2': 34, 'VIII.B2B3': 20, 'VIII.B2C1': 10, 'VIII.B2C2': 22, 'VIII.B2C3': 8, 'VIII.B2C4': 8, 'VIII.B2C5': 11, 'VIII.B2C6': 2, 'VIII.B3A': 8, 'VIII.B3B1': 11, 'VIII.B3B2': 17, 'VIII.B3B3': 3, 'VIII.B3C1': 7, 'VIII.B3C2': 10, 'VIII.B3C3': 5, 'VIII.B3C4': 5, 'VIII.B3C5': 7, 'VIII.B3C6': 1, 'VIII.C1A': 3, 'VIII.C1B1': 7, 'VIII.C1B2': 4, 'VIII.C1B3': 5, 'VIII.C1C1': 3, 'VIII.C1C2': 2, 'VIII.C1C3': 2, 'VIII.C1C4': 5, 'VIII.C1C5': 4, 'VIII.C1C6': 1, 'VIII.C2A': 11, 'VIII.C2B1': 13, 'VIII.C2B2': 20, 'VIII.C2B3': 8, 'VIII.C2C1': 8, 'VIII.C2C2': 10, 'VIII.C2C3': 5, 'VIII.C2C4': 4, 'VIII.C2C5': 3, 'VIII.C2C6': 1, 'VIII.C3A': 4, 'VIII.C3B1': 10, 'VIII.C3B2': 10, 'VIII.C3B3': 1, 'VIII.C3C1': 4, 'VIII.C3C2': 5, 'VIII.C3C3': 0, 'VIII.C3C4': 2, 'VIII.C3C5': 1, 'VIII.C3C6': 0, 'VIII.C4A': 8, 'VIII.C4B1': 8, 'VIII.C4B2': 7, 'VIII.C4B3': 5, 'VIII.C4C1': 0, 'VIII.C4C2': 4, 'VIII.C4C3': 1, 'VIII.C4C4': 3, 'VIII.C4C5': 2, 'VIII.C4C6': 0, 'VIII.C5A': 5, 'VIII.C5B1': 12, 'VIII.C5B2': 15, 'VIII.C5B3': 8, 'VIII.C5C1': 2, 'VIII.C5C2': 8, 'VIII.C5C3': 2, 'VIII.C5C4': 1, 'VIII.C5C5': 4, 'VIII.C5C6': 1, 'VIII.C6A': 2, 'VIII.C6B1': 2, 'VIII.C6B2': 1, 'VIII.C6B3': 1, 'VIII.C6C1': 0, 'VIII.C6C2': 1, 'VIII.C6C3': 0, 'VIII.C6C4': 2, 'VIII.C6C5': 2, 'VIII.C6C6': 0, 'IX.A': 7, 'IX.B.1': 7, 'IX.B.2': 20, 'IX.B.3': 5, 'IX.C.1': 3, 'IX.C.2': 13, 'IX.C.3': 6, 'IX.C.4': 10, 'IX.C.5': 6, 'IX.C.6': 2, 'IX.D.1': 11, 'IX.D.2': 5, 'IX.D.3': 6, 'IX.D.4': 7, 'IX.D.5': 5, 'IX.D.6': 0, 'IX.E.1': 11, 'IX.E.2': 20, 'IX.E.3': 3, 'IX.E.4': 3, 'IX.E.5': 9, 'IX.E.6': 1, 'IX.F.ddd': 13, 'IX.F.ddu': 10, 'IX.F.ddb': 3, 'IX.F.dud': 6, 'IX.F.duu': 6, 'IX.F.dub': 5, 'IX.F.dbd': 10, 'IX.F.dbu': 7, 'IX.F.dbb': 1, 'IX.F.udd': 11, 'IX.F.udu': 5, 'IX.F.udb': 3, 'IX.F.uud': 7, 'IX.F.uuu': 10, 'IX.F.uub': 4, 'IX.F.ubd': 5, 'IX.F.ubu': 0, 'IX.F.ubb': 1, 'IX.F.bdd': 5, 'IX.F.bdu': 7, 'IX.F.bdb': 0, 'IX.F.bud': 4, 'IX.F.buu': 3, 'IX.F.bub': 1, 'IX.F.bbd': 2, 'IX.F.bbu': 1, 'IX.F.bbb': 2, 'IX.Ga.A': 0, 'IX.Ga.B': 3, 'IX.Ga.C': 2, 'IX.Ga.D': 6, 'IX.Ga.E': 2, 'IX.Ga.F': 2, 'IX.Ga.G': 0, 'IX.Gb.ddd': 5, 'IX.Gb.ddu': 3, 'IX.Gb.dud': 1, 'IX.Gb.duu': 4, 'IX.Gb.udd': 5, 'IX.Gb.udu': 1, 'IX.Gb.uud': 4, 'IX.Gb.uuu': 3, 'IX.Gc.1': 0, 'IX.Gc.2': 2, 'IX.Gc.3': 0, 'IX.Gd.1': 1, 'IX.Gd.2': 3, 'IX.Gd.3': 1, 'IX.Ge.1': 0, 'IX.Ge.2': 3, 'IX.Ge.4': 0, 'IX.Gf.1': 1, 'IX.Gf.2': 1, 'IX.Gg': 0}
        self.assertEqualDict(sma.countMultiMotifs(self.G, 2, 2), counts)


if __name__ == '__main__':
    unittest.main()
