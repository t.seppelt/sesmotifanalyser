.. _sec-appendix-erdos:

Appendix: Probabilities in Erdős-Rényi random graphs
====================================================

*See also* :ref:`sec-distribution`.

.. _sec-expected-densities:

Expected densities
++++++++++++++++++

3-motifs
--------

The following table lists the expected densities for 3-motifs. The distinct node
lies in :math:`V_0`, the two other nodes in :math:`V_1`. Here, :math:`p_{11}` denotes
the probability that an edge between two nodes from :math:`V_1` exists. :math:`p_{01}`
is the probability that an edge between the two levels exists. Multiply all values
by :math:`|V_0| \binom{|V_1|}{2}` to obtain the expected number of motifs. The function
:py:meth:`sma.expected3Motifs` uses the formulas in this table. It can be verified
that the values sum to one.

.. math ::
    
    \def\arraystretch{1.5}
    \begin{array}{l|l}
    \text{Motif} & \text{Expected density} \\ \hline
    \text{I.A} & (1-p_{11}) (1-p_{01})^{2} \\
    \text{I.B} & 2(1-p_{11})p_{01}(1-p_{01}) \\
    \text{I.C} & (1-p_{11})p_{01}^2 \\
    \text{II.A} & p_{11} (1-p_{01})^{2} \\
    \text{II.B} & 2p_{11}p_{01}(1-p_{01}) \\
    \text{II.C} & p_{11}p_{01}^2
    \end{array}

4-motifs
--------

The following table lists the expected densities for 4-motifs. The upper nodes lie
in :math:`V_0`, the other nodes in :math:`V_1`. Here, :math:`p_{11}` denotes
the probability that an edge between two nodes from :math:`V_1` exists. :math:`p_{01}`
is the probability that an edge between the two levels exists, etc. Multiply all values
by :math:`\binom{|V_0|}{2} \binom{|V_1|}{2}` to obtain the expected number of motifs.
The function :py:meth:`sma.expected4Motifs` uses the formulas in this table. 
It can be verified that the values sum to one.

.. math ::

    \def\arraystretch{1.5}
    \begin{array}{l|l}
    \text{Motif} & \text{Expected density} \\ \hline
    \text{I.A} & 2(1-p_{00})p_{01}^2(1-p_{01})^{2}(1-p_{11}) \\
    \text{I.B} & 2p_{00}p_{01}^2(1-p_{01})^{2}(1-p_{11}) \\
    \text{I.C} & 2(1-p_{00})p_{01}^2(1-p_{01})^{2}p_{11} \\
    \text{I.D} & 2p_{00}p_{01}^2(1-p_{01})^{2}p_{11} \\
    \text{II.A} & 2(1-p_{00})p_{01}^2(1-p_{01})^{2}(1-p_{11}) \\
    \text{II.B} & 2p_{00}p_{01}^2(1-p_{01})^{2}(1-p_{11}) \\
    \text{II.C} & 2(1-p_{00})p_{01}^2(1-p_{01})^{2}p_{11} \\
    \text{II.D} & 2p_{00}p_{01}^2(1-p_{01})^{2}p_{11} \\
    \text{III.A} & (1-p_{00})p_{01}^4(1-p_{11}) \\
    \text{III.B} & p_{00}p_{01}^4(1-p_{11}) \\
    \text{III.C} & (1-p_{00})p_{01}^4p_{11} \\
    \text{III.D} & p_{00}p_{01}^4p_{11} \\
    \text{IV.A} & (1-p_{00})(1-p_{01})^{4}(1-p_{11}) \\
    \text{IV.B} & p_{00}(1-p_{01})^{4}(1-p_{11}) \\
    \text{IV.C} & (1-p_{00})(1-p_{01})^{4}p_{11} \\
    \text{IV.D} & p_{00}(1-p_{01})^{4}p_{11} \\
    \text{V.A} & 4(1-p_{00})p_{01}^3(1-p_{01})(1-p_{11}) \\
    \text{V.B} & 4p_{00}p_{01}^3(1-p_{01})(1-p_{11}) \\
    \text{V.C} & 4(1-p_{00})p_{01}^3(1-p_{01})p_{11} \\
    \text{V.D} & 4p_{00}p_{01}^3(1-p_{01})p_{11} \\
    \text{VI.A} & 4p_{00}p_{01}(1-p_{01})^{3}(1-p_{11}) \\
    \text{VI.B} & 4p_{00}p_{01}(1-p_{01})^{3}p_{11} \\
    \text{VI.C} & 2p_{00}p_{01}^2(1-p_{01})^{2}(1-p_{11}) \\
    \text{VI.D} & 2p_{00}p_{01}^2(1-p_{01})^{2}p_{11} \\
    \text{VII.A} & 4(1-p_{00})p_{01}(1-p_{01})^{3}(1-p_{11}) \\
    \text{VII.B} & 4(1-p_{00})p_{01}(1-p_{01})^{3}p_{11} \\
    \text{VII.C} & 2(1-p_{00})p_{01}^2(1-p_{01})^{2}(1-p_{11}) \\
    \text{VII.D} & 2(1-p_{00})p_{01}^2(1-p_{01})^{2}p_{11} 
    \end{array} 

(1,1,1)-motifs
--------------

Let the three levels of a multi motif with arities 2, 2, 2 be indexed 0, 1, 2. Let
:math:`p_{ij}` for :math:`0 \leq i \leq j \leq 2` denote the probability that an
edge linking levels :math:`i` and :math:`j` exists. Then the expected densities are:

.. math ::

    \def\arraystretch{1.5}
    \begin{array}{l|l}
    \text{Motif} & \text{Expected density} \\ \hline
    0 & (1-p_{01})(1-p_{12})(1-p_{02}) \\
    1 & p_{01}(1-p_{12})(1-p_{02}) \\
    2 & (1-p_{01})p_{12}(1-p_{02}) \\
    3 & p_{01}p_{12}(1-p_{02}) \\
    4 & (1-p_{01})(1-p_{12})p_{02} \\
    5 & p_{01}(1-p_{12})p_{02} \\
    6 & (1-p_{01})p_{12}p_{02} \\
    7 & p_{01}p_{12}p_{02} 
    \end{array} 


(1,2,1)-motifs
--------------

Let the three levels of a multi motif with arities 1, 2, 1 be indexed 0, 1, 2. Let
:math:`p_{ij}` for :math:`0 \leq i \leq j \leq 2` denote the probability that an
edge linking levels :math:`i` and :math:`j` exists. Then the expected densities are:

.. math ::

    \def\arraystretch{1.5}
    \begin{array}{l|l}
    \text{Motif} & \text{Expected density} \\ \hline
    1 & 2p_{01}(1-p_{01})(1-p_{11})p_{12}^2(1-p_{02}) \\
    2 & p_{01}^2(1-p_{11})p_{12}^2(1-p_{02}) \\
    3 & p_{01}^2(1-p_{11})p_{12}^2p_{02} \\
    4 & p_{01}^2p_{11}p_{12}^2p_{02} \\
    -1 & \text{remainder} 
    \end{array} 

The expected density for the unclassified motifs (class :math:`-1`) equals one minus
the sum of the other densities. 

(2,2,1)-motifs
--------------

Let the three levels of a multi motif with arities 2, 2, 1 be indexed 0, 1, 2. Let
:math:`p_{ij}` for :math:`0 \leq i \leq j \leq 2` denote the probability that an
edge linking levels :math:`i` and :math:`j` exists. Such a motif consists of a
4-motif in the upper part extended by a lower node. The classification of such 
motifs relies on the classification of 4-motifs. For this reason, the following
table only provides factors. For obtaining expected densities one has to multiply
them with the expected densities for 4-motifs listed in one of the preceding sections.

.. math ::

    \def\arraystretch{1.5}
    \begin{array}{l|l}
    \text{Motif} & \text{Factor} \\ \hline
    \text{\it CLASS}.0 & (1-p_{02})^2(1-p_{12})^2 \\
    \text{\it CLASS}.1 & p_{02}^2(1-p_{12})^2 \\
    \text{\it CLASS}.2 & (1-p_{02})^2p_{12}^2 \\
    \text{\it CLASS}.3 & p_{02}^2p_{12}^2 \\
    \text{Unclassified} & \text{remainder} 
    \end{array} 

For example, the expected density for motifs of class III.A.3 is then 
:math:`p_{02}^2p_{12}^2 \cdot (1-p_{00})p_{01}^4(1-p_{11})`.
The expected density for the unclassified motifs equals one minus
the sum of the other densities.

(2,2,2)-motifs
--------------

Let the three levels of a multi motif with arities 2, 2, 2 be indexed 0, 1, 2. Let
:math:`p_{ij}` for :math:`0 \leq i \leq j \leq 2` denote the probability that an
edge linking levels :math:`i` and :math:`j` exists. Then the expected densities are:

.. math ::

    \def\arraystretch{1.5}
    \begin{array}{l|l}
    \text{Motif} & \text{Expected density} \\ \hline
    1 & 2(1-p_{00})(1-p_{01})^2p_{01}^2(1-p_{12})^2(1-p_{11})p_{12}^2p_{22}(1-p_{02})^4 \\
    2 & 2p_{00}(1-p_{01})^2p_{01}^2(1-p_{12})^2(1-p_{11})p_{12}^2p_{22}(1-p_{02})^4 \\
    3 & (1-p_{00})p_{01}^4p_{02}^4(1-p_{11})p_{12}^4p_{22} \\
    4 & p_{00}p_{01}^4p_{02}^4(1-p_{11})p_{12}^4p_{22} \\
    -1 & \text{remainder} 
    \end{array} 

The expected density for the unclassified motifs (class :math:`-1`) equals one minus
the sum of the other densities.

Second moments
++++++++++++++

Variances are computed by determining second moments and expectation. Therefore this
section focuses on second moments. As described in :ref:`sec-distribution`, we need
to compute a sum over all choices of two motifs. These motifs can overlap in different
ways giving different probabilities. The following sections describe how the sums
are split and which probabilities the different cases give.

3-motifs
--------

.. math ::
    
    \def\arraystretch{1.5}
    \small
    \begin{array}{l|lll} 
    \text{Overlap} & \text{all nodes} & \text{one from each level} & \text{two non-distinct nodes} \\
    \text{Factor} & |V_0| \binom{|V_1|}{2} 
                  & |V_0|(|V_0|-1)|V_1|(|V_1|-1) 
                  & \binom{|V_1|}{2} |V_0|(|V_0|-1)  \\ \hline
    \text{I.A} & (1-p_{01})^2(1-p_{11}) & (1-p_{01})^3(1-p_{11})^2 & (1-p_{01})^4(1-p_{11}) \\
    \text{I.B} & 2(1-p_{01})(1-p_{11})p_{01} & (1-p_{11})^2((1-p_{01})^2p_{01}+p_{01}^2(1-p_{01})) & 4(1-p_{01})^2p_{01}^2(1-p_{11}) \\
    \text{I.C} & p_{01}^2(1-p_{11}) & p_{01}^3(1-p_{11})^2 & p_{01}^4(1-p_{11}) \\
    \text{II.A} & (1-p_{01})^2p_{11} & (1-p_{01})^3p_{11}^2 & (1-p_{01})^4p_{11} \\
    \text{II.B} & 2(1-p_{01})p_{11}p_{01} & p_{11}^2((1-p_{01})^2p_{01}+p_{01}^2(1-p_{01})) & 4(1-p_{01})^2p_{01}^2p_{11} \\
    \text{II.C} & p_{01}^2p_{11} & p_{01}^3p_{11}^2 & p_{01}^4p_{11}\\ \hline \hline
    \text{Overlap} & \text{one non-distinct node} & \text{one distinct node} & \text{none}  \\
    \text{Factor} & |V_0|(|V_0|-1)|V_1|(|V_1|-1)(|V_1|-2) 
                  & |V_0|\binom{|V_1|}{2}\binom{|V_1|-2}{2}
                  & |V_0|(|V_0|-1) \binom{|V_1|}{2}\binom{|V_1|-2}{2} \\ \hline
    \text{I.A} & (1-p_{01})^4(1-p_{11})^2 & (1-p_{01})^4(1-p_{11})^2 & (1-p_{01})^4(1-p_{11})^2 \\
    \text{I.B}  & 4(1-p_{01})^2p_{01}^2(1-p_{11})^2 & 4(1-p_{01})^2p_{01}^2(1-p_{11})^2 & 4(1-p_{01})^2p_{01}^2(1-p_{11})^2 \\
    \text{I.C} & p_{01}^4(1-p_{11})^2 & p_{01}^4(1-p_{11})^2 & p_{01}^4(1-p_{11})^2\\
    \text{II.A} & (1-p_{01})^4p_{11}^2 & (1-p_{01})^4p_{11}^2 & (1-p_{01})^4p_{11}^2\\
    \text{II.B} & 4(1-p_{01})^2p_{01}^2p_{11}^2 & 4(1-p_{01})^2p_{01}^2p_{11}^2 & 4(1-p_{01})^2p_{01}^2p_{11}^2 \\
    \text{II.C} & p_{01}^4p_{11}^2 & p_{01}^4p_{11}^2 & p_{01}^4p_{11}^2 \\
    \end{array}

The second moments for a class of motifs can be computed by summing the products of 
the factors and the corresponding probabilities.

The formulas have been computed with great care and can be reproduced by considering
all possibilities for the overlap of two 3-motifs. The following combinatorial
observations can be made given this table:

    - The factors sum to :math:`|V_0|^2 \binom{|V_1|}{2}^2`, the square of the
      total number of 3-motifs.
    - The first columns and the columns in the bottom row are closely related to
      the formulas for the expectation of 3-motifs, cf. :ref:`sec-expected-densities`.
      The first column equals the formula for the expectation while the columns
      in the bottom row equal their squares. This is because in these scenarios
      the overlapping subgraph does not contain any edges.
    - The first column sums to one.
    - The second column sums to :math:`(2p_{01}^2 - 2p_{01} + 1)(2p_{11}^2 - 2p_{11} + 1)`,
      an expression symmetrical in :math:`p_{01}` and :math:`p_{11}` and minimal
      if :math:`p_{01} = p_{11} = 1/2`. The expression :math:`2p^2 - 2p +1 = (1-p)^2 + p^2`
      arises as the probability of two randomly with probability :math:`p` chosen
      edges to be both present or both absent.
    - The third column sums to :math:`6p_{01}^4 - 12 p_{01}^3 + 10 p_{01}^2 - 4 p_{01} + 1`
      which equals :math:`(1-p_{01})^4 + 4 p_{01}^2 (1-p_{01})^2 + p_{01}^4`. This
      is the probability that two 3-motifs overlapping in two non-distinct nodes
      are isomorphic. Node that this quantity does not depend on :math:`p_{11}`.

4-motifs
--------

Two 4-motifs can overlap in the following ways:
    
    (a) all nodes, occurring :math:`\binom{|V_0|}{2} \binom{|V_1|}{2}` times,
    (b) two upper nodes and one bottom node,
        occurring :math:`\binom{|V_0|}{2} |V_1| (|V_1| - 1) (|V_1| - 2)` times,
    (c) one upper node and two bottom nodes,
        occurring :math:`\binom{|V_1|}{2} |V_0|(|V_0| - 1) (|V_0| - 2)` times,
    (d) one upper node and one bottom node,
        occurring :math:`|V_0| |V_1| (|V_0| - 1) (|V_1| - 1) (|V_0| - 2) (|V_1| - 2)` times,
    (e) two upper nodes,
        occurring :math:`\binom{|V_0|}{2} \binom{|V_1| - 2}{2} \binom{|V_1|}{2}` times,
    (f) two bottom nodes,
        occurring :math:`\binom{|V_1|}{2} \binom{|V_0| - 2}{2} \binom{|V_0|}{2}` times,
    (g) one upper node,
        occurring :math:`\binom{|V_1|}{2} \binom{|V_0| - 2}{2} \binom{|V_0|}{2}` times,
    (h) one lower node,
        occurring :math:`|V_1| \binom{|V_0|}{2} \binom{|V_0| - 2}{2} (|V_1| - 1) (|V_1| - 2)` times,
    (i) no overlap,
        occurring :math:`\binom{|V_0|}{2} \binom{|V_0| - 2}{2} \binom{|V_1|}{2} \binom{|V_1| - 2}{2}` times.

These numbers sum to :math:`\left( \binom{|V_0|}{2} \binom{|V_1|}{2} \right)^2`.
The probabilities for the 28 possible 4-motifs can mostly be derived from their
expectations, cf. :ref:`sec-expected-densities`. In particular, the probabilities
for (a) are the same as the expected densities. In (g), (h) and (i) none of the
edges overlap, so the probabilities equal the squares of the expected densities.
The probabilities for (e) and (f) can be computed by dividing the squares of the
expected densities by the probability of the single edge that is counted twice.
Hence, it remains to compute the probabilities for (b), (c) and (d). They
are given in the following tables:

The values for overlap (b) and (c) are computed by multiplying the entries in the
respective columns with the corresponding value in the column "Factor". This is
because the only difference between (b) and (c) is that the number of nodes and
hence relevant edges on the top and the bottom level are interchanged.

.. math ::
    
    \def\arraystretch{1.5}
    \small
    \begin{array}{l|l|lll} 
    \text{Motif} & \text{Factor} & \text{Overlap (b)} & \text{Overlap (c)} \\ \hline
    \text{I.A} & 2  (1-p_{01})^3  p_{01}^3 & (1-p_{00})  (1-p_{11})^2 & (1-p_{00})^2  (1-p_{11})\\
    \text{I.B} & 2  (1-p_{01})^3  p_{01}^3 & p_{00}      (1-p_{11})^2 & p_{00}    ^2  (1-p_{11})\\
    \text{I.C} & 2  (1-p_{01})^3  p_{01}^3 & (1-p_{00})  p_{11}^2 & (1-p_{00})^2  p_{11}\\
    \text{I.D} & 2  (1-p_{01})^3  p_{01}^3 & p_{00}      p_{11}^2 & p_{00}    ^2  p_{11}\\
    \text{II.A} & (p_{01}^4  (1-p_{01})^2 + p_{01}^2  (1-p_{01})^4) & (1-p_{00})  (1-p_{11})^2 & (1-p_{00})^2  (1-p_{11})\\
    \text{II.B} & (p_{01}^4  (1-p_{01})^2 + p_{01}^2  (1-p_{01})^4) & p_{00}      (1-p_{11})^2 & p_{00}    ^2  (1-p_{11})\\
    \text{II.C} & (p_{01}^4  (1-p_{01})^2 + p_{01}^2  (1-p_{01})^4) & (1-p_{00})  p_{11}^2 & (1-p_{00})^2  p_{11}\\
    \text{II.D} & (p_{01}^4  (1-p_{01})^2 + p_{01}^2  (1-p_{01})^4) & p_{00}      p_{11}^2 & p_{00}    ^2  p_{11}\\
    \text{III.A} & p_{01}^6 & (1-p_{00})  (1-p_{11})^2 & (1-p_{00})^2  (1-p_{11})\\
    \text{III.B} & p_{01}^6 & p_{00}      (1-p_{11})^2 & p_{00}    ^2  (1-p_{11})\\
    \text{III.C} & p_{01}^6 & (1-p_{00})  p_{11}^2 & (1-p_{00})^2  p_{11}\\
    \text{III.D} & p_{01}^6 & p_{00}      p_{11}^2 & p_{00}    ^2  p_{11}\\
    \text{IV.A} & (1-p_{01})^6 & (1-p_{00})  (1-p_{11})^2 & (1-p_{00})^2  (1-p_{11})\\
    \text{IV.B} & (1-p_{01})^6 & p_{00}      (1-p_{11})^2 & p_{00}    ^2  (1-p_{11})\\
    \text{IV.C} & (1-p_{01})^6 & (1-p_{00})  p_{11}^2 & (1-p_{00})^2  p_{11}\\
    \text{IV.D} & (1-p_{01})^6 & p_{00}      p_{11}^2 & p_{00}    ^2  p_{11}\\
    \text{V.A} & (2  p_{01}^5  (1-p_{01}) + 4  p_{01}^4  (1-p_{01})^2) & (1-p_{00})  (1-p_{11})^2 & (1-p_{00})^2  (1-p_{11})\\
    \text{V.B} & (2  p_{01}^5  (1-p_{01}) + 4  p_{01}^4  (1-p_{01})^2) & p_{00}      (1-p_{11})^2 & p_{00}    ^2  (1-p_{11})\\
    \text{V.C} & (2  p_{01}^5  (1-p_{01}) + 4  p_{01}^4  (1-p_{01})^2) & (1-p_{00})  p_{11}^2 & (1-p_{00})^2  p_{11}\\
    \text{V.D} & (2  p_{01}^5  (1-p_{01}) + 4  p_{01}^4  (1-p_{01})^2) & p_{00}      p_{11}^2 & p_{00}    ^2  p_{11}\\
    \text{VI.A} & (4  p_{01}^2  (1-p_{01})^4 + 2  p_{01}  (1-p_{01})^5) & p_{00}  (1-p_{11})^2 & p_{00}^2  (1-p_{11})\\
    \text{VI.B} & (4  p_{01}^2  (1-p_{01})^4 + 2  p_{01}  (1-p_{01})^5) & p_{00}  p_{11}^2 & p_{00}^2  p_{11}\\
    \text{VI.C} & 2  p_{01}^3  (1-p_{01})^3 & p_{00}  (1-p_{11})^2 & p_{00}^2  (1-p_{11})\\
    \text{VI.D} & 2  p_{01}^3  (1-p_{01})^3 & p_{00}  p_{11}^2 & p_{00}^2  p_{11}\\
    \text{VII.A} & (4  p_{01}^2  (1-p_{01})^4 + 2  p_{01}  (1-p_{01})^5) & (1-p_{00})  (1-p_{11})^2 & (1-p_{00})^2  (1-p_{11})\\
    \text{VII.B} & (4  p_{01}^2  (1-p_{01})^4 + 2  p_{01}  (1-p_{01})^5) & (1-p_{00})  p_{11}^2 & (1-p_{00})^2  p_{11}\\
    \text{VII.C} & 2  p_{01}^3  (1-p_{01})^3 & (1-p_{00})  (1-p_{11})^2 & (1-p_{00})^2  (1-p_{11})\\
    \text{VII.D} & 2  p_{01}^3  (1-p_{01})^3 & (1-p_{00})  p_{11}^2 & (1-p_{00})^2  p_{11}
    \end{array}

.. math ::
    
    \def\arraystretch{1.5}
    \small
    \begin{array}{l|lll} 
    \text{Motif} & \text{Overlap (d)} \\ \hline
    \text{I.A} & (1-p_{00})^2(1-p_{11})^2(p_{01}^3(1-p_{01})^4 + p_{01}^4(1-p_{01})^3) \\
    \text{I.B} & p_{00}^2(1-p_{11})^2(p_{01}^3(1-p_{01})^4 + p_{01}^4(1-p_{01})^3) \\
    \text{I.C} & (1-p_{00})^2p_{11}^2(p_{01}^3(1-p_{01})^4 + p_{01}^4(1-p_{01})^3) \\
    \text{I.D} & p_{00}^2p_{11}^2(p_{01}^3(1-p_{01})^4 + p_{01}^4(1-p_{01})^3) \\
    \text{II.A} & (1-p_{00})^2(1-p_{11})^2(p_{01}^3(1-p_{01})^4 + p_{01}^4(1-p_{01})^3) \\
    \text{II.B} & p_{00}^2(1-p_{11})^2(p_{01}^3(1-p_{01})^4 + p_{01}^4(1-p_{01})^3) \\
    \text{II.C} & (1-p_{00})^2p_{11}^2(p_{01}^3(1-p_{01})^4 + p_{01}^4(1-p_{01})^3) \\
    \text{II.D} & p_{00}^2p_{11}^2(p_{01}^3(1-p_{01})^4 + p_{01}^4(1-p_{01})^3) \\
    \text{III.A} & (1-p_{00})^2(1-p_{11})^2p_{01}^7 \\
    \text{III.B} & p_{00}^2(1-p_{11})^2p_{01}^7 \\
    \text{III.C} & (1-p_{00})^2p_{11}^2p_{01}^7 \\
    \text{III.D} & p_{00}^2p_{11}^2p_{01}^7 \\
    \text{IV.A} & (1-p_{00})^2(1-p_{11})^2(1-p_{01})^7 \\
    \text{IV.B} & p_{00}^2(1-p_{11})^2(1-p_{01})^7 \\
    \text{IV.C} & (1-p_{00})^2p_{11}^2(1-p_{01})^7 \\
    \text{IV.D} & p_{00}^2p_{11}^2(1-p_{01})^7 \\
    \text{V.A} & (1-p_{00})^2(1-p_{11})^2(9p_{01}^5(1-p_{01})^2 + p_{01}^6(1-p_{01})) \\
    \text{V.B} & p_{00}^2(1-p_{11})^2(9p_{01}^5(1-p_{01})^2 + p_{01}^6(1-p_{01})) \\
    \text{V.C} & (1-p_{00})^2p_{11}^2(9p_{01}^5(1-p_{01})^2 + p_{01}^6(1-p_{01})) \\
    \text{V.D} & p_{00}^2p_{11}^2(9p_{01}^5(1-p_{01})^2 + p_{01}^6(1-p_{01})) \\
    \text{VI.A} & p_{00}^2(1-p_{11})^2(9p_{01}^2(1-p_{01})^5 + p_{01}(1-p_{01})^6) \\
    \text{VI.B} & p_{00}^2p_{11}^2(9p_{01}^2(1-p_{01})^5 + p_{01}(1-p_{01})^6) \\
    \text{VI.C} & p_{00}^2(1-p_{11})^2(p_{01}^3(1-p_{01})^4 + p_{01}^4(1-p_{01})^3) \\
    \text{VI.D} & p_{00}^2p_{11}^2(p_{01}^3(1-p_{01})^4 + p_{01}^4(1-p_{01})^3) \\
    \text{VII.A} & (1-p_{00})^2(1-p_{11})^2(9p_{01}^2(1-p_{01})^5 + p_{01}(1-p_{01})^6) \\
    \text{VII.B} & (1-p_{00})^2p_{11}^2(9p_{01}^2(1-p_{01})^5 + p_{01}(1-p_{01})^6) \\
    \text{VII.C} & (1-p_{00})^2(1-p_{11})^2(p_{01}^3(1-p_{01})^4 + p_{01}^4(1-p_{01})^3) \\
    \text{VII.D} & (1-p_{00})^2p_{11}^2(p_{01}^3(1-p_{01})^4 + p_{01}^4(1-p_{01})^3) 
    \end{array}
