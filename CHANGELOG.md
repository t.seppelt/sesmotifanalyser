# SESMotifAnalyser Change Log

## Version 2.3.4

* calls to deprecated function `networkx.from_numpy_matrix` removed.

## Version 2.3.3

* adding support for (1,3)-motifs
  see [motifr issue #69](https://github.com/marioangst/motifr/issues/69)

## Version 2.3.2

* bug in findIdealCounter fixed which resulted in unsuitable optimisations
  for counting (2,2,1)-motifs, 
  see [motifr issue #68](https://github.com/marioangst/motifr/issues/68)

## Version 2.3.1

* motifs with signature (2,3) added, 
  see [motifr issue #66](https://github.com/marioangst/motifr/issues/66)

## Version 2.3.0

* Introducing progress reports for counting functions. Use the ``progress_report`` parameter of counting functions to get a progress bar. This feature is activated by default for the R interface. See [motifr issue #57](https://github.com/marioangst/motifr/issues/57).
* **Breaking** The functions count3Motifs and count4Motifs now have parameters (G, level0, level1, **kwargs).
  The parameter iterator has been dropped. This is in line with the requirements for
  the counter function in motif database entries.
* count3pMotifsLinalg for fast counting of plain 3-motifs has been added. See [motifr issue #58](https://github.com/marioangst/motifr/issues/58).
* count2pMotifsLinalg for fast counting of plain 2-motifs has been added. See [motifr issue #58](https://github.com/marioangst/motifr/issues/58).

## Version 2.2.1

* bug fix in classification for directed (2,2)-motifs

## Version 2.2.0

* adding support for directed (2,2)-motifs

## Version 2.1.2

*This version is required by motifr 1.0.0*

* randomDiSENs() added
* exemplifyMotif() now returns None instead of raising StopIteration when no motif of the specified type can be found

## Version 2.1.1

* resolving issue in R interface function translateGraph() causing confusion with user-defined level attribute names

## Version 2.1.0

*Getting ready for next motifr release!*

* directed motifs with signature (1,2) added
* better error handling
* clearer error messages in motif identifier string parsing facility
* testing extended

## Version 2.0.11

* motifTable() added
* support for directed motifs
    * breaking changes in supportedSignatures() and motifInfo()
    * property directed added to motif info objects
    * two classificators for directed motifs added
    * Python-R translation mechanism now supports directed networks
    * testing

## Version 2.0.10

* documentation improved
* bug fix in var4Motifs
* refactoring
* preparing R interface for multiprocessing
* expected111Motifs added

## Version 2.0.9

* packaging issue resolved

## Version 2.0.8

* documentation improved (bibliography added)
* degreeDistribution(), nodesByType() added
* nodesCount() can handle empty networks now
* mistakes in closed/open projections for 4-motifs removed
* variances for 4-motifs in Erdős-Rényi model added
* small speed-up for 3- and 4-motif classificators

## Version 2.0.7

* documentation improved, optimizations, bug fixes
* classificator for 1-motifs added
* level selection in distributionMotifsActorsChoice() now in accordance with identifyGaps()
* typos fixed: splitMotifIdentifer() is now splitMotifIdentifier()
* simulation for Actor's Choice model added

## Version 2.0.6

* bug fixes
* increased test coverage
* supportedSignatures() returns now an iterator instead of a list
* randomMultiSENs() is now randomMultiSENsFixedDensities()
* randomMultiSENsErdosRenyi() added
* model parameter added to randomSimilarMultiSENs()
