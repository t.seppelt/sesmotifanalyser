.. _sec-multi:

An Example: Multi-level SENs
============================

In this section we will see how to analyse multi-level SENs with this framework.
We will go through all steps of SEN analysis, from generation of random SENs to 
counting motifs of different classes. In general this software focuses on two-level
networks where the first level (type 0) is referred to as ecological and the second
level (type 1) is called social. In contrary to that we will in this section look
into the analysis of three-level networks. We will refer to the levels as

    0. Actor level
    1. Venue level
    2. Issue level

The levels are encoded as nodal attribute (``sesType``) taking 0, 1 or 2 as its
value.

The Python script that contains the described analysis can be downloaded from 
`Gitlab <https://gitlab.com/t.seppelt/sesmotifanalyser/blob/master/examples/example_multilevel.py>`_.
It should be executable without further setup.

Generation of random multi-level SENs
-------------------------------------

Random multi-level SENs can be computed using an adapted Erdős-Rényi model. The
probabilities of the single edges are all the same. We will use 
:py:meth:`sma.randomMultiSENs` for generating a three-level network with 10 actors,
3 venues and 7 issues. We will need to define how many edges connecting the different
types of nodes should be present in the random networks. These values must be given
as a upper-triangular matrix.

.. literalinclude:: ../examples/example_multilevel.py
   :lines: 12-18
   :linenos:
    
Note that the object obtained in line 4 is a Python generator. We have to call ``next``
on it in order to obtain the graph object ``G``, cf. :ref:`sec-generate`.

For the example discussed in this section we will used a fixed (previously random)
graph available in the ``data/``-folder. Let's load it.

.. literalinclude:: ../examples/example_multilevel.py
   :lines: 20-22


Drawing a multi-level SEN
-------------------------

It may be useful to obtain a visual impression of the just generated random network.
We will continue with the example from the previous section. For drawing the
network we will use :py:meth:`sma.drawSEN`. 

.. literalinclude:: ../examples/example_multilevel.py
   :lines: 24-25
    
The result looks as follows:

.. image :: _static/multilevel.pdf
	:align: center
	:width: 50%

Listing motifs
--------------

Having a SEN at hand, we can now start to count the motifs in it. Motifs are tuples
of nodes, which can be classified based on the edges linking the nodes. Motifs can
consist of different amounts of nodes from the separate levels. These amounts are 
called the *signature* of the motif. In a two-level (social-ecological) network there are for example

    - 4-motifs consisting of two social and two ecological nodes,
    - 3E-motifs consisting of one ecological and two social nodes,
    - 3S-motifs consisting of one social and two ecological nodes,
    - 2-motifs consisting of one social and one ecological nodes,
    - 3p-motifs consisting of three nodes of the same type.
    
This list could surely be prolonged. We will start with computing lists of all motifs
of a given signature. In the next section we will use these lists to classify the motifs
in them.

This software uses Python iterators called :ref:`sec-motif-sources` to represent 
sets of motifs. In this introductory section we will look at the following motifs:

.. image :: _static/multilevel-classes.pdf
	:align: center
	:width: 90%
	
For obtaining a list of all motifs we use an instance of :py:class:`sma.MultiMotifs`.
As parameters we need to specify how many nodes should be taken from each level of the network.
We have to call ``list`` on the object in order to compute the set of motifs:

.. literalinclude:: ../examples/example_multilevel.py
   :lines: 27-41

Some technical remarks: The motifs provided by :py:class:`sma.MultiMotifs` are 
Python tuples consisting of one entry for each level of the network. The entries 
are themselves tuples consisting of the nodes. Sometimes these tuples are empty,
i.e. ``()``. The internal mechanisms of :py:class:`sma.MultiMotifs` are fundamentally 
different from those of the classes for two-level SENs, e.g. :py:class:`sma.FourMotifs`.
Most tools are not compatible. This should not bother us here.

How many motifs did we obtain? The motifs for (h)-(i) are now stored in ``list4``.
Its length is 210. This is what we would expect since 
:math:`{10 \\\choose 1}{3 \\\choose 2}{7 \\\choose 1} = 210`.
    
Classifying motifs
------------------

Every signature of motifs comes with different classes. All motif classes supported
by this software are listed in :ref:`sec-zoo`. This software uses objects called
:ref:`sec-motif-classificator` for classifying motifs. Since we are interested in
motifs spanning across more than two levels, we will in particular be using
:py:class:`sma.MultiMotifClassificator`.

First we want to see how many motifs in ``list1`` are of classes (a), (b) and (c).
Since here only two levels of the SEN are involved, namely the actors level and the venues
level, :py:class:`sma.MultiMotifClassificator` falls back to :py:class:`sma.ThreeMotifClassificator`,
a classificator for 3-motifs in two-level SENs. The possible classes outlined 
in :ref:`sec-terminology`. (a) correspondes to II.C, (b) to I.C and (c) to II.B. 
Again we need to specify how many nodes are taken from each level of the network.

The classificator object can then be used to classify individual motifs or lists
of motifs using :py:meth:`map`. Motifs can be counted using :py:meth:`sma.countMotifs`.

.. literalinclude:: ../examples/example_multilevel.py
   :lines: 43-53

In other words, (a) occurs 8 times in the example, (b) 14 times and (c) 16 times.
We can count the occurrences of patterns (f) and (g) analogously: (f) corresponds
to II.B while (g) corresponds to II.C.


.. literalinclude:: ../examples/example_multilevel.py
   :lines: 55-56
    
Note that :py:class:`sma.MultiMotifClassificator` detects automatically what the signature
and the roles of the motifs are. Based on the parameters ``0, 1, 2`` it automatically
chooses the venue level as the origin of the distinct node in the motif described
in :ref:`sec-terminology`.

We continue with (d) and (e). (d) corresponds to ``6``, (e) to ``7``. 
Apart from classifying all motifs we extract lists of motifs of the two interesting classes:

.. literalinclude:: ../examples/example_multilevel.py
   :lines: 58-67

For motif classes (j) and (o) need to specify ``2, 2, 0`` as parameter for the arities. :py:class:`sma.MultiMotifClassificator`
uses then an instance of :py:class:`sma.FourMotifClassificator` for classifying
the motifs. The classification follows Ö. Bodin, M. Tengo (2012). (j) corresponds 
to I.B, (o) corresponds to V.B.

.. literalinclude:: ../examples/example_multilevel.py
   :lines: 69-71

We treat the remaining cases of (h), (i), (k)-(n) analogously. Note that this software provides only
a partial classification for motifs of these signatures. (h) is classified as 2, (i) as 1,
(k) corresponds to I.B.2 while (l) corresponds to I.A.2. (m) is classified as 2, (n) as 1.
Motifs not matching these classes are classified either as -1 or as Unclassified.

.. literalinclude:: ../examples/example_multilevel.py
   :lines: 73-91

We finally give an overview over the motifs studied here, their classificators
and the terminology used. This should also serve as part of the documentation
of :py:class:`sma.MultiMotifClassificator`.

+-------------------------+-------------------+---------------------------------+
| **Motif in the figure** | **Classificator** | **Classificator's terminology** |
+-------------------------+-------------------+---------------------------------+
| Motif (a)               | 2, 1, 0           | II.C                            |
+-------------------------+                   +---------------------------------+
| Motif (b)               |                   | I.C                             |
+-------------------------+                   +---------------------------------+
| Motif (c)               |                   | II.B                            |
+-------------------------+-------------------+---------------------------------+
| Motif (d)               | 1, 1, 1           | 6                               |
+-------------------------+                   +---------------------------------+
| Motif (e)               |                   | 7                               |
+-------------------------+-------------------+---------------------------------+
| Motif (f)               | 0, 1, 2           | II.B                            |
+-------------------------+                   +---------------------------------+
| Motif (g)               |                   | II.C                            |
+-------------------------+-------------------+---------------------------------+
| Motif (h)               | 1, 2, 1           | 2                               |
+-------------------------+                   +---------------------------------+
| Motif (i)               |                   | 1                               |
+-------------------------+-------------------+---------------------------------+
| Motif (j)               | 2, 2, 0           | I.B                             |
+-------------------------+                   +---------------------------------+
| Motif (o)               |                   | V.B                             |
+-------------------------+-------------------+---------------------------------+
| Motif (k)               | 2, 2, 1           | I.B.2                           |
+-------------------------+                   +---------------------------------+
| Motif (l)               |                   | I.A.2                           |
+-------------------------+-------------------+---------------------------------+
| Motif (m)               | 2, 2, 2           | 2                               |
+-------------------------+                   +---------------------------------+
| Motif (n)               |                   | 1                               |
+-------------------------+-------------------+---------------------------------+

See also :ref:`sec-zoo` for a full list of all motif classes.