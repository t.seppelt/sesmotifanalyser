Auxiliary functions
====================

Identifying motifs
------------------

Functions in this section are used to identify multi-level motifs, for example in
:py:class:`sma.MultiMotifClassificator`, and for interpreting motif identifier
strings as defined in :ref:`sec-terminology`.
Structural information about motifs are kept in subclasses of :py:class:`sma.MotifInfo`
and can be accessed via functions described in this section.

Motif database
++++++++++++++

.. autofunction :: sma.motifInfo
.. autofunction :: sma.supportedSignatures

.. autoclass :: sma.MotifInfo
    :members:

The following families of motifs are supported. Visit the motif zoo in
:ref:`sec-zoo` for details.

One level motifs
****************

.. autoclass :: sma.Motif3pInfo
.. autoclass :: sma.Motif1Info
.. autoclass :: sma.Motif2Info
.. autoclass :: sma.DiMotif2Info

Two levels motifs
*****************

.. autoclass :: sma.Motif11Info
.. autoclass :: sma.Motif3Info
.. autoclass :: sma.Motif4Info
.. autoclass :: sma.Motif23Info
.. autoclass :: sma.Motif13Info
.. autoclass :: sma.DiMotif11Info
.. autoclass :: sma.DiMotif12Info
.. autoclass :: sma.DiMotif22Info

Three levels motifs
*******************

.. autoclass :: sma.Motif111Info
.. autoclass :: sma.Motif121Info
.. autoclass :: sma.Motif221Info
.. autoclass :: sma.Motif222Info

Working with Motif Identifier Strings
+++++++++++++++++++++++++++++++++++++

.. autofunction :: sma.multiSignature
.. autofunction :: sma.splitMotifIdentifier
.. autofunction :: sma.parseMotifIdentifier
.. autofunction :: sma.groupMotifIdentifiers

Position matching
+++++++++++++++++

*See also* :ref:`sec-position_matching`.

.. autofunction :: sma.matchPositions
.. autofunction :: sma.exemplifyMotif
.. autofunction :: sma.sortPositions

Manipulating SENs
-----------------

.. autofunction :: sma.addRandomEdges
.. autofunction :: sma.multiToWeightedGraph

Constants
---------

.. data :: sma.NODE_TYPE_SOC
.. data :: sma.NODE_TYPE_ECO
.. data :: sma.EDGE_TYPE_SOC_SOC
.. data :: sma.EDGE_TYPE_ECO_ECO
.. data :: sma.EDGE_TYPE_ECO_SOC

.. data :: sma.MOTIF4_NAMES
.. data :: sma.MOTIF3_NAMES
.. data :: sma.MOTIF3_AUT

.. data :: sma.COLORS_TYPES

.. data :: sma.MULTI_DEFAULT_NAMES

.. data :: sma.MOTIF3_EDGES
.. data :: sma.MOTIF4_EDGES
.. data :: sma.MOTIF4_SYMMETRIES
.. data :: sma.MOTIF4_AUT

.. data :: sma.MODEL_ERDOS_RENYI
.. data :: sma.MODEL_ACTORS_CHOICE
.. data :: sma.MODEL_FIXED_DENSITIES

Motif classificators
--------------------
These are internal helper functions. See :ref:`sec-motif-classificator` for more
practical and less technical implementations.

.. autofunction :: sma.binaryCodeToClass4Motifs
.. autofunction :: sma.binaryCodeToClass3Motifs


Drawing SENs
------------

Most example graphs in this documentation were drawn using the following functions.

.. autofunction :: sma.drawSEN
.. autofunction :: sma.drawGeoSEN
.. autofunction :: sma.drawMotif
.. autofunction :: sma.layer_layout
.. autofunction :: sma.advanced_layer_layout


Other Functions
---------------

.. autofunction :: sma.maxEdgeCountMatrix
.. autofunction :: sma.randomEdgeCountMatrix
.. autofunction :: sma.progress_indicator
